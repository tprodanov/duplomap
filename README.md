[![Install with bioconda](https://img.shields.io/conda/v/bioconda/duplomap.svg?label=Install%20with%20conda&color=blueviolet&style=flat-square)](https://anaconda.org/bioconda/duplomap)
[![Updated](https://anaconda.org/bioconda/duplomap/badges/latest_release_date.svg?style=flat-square)](https://anaconda.org/bioconda/duplomap)

DuploMap
===================

DuploMap is designed to improve precision and recall of long-read alignments in segmental duplications.

Created by Timofey Prodanov `timofey.prodanov[at]gmail.com` and Vikas Bansal `vibansal[at]health.ucsd.edu` at the University of California San Diego.

Table of contents
=================
* [Citing DuploMap](#citing-duplomap)
* [Installation](#installation)
* [Time and memory usage](#time-and-memory-usage)
* [Precomputed PSV databases](#precomputed-psv-databases)
* [Mapping reads](#mapping-reads)
    * [Output files](#output-map)
* [Creating a PSV database](#creating-a-psv-database)
    * [Output files](#output-prepare)
* [Test pipeline](#test-pipeline)
* [Issues](#issues)
* [See also](#see-also)

Citing DuploMap
===========

If you use DuploMap, please cite:
- Prodanov T., Bansal V. (2020) Sensitive alignment using paralogous sequence variants improves
    long-read mapping and variant calling in segmental duplications. *Nucleic Acids Research*,
    Volume 48, Issue 19, Page e114,
    [doi:10.1093/nar/gkaa829](https://doi.org/10.1093/nar/gkaa829)

Installation
============

To install the DuploMap you can use `conda`:
```
conda install -c bioconda duplomap
```

For manual installation you need Rust language compiler.
It can be installed using the following command:
```
curl https://sh.rustup.rs -sSf | sh
```

Next, you can download and build the source code using
```
git clone https://gitlab.com/tprodanov/duplomap.git
cd duplomap
./build
```

To install `duplomap` to the `PATH`, you can use `--install` argument,
for example use `./build --install /usr/local` to install to `/usr/local/bin`.

DuploMap requires installed [samtools](http://samtools.sourceforge.net).

Time and memory usage
=====================

Creating a PSV database with standard parameters usually requires less than 1 Gb of memory and less than 10 minutes on one core. Resulting output directory weights around 100 Mb, and can be significantly shrinked by removing `psvs.vcf.gz` and
`prepare.log`.

DuploMap realigns reads jointly for each segmental duplications cluster, and therefore it stores all alignments in memory.
Memory usage scales lineary with coverage and can take 30-64 Gb. You can reduce memory usage significantly by using a database without high copy number regions (shown in the table below). Without such regions, large clusters of duplications split into multiple smaller clusters, reducing peak memory usage.

DuploMap usually requires less time than initial read mapping, and takes 2-5 hours for 30x coverage using 8 cores. You can specify number of cores using `-@` (4 by default). Number of cores does not affect memory usage.

Precomputed PSV databases
=========================

You can download precomputed PSV databases for the human genome here:

Genome | Version | Regions | Sum length | Database | PSVs |
-------|---------|---------|------------|----------|------|
hg19 | 0.9 | All           | 90.1 Mb  | [22 MB](https://dl.dropboxusercontent.com/s/x57wec2ndt66ik0/hg19.tar.gz) | [59 MB](https://dl.dropboxusercontent.com/s/v00kv0uynf2rv9z/hg19.psvs.vcf.gz) |
hg19 | 0.9 | Copy num. < 6 | 79.2 Mb  | [13 MB](https://dl.dropboxusercontent.com/s/fub2asp8c5ipjs1/hg19-cn6.tar.gz) | [34 MB](https://dl.dropboxusercontent.com/s/ccr6rgalxve7m0x/hg19-cn6.psvs.vcf.gz) |
hg38 | 0.9 | All           | 106.7 Mb | [27 MB](https://dl.dropboxusercontent.com/s/bicrz0i7vw2r0e5/hg38.tar.gz) | [76 MB](https://dl.dropboxusercontent.com/s/fdz38axuyj0bxff/hg38.psvs.vcf.gz) |
hg38 | 0.9 | Copy num. < 6 | 92.1 Mb  | [15 MB](https://dl.dropboxusercontent.com/s/5wrjdodetafcm2f/hg38-cn6.tar.gz) | [41 MB](https://dl.dropboxusercontent.com/s/6wxyyw6095zhi65/hg38-cn6.psvs.vcf.gz) |
hg19 | 0.8 | All           | 85.6 Mb  | [21 MB](https://dl.dropboxusercontent.com/s/cp5cep5mdruewck/hg19.tar.gz) | [50 MB](https://dl.dropboxusercontent.com/s/120zrja2svmnpzw/hg19.psvs.vcf.gz) |
hg38 | 0.8 | All           | 101.4 Mb | [26 MB](https://dl.dropboxusercontent.com/s/4aw0s9ngo2b6vyt/hg38.tar.gz) | [65 MB](https://dl.dropboxusercontent.com/s/2e5t0lp64vu1mj6/hg38.psvs.vcf.gz) |

You need to decompress a database using `tar xf <database>.tar.gz` before running `duplomap`.

You can use databases without high copy number regions (Copy num. < 6) to reduce memory usage.

Mapping reads
=============

DuploMap realigns reads that intersect a segmental duplication or lie completely within it. Input BAM file
should be sorted and indexed.

You can realign reads using a command
```
./duplomap -i input.sort.bam -d psv_database -r reference.fasta -o out_dir
```

Additionally, you can specify sequencing technology with `-x <preset>`, which is then passed to `minimap2`.

Note, that the reference fasta should be in plain fasta format (without gz), and should have `.fai` index
(you can run `samtools faidx <fasta>`).

You can use `./duplomap --help` to get all arguments.

<a name="output-map"></a> Output files
----------------

Output directory contains two main files: `realigned.bam` and `psvs.vcf.gz`.

Output alignment file `realigned.bam` is already sorted and indexed. It contains changed alignments for reads that intersect segmental duplications and unchanged alignments for all other reads. You can use argument `--skip-unique` to output only reads that overlap duplications. DuploMap removes all tags from the realigned reads except for read group `RG`. Additionally, it outputs the following tags:
- NM: edit distance,
- AS: Minimap2 alignment score of the segment,
- re: realignment information. Contains three values:
    - alignment locations consistency (`same` or `diff`),
    - mapping quality before and after (for example `low->high`), mapping quality is high if it is greater or equal than 30,
    - and reason for realignment (for example `lcs`, `psvs` and others).

    For example read that was realigned to the same position with higher MAPQ because on the LCS filtering step would have tag `re:Z:same,low->high,lcs`,
- oa: original alignment location, for example `oa:Z:chr1:180092-188246`,
- om: original mapping quality, for example `om:i:22`,
- co: number of conflicting PSVs (PSVs that support a different alignment location). Contains two numbers: number of conflicting PSVs and total number of reliable PSVs, for example read with 1 conflicting PSV out of 100 would have tag `co:B:I,1,100`,
- db: PSV database cluster, for example `db:Z:3-CDH12`.

By default, DuploMap outputs only a primary alignment and supplementary alignments, associated with it.
You can find information about supplementary/primary alignments in an `SA` tag.
Additionally, you can specify the number of output secondary alignments using `--secondary <INT|all>` flag.
In that case each read will have a tag `tp`: single char, `P` (primary) or `S` (secondary). Additionally, you can
find all non-supplementary primary or secondary alignments in an `se` tag.

Variants file `psvs.vcf.gz` contains information about *paralogous sequence variants* (PSVs). It contains lines similar to:
```
#CHROM POS   ID REF ALT QUAL  FILTER INFO                                                FORMAT             SAMPLE
chr7   37363 .  G   A   113.2 PASS   db=1-CNTNAP3B;psv=1ffeb22d;paralog=chr16:90100405:- GT:GQ:DP:GL:sp:rel 0/0:113:34:-0,-11.33,-99.78:32,0,2:1
```
INFO field `db=x` shows the name of the segmental duplications cluster. Field `psv=x` shows the PSV hash, and field
`paralog=chr:pos:strand` shows position of the second copy of the PSV.
Format field `sp` shows three numbers (`32,0,2` here) that show the number of reads that support reference allele (32);
alternative allele (0); and number of ambigous reads that do not support either allele (2). Field `rel` shows the estimated
probability that the PSV is reliable.

Creating a PSV database
=======================

To create a PSV database you will need a csv file with segmental duplications. Human segmental duplications can be found at the [Segmental Duplications Database](http://humanparalogy.gs.washington.edu/) or can be downloaded from the
[UCSC genome browser](https://genome.ucsc.edu/) through a *Table Browser*. You can use a subset of the segmental duplications CSV file.

You can create a PSV database using a command
```
./duplomap-prepare -i segm_dup.csv -r reference.fasta [-a annotation.gff] -o db_dir
```
You can provide a genome annotation in GFF or GFF3 format to see genes that intersect segmental duplications.

You can use `./duplomap-prepare --help` to get all arguments.

Use `--skip-copy-num` to remove regions with high copy number.

<a name="output-prepare"></a> Output files
----------------

Output directory contains multiple `name.db` files, each contains a PSV database for a single cluster of segmental duplications.

Output directory also contains `summary.csv`, which provides some characteristics of the components, such as the number of duplicated regions, sum length and mean copy number. Bed file `regions.bed` contains all regions that will be analysed by DuploMap. Graph description can be found in `duplications.dot`.

More detailed information about each component can be found in the `description.txt`

Test pipeline
=============

You can run a small pipeline to see how DuploMap works. For that, download
[test data (8 MB)](https://dl.dropboxusercontent.com/s/to08sezjzelciwo/test_data.tar.gz)
to `duplomap` folder and decompress it using `tar xf test_data.tar.gz`. Then, you can construct a PSV database with
```
./duplomap-prepare -i test_data/segm_dup.csv -r test_data/genome.fa -o test_data/db
```

After that you can map reads using
```
./duplomap -i test_data/reads.bam -d test_data/db -r test_data/genome.fa -o test_data/out
```

Directory `test_data/db` will contain PSV database. You can find realigned reads at `test_data/out/realigned.bam` and information about PSVs at `test_data/out/psvs.vcf.gz`.

Additionally, you can align reads with `--generated` flag, as input reads contain information about their true location:
```
./duplomap -i test_data/reads.bam -d test_data/db -r test_data/genome.fa -o test_data/out2 --generated
```

Issues
======
Please submit issues [here](https://gitlab.com/tprodanov/duplomap/issues) or send them to `timofey.prodanov[at]gmail.com`.

See also
========

Additionally, you may be interested in these tools:
- [Longshot](https://github.com/pjedge/longshot/): fast and accurate long read variant calling tool,
- [Pileuppy](https://gitlab.com/tprodanov/pileuppy): colorful and fast BAM pileup.
