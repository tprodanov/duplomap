use std::sync::mpsc;
use std::time::{Instant, Duration};
use std::io::{stderr, Write, Result};
use std::fmt::Write as FmtWrite;
use std::process::{Command, Stdio};

use term_size;

use utils::common;

#[derive(Debug)]
enum Action {
    Prefix(String),
    Message(String),
    Progress(Option<(u32, u32)>),
    Pause(bool),
    Increment,
    Finish,
}

pub struct Observer {
    id: usize,
    sender: mpsc::Sender<(usize, Action)>,
}

impl Observer {
    fn send(&self, action: Action) {
        self.sender.send((self.id, action))
            .expect("Failed to send a message: Receiver is already disconnected");
    }

    fn try_send(&self, action: Action) -> std::result::Result<(), mpsc::SendError<(usize, Action)>> {
        self.sender.send((self.id, action))
    }

    pub fn prefix(&self, prefix: String) {
        self.send(Action::Prefix(prefix.to_string()));
    }

    pub fn message(&self, message: String) {
        self.send(Action::Message(message.to_string()));
    }

    pub fn progress(&self, current: u32, total: u32) {
        self.send(Action::Progress(Some((current, total))));
    }

    pub fn remove_progress(&self) {
        self.send(Action::Progress(None));
    }

    pub fn pause(&self) {
        self.send(Action::Pause(true));
    }

    pub fn start(&self) {
        self.send(Action::Pause(false));
    }

    pub fn inc(&self) {
        self.send(Action::Increment);
    }

    pub fn finish(&self) {
        self.send(Action::Finish);
    }
}

impl Drop for Observer {
    fn drop(&mut self) {
        let _ = self.try_send(Action::Finish);
    }
}

struct Message {
    prefix: String,
    message: String,
    progress: Option<(u32, u32)>,
    paused: bool,
    finished: bool,
    full_message: String,
    need_update: bool,
    bar_chars: Vec<&'static str>,
}

impl Message {
    fn new(bar_chars: Vec<&'static str>) -> Self {
        Message {
            prefix: String::default(),
            message: String::default(),
            progress: None,
            paused: true,
            finished: false,
            full_message: String::new(),
            need_update: true,
            bar_chars,
        }
    }

    fn draw(&mut self, writer: &mut impl Write) -> Result<()> {
        if self.need_update {
            self.full_message.clear();
            write!(self.full_message, "{}  {}  ", self.prefix, self.message)
                .expect("Could not write message to string");
            if let Some((current, total)) = self.progress {
                write!(self.full_message, "{:>5} / {:>5}", current, total)
                    .expect("Could not write message to string");

                let width = term_size::dimensions().map(|(w, _h)| w).unwrap_or(80);
                let bar_width = width.saturating_sub(self.full_message.len() + 5);

                if bar_width > 5 {
                    self.full_message.push_str("  [");
                    let ratio = current as f64 / total as f64 * bar_width as f64;
                    let left = ratio.floor() as usize;
                    self.full_message.push_str(
                        &self.bar_chars[self.bar_chars.len() - 1].repeat(left));
                    if current < total {
                        self.full_message.push_str(&self.bar_chars[
                            (ratio.fract() * (self.bar_chars.len() - 1) as f64).round() as usize]);
                    }
                    if left + 1 < bar_width {
                        self.full_message.push_str(&self.bar_chars[0].repeat(bar_width - left - 1));
                    }
                    self.full_message.push_str("]");
                }
            }
            self.need_update = false;
        }
        writeln!(writer, "{}", self.full_message)
    }

    fn set_prefix(&mut self, prefix: String) {
        self.prefix = prefix;
        self.need_update = true;
        self.paused = false;
    }

    fn set_message(&mut self, message: String) {
        self.message = message;
        self.need_update = true;
        self.paused = false;
    }

    fn set_progress(&mut self, progress: Option<(u32, u32)>) {
        self.need_update = self.need_update || (self.progress != progress);
        self.progress = progress;
        self.paused = false;
    }

    fn inc(&mut self) {
        self.progress.iter_mut().for_each(|(current, _total)| *current += 1);
        self.need_update = true;
        self.paused = false;
    }
}

fn is_utf8() -> bool {
    let child = match Command::new("locale")
            .arg("charmap")
            .stdout(Stdio::piped())
            .stderr(Stdio::null()).spawn() {
        Ok(value) => value,
        _ => return false,
    };
    let output = match child.wait_with_output() {
        Ok(value) => value.stdout,
        _ => return false,
    };
    let mut output = match String::from_utf8(output) {
        Ok(value) => value,
        _ => return false,
    };
    output.make_ascii_lowercase();
    output.contains("utf-8") || output.contains("utf8")
}

pub struct Narrator {
    sender: mpsc::Sender<(usize, Action)>,
    receiver: mpsc::Receiver<(usize, Action)>,
    messages: Vec<Message>,
    timer: Instant,
    bar_chars: Vec<&'static str>,
}

impl Narrator {
    pub fn new(timer: Instant) -> Narrator {
        let bar_chars = if is_utf8() {
            vec![" ", "\u{258F}", "\u{258E}", "\u{258D}", "\u{258C}", "\u{258B}",
                "\u{258A}", "\u{2589}", "\u{2588}"]
        } else {
            vec![" ", "="]
        };

        let (sender, receiver) = mpsc::channel();
        Narrator {
            sender,
            receiver,
            messages: Vec::new(),
            timer,
            bar_chars,
        }
    }

    pub fn create_observer(&mut self) -> Observer {
        self.messages.push(Message::new(self.bar_chars.clone()));
        Observer {
            id: self.messages.len() - 1,
            sender: self.sender.clone(),
        }
    }

    pub fn run(&mut self, sleep_time: Duration) {
        loop {
            for (id, action) in self.receiver.try_iter() {
                match action {
                    Action::Prefix(prefix) => self.messages[id].set_prefix(prefix),
                    Action::Message(message) => self.messages[id].set_message(message),
                    Action::Progress(progress) => self.messages[id].set_progress(progress),
                    Action::Pause(value) => self.messages[id].paused = value,
                    Action::Increment => self.messages[id].inc(),
                    Action::Finish => self.messages[id].finished = true,
                }
            }
            self.draw().unwrap_or_else(|_| error!("Failed to write a message to the stderr"));
            if self.messages.iter().all(|msg| msg.finished) {
                return;
            }
            for message in self.messages.iter_mut() {
                if message.finished {
                    message.paused = true;
                }
            }
            std::thread::sleep(sleep_time);
        }
    }

    fn draw(&mut self) -> Result<()> {
        if self.messages.iter().all(|msg| msg.paused || !msg.need_update) {
            return Ok(());
        }

        let mut writer = stderr();
        writeln!(writer)?;
        common::write_duration(&mut writer, &self.timer.elapsed())?;
        writeln!(writer)?;
        for message in self.messages.iter_mut() {
            if !message.paused {
                message.draw(&mut writer)?;
            }
        }
        Ok(())
    }
}
