use std::path::Path;
use std::fs::{self, File};
use std::str::FromStr;
use std::collections::HashMap;
use std::io::{self, Write};
use std::time::{Instant, Duration};

use clap::ArgMatches;
use colored::Colorize;
use slog;
use serde::{Serialize, Serializer};
use serde::de::{Deserialize, Deserializer, Error};
use flate2::write::GzEncoder;

use utils::init_logger;

pub fn clear_path(path: &Path, force: bool, cont: bool) -> Result<(), &'static str> {
    if force && cont {
        eprintln!("{}", "Cannot use both --force and --continue at the same time".red().bold());
        panic!();
    }
    if path.is_file() {
        if force {
            fs::remove_file(path).map_err(|_| "Failed to clean output directory")?;
        } else if !cont {
            eprintln!("{}", "Output path is not empty. Consider using --force".red().bold());
            panic!();
        }
        return Ok(());
    }

    if fs::read_dir(path).map_err(|_| "Failed to access output directory")?.next().is_some() {
        if force {
            eprintln!("{}", "Warning: Clearing non-empty output directory".yellow());
            fs::remove_dir_all(path).map_err(|_| "Failed to clean output directory")?;
        } else if !cont {
            eprintln!("{}", "Output path is not empty. Consider using --force".red().bold());
            panic!();
        }
    } else {
        fs::remove_dir(path).map_err(|_| "Failed to clean output directory")?;
    }
    Ok(())
}

pub fn create_dir<P: AsRef<Path>>(path: P, can_exist: bool) -> io::Result<()> {
    match fs::create_dir(&path) {
        Err(e) if !can_exist || e.kind() != io::ErrorKind::AlreadyExists
          => Err(e),
        _ => Ok(()),
    }
}

pub fn create_main_directory(matches: &MatchesMap) -> Result<(), String> {
    let path = Path::new(matches.get_str("output"));

    let force = matches.contains("force");
    let cont = matches.contains("continue");
    if path.exists() {
        clear_path(path, force, cont)?;
    }
    create_dir(&path, cont)
        .map_err(|_| format!("Failed to create output directory '{}'", path.display()))
}

pub fn matches_to_map(matches: &ArgMatches) -> MatchesMap {
    matches.args.iter().map(|(key, value)| (key.to_string(),
        value.vals.iter()
            .map(|os_str| os_str.to_str()
                .unwrap_or_else(|| panic!("Cannot convert argument {:?}", os_str))
                .to_string())
            .collect()
        )).collect()
}

fn parse_or_panic<T: FromStr, S: AsRef<str>>(value: &str, name: S) -> T {
    match T::from_str(value) {
        Ok(res) => res,
        Err(_) => panic!("Argument --{}: value \"{}\" has unexpected type", name.as_ref(), value),
    }
}

pub type MatchesMap = HashMap<String, Vec<String>>;

pub trait Matches {
    fn contains<S: AsRef<str>>(&self, name: S) -> bool;

    fn get_nth_str<S: AsRef<str>>(&self, name: S, index: usize) -> &str;

    fn get_str<S: AsRef<str>>(&self, name: S) -> &str {
        self.get_nth_str(name, 0)
    }

    fn get_nth_value<T: FromStr, S: AsRef<str>>(&self, name: S, index: usize) -> T {
        parse_or_panic(self.get_nth_str(&name, index), name)
    }

    fn get_value<T: FromStr, S: AsRef<str>>(&self, name: S) -> T {
        self.get_nth_value(name, 0)
    }

    fn get_str_vec<S: AsRef<str>>(&self, name: S, len: Option<usize>) -> Vec<&str>;

    fn get_str_vec_or_default<'a, 'b: 'a, S: AsRef<str>>(&'a self,
            name: S, default: Vec<&'b str>) -> Vec<&'a str> {
        if self.contains(&name) {
            self.get_str_vec(name, Some(default.len()))
        } else {
            default
        }
    }

    fn get_vec<T: FromStr, S: AsRef<str>>(&self, name: S, len: Option<usize>) -> Vec<T> {
        let values = self.get_str_vec(&name, len);
        values.into_iter().map(|s| parse_or_panic(s, &name)).collect()
    }

    fn get_vec_or_default<T: FromStr, S: AsRef<str>>(&self, name: S, default: Vec<T>)-> Vec<T> {
        if self.contains(&name) {
            self.get_vec(name, Some(default.len()))
        } else {
            default
        }
    }
}

impl Matches for MatchesMap {
    fn contains<S: AsRef<str>>(&self, name: S) -> bool {
        self.contains_key(name.as_ref())
    }

    fn get_nth_str<S: AsRef<str>>(&self, name: S, index: usize) -> &str {
        let name = name.as_ref();
        match self.get(name) {
            Some(values) if values.len() > index => &values[index],
            Some(_) => panic!("Argument --{} has unexpected number of arguments"),
            None => panic!("Please specify --{} argument", name),
        }
    }

    fn get_str_vec<S: AsRef<str>>(&self, name: S, len: Option<usize>) -> Vec<&str> {
        let name = name.as_ref();
        match self.get(name) {
            Some(values) => {
                if let Some(len_value) = len {
                    if len_value != values.len() {
                        panic!("Argument --{} has {} values (expected {})",
                            name, len_value, values.len());
                    }
                }
                values.iter().map(std::borrow::Borrow::borrow).collect()
            },
            None => panic!("Please specify --{} argument", name),
        }
    }
}

pub fn create_logger(timer: &Instant, matches: &MatchesMap, filename: &str)
        -> slog_scope::GlobalLoggerGuard {
    let terminal = Some(slog::Level::Info);
    let log_file = Path::new(matches.get_str("output")).join(filename);
    let file = init_logger::get_level_name(matches.get_str("log"))
        .map(|level| (log_file.as_path(), level));
    let logger = init_logger::create(timer, terminal, file);
    let guard = init_logger::register(logger);
    greet();
    guard
}

pub fn greet() {
    info!("{}", std::env::args().collect::<Vec<_>>().join(" "));
    info!("    {}", chrono::Local::now().format("%Y-%m-%d %H:%M:%S"));
    if let Some(git_hash) = option_env!("GIT_HASH") {
        debug!("    Git hash: {}", git_hash);
    }
}

pub fn goodbye(duration: &Duration) {
    info!("Success!");
    info!("Finished at {}", chrono::Local::now().format("%Y-%m-%d %H:%M:%S"));
    info!("Elapsed time: {}", write_duration_str(duration));
}

pub fn print_parameters(matches: &MatchesMap) {
    let mut keys: Vec<_> = matches.keys().collect();
    keys.sort();
    debug!("Parameters:");
    for key in keys {
        let values = &matches[key];
        if values.is_empty() {
            debug!("    {:<20} (flag present)", key);
        } else {
            debug!("    {:<20} : {}", key, matches[key].join(" "));
        }
    }
}

pub fn adjust_window_size(length: usize, window: usize) -> usize {
    let length = length as f64;
    let window = window as f64;
    let n_windows = (length / window).ceil();
    (length / n_windows).ceil() as usize
}

pub fn write_duration(writer: &mut dyn Write, duration: &Duration) -> std::io::Result<()> {
    let mut millis = duration.as_millis() as u64;
    let hours = millis / (60 * 60 * 1000);
    millis %= 60 * 60 * 1000;
    let minutes = millis / (60 * 1000);
    millis %= 60 * 1000;
    let seconds = millis / 1000;
    millis %= 1000;
    write!(writer, "{}:{:02}:{:02}.{:03}", hours, minutes, seconds, millis)
}

pub fn write_duration_str(duration: &Duration) -> String {
    let mut v = Vec::new();
    write_duration(&mut v, duration).unwrap();
    String::from_utf8(v).unwrap()
}

#[macro_export]
macro_rules! formatted {
    ($name:ident, $format:expr) => {
        pub struct $name<T: std::fmt::Display>(pub T);

        impl<T: std::fmt::Display> std::fmt::Display for $name<T> {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                write!(f, $format, self.0)
            }
        }

        impl<T: std::fmt::Display> serde::Serialize for $name<T> {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where S: serde::Serializer {
                serializer.collect_str(&self)
            }
        }
    }
}

pub struct NaOption<T: Serialize + FromStr>(pub Option<T>);

impl<T: Serialize + FromStr> Serialize for NaOption<T> {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        match self.0 {
            Some(ref value) => value.serialize(serializer),
            None => serializer.serialize_str("NA"),
        }
    }
}

impl<'de, T: Serialize + FromStr> Deserialize<'de> for NaOption<T> {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let in_value = <&str>::deserialize(deserializer)?;
        if in_value == "NA" {
            Ok(NaOption(None))
        } else {
            match T::from_str(in_value) {
                Ok(t_value) => Ok(NaOption(Some(t_value))),
                Err(_) => Err(D::Error::invalid_type(serde::de::Unexpected::Other("unknown type"),
                    &"NA or value of type T")),
            }
        }
    }
}

pub type GzWriter = GzEncoder<io::BufWriter<File>>;
pub type CsvGzWriter = csv::Writer<GzEncoder<File>>;

pub fn create_gzip_writer<P: AsRef<Path>>(path: P) -> io::Result<GzWriter> {
    let f = io::BufWriter::new(File::create(path)?);
    Ok(GzEncoder::new(f, flate2::Compression::default()))
}

pub fn create_csv_gzip_writer<P: AsRef<Path>>(path: P) -> io::Result<CsvGzWriter> {
    let f = GzEncoder::new(File::create(path)?, flate2::Compression::default());
    Ok(csv::WriterBuilder::new()
        .delimiter(b'\t')
        .from_writer(f))
}
