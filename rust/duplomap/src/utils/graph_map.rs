use std::collections::HashMap;
use std::hash::Hash;
use std::cmp::Eq;
use std::ops::Deref;

use petgraph::{Graph, Directed, Undirected, EdgeType};
use petgraph::graph::{NodeIndex, EdgeIndex, DefaultIx};
use petgraph::csr::IndexType;

// petgraph::graphmap::GraphMap requires Ord + Copy
pub struct GraphMap<N: Hash + Clone + Eq, E, Ty: EdgeType = Directed, Ix: IndexType = DefaultIx> {
    graph: Graph<N, E, Ty, Ix>,
    nodes: HashMap<N, NodeIndex<Ix>>,
}

pub type UnGraphMap<N, E, Ix = DefaultIx> = GraphMap<N, E, Undirected, Ix>;
pub type DiGraphMap<N, E, Ix = DefaultIx> = GraphMap<N, E, Directed, Ix>;

impl<N: Hash + Clone + Eq, E, Ty: EdgeType, Ix: IndexType> GraphMap<N, E, Ty, Ix> {
    pub fn new() -> Self {
        GraphMap {
            graph: Graph::default(),
            nodes: HashMap::new(),
        }
    }

    pub fn from_graph(graph: Graph<N, E, Ty, Ix>) -> Self {
        let nodes = graph.node_indices().map(|i| (graph[i].clone(), i)).collect();
        GraphMap { graph, nodes }
    }

    pub fn graph(&self) -> &Graph<N, E, Ty, Ix> {
        &self.graph
    }

    pub fn add_node(&mut self, weight: N) -> NodeIndex<Ix> {
        if self.nodes.contains_key(&weight) {
            return self.nodes[&weight];
        }
        let index = self.graph.add_node(weight.clone());
        self.nodes.insert(weight, index);
        index
    }

    pub fn get_node(&self, weight: &N) -> Option<&NodeIndex<Ix>> {
        self.nodes.get(weight)
    }

    pub fn add_edge(&mut self, a: NodeIndex<Ix>, b: NodeIndex<Ix>, weight: E) -> EdgeIndex<Ix> {
        self.graph.add_edge(a, b, weight)
    }

    pub fn update_edge(&mut self, a: NodeIndex<Ix>, b: NodeIndex<Ix>, weight: E) -> EdgeIndex<Ix> {
        self.graph.update_edge(a, b, weight)
    }
}

impl<N: Hash + Clone + Eq, E, Ty: EdgeType, Ix: IndexType> Deref for GraphMap<N, E, Ty, Ix> {
    type Target = Graph<N, E, Ty, Ix>;

    fn deref(&self) -> &Self::Target {
        &self.graph
    }
}