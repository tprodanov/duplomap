use std::fmt::{self, Write as fmtWrite};
use std::io::{self, Write, BufRead, BufReader, Error, ErrorKind};
use std::fs::File;
use std::cmp::Ordering;
use std::path::Path;

use bam::bgzip;

use biology::Genome;

#[derive(Clone)]
pub struct Record {
    pub chrom_id: u32,
    // 1-based position
    pub pos: u32,
    pub id: String,
    pub reference: String,
    pub alt: String,
    pub qual: f32,
    pub filter: String,
    pub info: String,
    pub format: String,
    pub samples: Vec<String>,
}

fn write_or_missing<W: Write>(stream: &mut W, value: &str, missing: &str) -> io::Result<()> {
    write!(stream, "\t{}", if value.is_empty() { missing } else { value })
}

impl Record {
    pub fn new() -> Record {
        Record {
            chrom_id: 0,
            pos: 0,
            id: String::new(),
            reference: String::new(),
            alt: String::new(),
            qual: 0.0,
            filter: String::new(),
            info: String::new(),
            format: String::new(),
            samples: Vec::with_capacity(1),
        }
    }

    pub fn clear(&mut self) {
        self.chrom_id = 0;
        self.pos = 0;
        self.id.clear();
        self.reference.clear();
        self.alt.clear();
        self.qual = 0.0;
        self.filter.clear();
        self.info.clear();
        self.format.clear();
        self.samples.clear();
    }

    pub fn chrom_id(&mut self) -> u32 {
        self.chrom_id
    }

    pub fn pos(&mut self) -> u32 {
        self.pos
    }

    pub fn qual(&mut self) -> f32 {
        self.qual
    }

    pub fn set_chrom_id(&mut self, chrom_id: u32) {
        self.chrom_id = chrom_id;
    }

    pub fn set_pos(&mut self, pos: u32) {
        self.pos = pos;
    }

    pub fn set_qual(&mut self, qual: f32) {
        self.qual = qual;
    }

    pub fn update_id(&mut self, id: &str) {
        self.id += id;
    }

    pub fn update_ref(&mut self, reference: &str) {
        self.reference += reference;
    }

    pub fn update_alt(&mut self, alt: &str) {
        if self.alt.is_empty() {
            self.alt += alt;
        } else {
            write!(self.alt, ",{}", alt).unwrap()
        }
    }

    pub fn push_filter(&mut self, filter: &str) {
        if self.filter.is_empty() {
            self.filter += filter;
        } else {
            write!(self.filter, ";{}", filter).unwrap()
        }
    }

    pub fn push_info<T: fmt::Display>(&mut self, key: &str, value: &T) {
        if self.info.is_empty() {
            write!(self.info, "{}={}", key, value).unwrap();
        } else {
            write!(self.info, ";{}={}", key, value).unwrap();
        }
    }

    pub fn push_format<T: fmt::Display>(&mut self, key: &str, values: &[&T]) {
        if self.format.is_empty() {
            self.format += key;
            for value in values.iter() {
                self.samples.push(format!("{}", value));
            }
        } else {
            assert!(values.len() == self.samples.len(), "Inconsistent number of samples");
            write!(self.format, ":{}", key).unwrap();
            for (sample, value) in self.samples.iter_mut().zip(values.iter()) {
                write!(sample, ":{}", value).unwrap();
            }
        }
    }

    fn write<W: Write>(&self, stream: &mut W, chromosomes: &[String]) -> io::Result<()> {
        write!(stream, "{}", chromosomes[self.chrom_id as usize])?;
        write!(stream, "\t{}", self.pos)?;
        write_or_missing(stream, &self.id, ".")?;
        write!(stream, "\t{}", self.reference)?;
        write_or_missing(stream, &self.alt, ".")?;
        // All available precision.
        write!(stream, "\t{:.}", self.qual)?;
        write_or_missing(stream, &self.filter, "PASS")?;
        write_or_missing(stream, &self.info, "*")?;
        write_or_missing(stream, &self.format, "*")?;
        for sample in self.samples.iter() {
            write!(stream, "\t{}", sample)?;
        }
        writeln!(stream)
    }

    // Do not implement PartialOrd because then we need to implement ==.
    pub fn cmp(&self, other: &Record) -> Ordering {
        (self.chrom_id, self.pos).cmp(&(other.chrom_id, other.pos))
    }
}

pub struct Writer<W: Write> {
    stream: W,
    chromosomes: Vec<String>,
}

impl Writer<bgzip::Writer<File>> {
    pub fn create_compressed<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Ok(Writer {
            stream: bgzip::Writer::from_path(path)?,
            chromosomes: Vec::new(),
        })
    }
}

impl Writer<File> {
    pub fn create<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Ok(Writer {
            stream: File::create(path)?,
            chromosomes: Vec::new(),
        })
    }
}

pub trait VcfWrite {
    fn write_line(&mut self, line: &str) -> io::Result<()>;
    fn add_chromosome(&mut self, name: String, length: u32) -> io::Result<()>;
    fn write_samples(&mut self, samples: &[&str]) -> io::Result<()>;

    fn write(&mut self, record: &Record) -> io::Result<()>;
    fn flush(&mut self) -> io::Result<()>;
}

impl<W: Write> VcfWrite for Writer<W> {
    fn write_line(&mut self, line: &str) -> io::Result<()> {
        writeln!(self.stream, "{}", line)
    }

    fn add_chromosome(&mut self, name: String, length: u32) -> io::Result<()> {
        writeln!(self.stream, "##contig=<ID={},length={}>", name, length)?;
        self.chromosomes.push(name);
        Ok(())
    }

    fn write_samples(&mut self, samples: &[&str]) -> io::Result<()> {
        write!(self.stream, "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")?;
        if !samples.is_empty() {
            write!(self.stream, "\tFORMAT")?;
        }
        for sample in samples.iter() {
            write!(self.stream, "\t{}", sample)?;
        }
        writeln!(self.stream)
    }

    fn write(&mut self, record: &Record) -> io::Result<()> {
        record.write(&mut self.stream, &self.chromosomes)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.stream.flush()
    }
}

pub struct Reader<R: BufRead> {
    stream: R,
    buf: String,
}

impl Reader<BufReader<bgzip::ConsecutiveReader<File>>> {
    pub fn load_compressed<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Ok(Reader {
            stream: BufReader::new(bgzip::ConsecutiveReader::from_path(path, 0)?),
            buf: String::new(),
        })
    }
}

impl Reader<BufReader<File>> {
    pub fn load<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Ok(Reader {
            stream: BufReader::new(File::open(path)?),
            buf: String::new(),
        })
    }
}

impl<R: BufRead> Reader<R> {
    pub fn read(&mut self, genome: &Genome) -> io::Result<Record> {
        loop {
            self.buf.clear();
            match self.stream.read_line(&mut self.buf) {
                Ok(0) => return Err(Error::new(ErrorKind::UnexpectedEof, "Reached EOF")),
                Ok(_) => {},
                Err(e) => return Err(e),
            }
            if self.buf.starts_with('#') {
                continue;
            }

            let mut record = Record::new();
            let split: Vec<_> = self.buf.trim_end().split('\t').collect();
            if split.len() < 8 {
                return Err(Error::new(ErrorKind::InvalidData, "File is truncated"));
            }
            record.set_chrom_id(genome.chrom_id(split[0]).ok_or_else(||
                Error::new(ErrorKind::InvalidData, "Chromosome id not in the genome"))?);
            record.set_pos(split[1].parse().map_err(|_|
                Error::new(ErrorKind::InvalidData, "Cannot parse pos"))?);
            record.update_id(split[2]);
            record.update_ref(split[3]);
            record.update_alt(split[4]);
            record.set_qual(split[5].parse().map_err(|_|
                Error::new(ErrorKind::InvalidData, "Cannot parse qual"))?);
            record.push_filter(split[6]);
            record.info += split[7];

            if !split[8].is_empty() {
                record.format += split[8];
            }
            for i in 9..split.len() {
                record.samples.push(split[i].to_string());
            }
            return Ok(record);
        }
    }

    pub fn records<'a, 'b: 'a>(&'a mut self, genome: &'b Genome) -> Records<'a, R> {
        Records {
            reader: self,
            genome,
        }
    }
}


pub struct Records<'a, R: BufRead> {
    reader: &'a mut Reader<R>,
    genome: &'a Genome,
}

impl<'a, R: BufRead> Iterator for Records<'a, R> {
    type Item = io::Result<Record>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.reader.read(self.genome) {
            Err(e) if e.kind() == ErrorKind::UnexpectedEof => None,
            res => Some(res),
        }
    }
}
