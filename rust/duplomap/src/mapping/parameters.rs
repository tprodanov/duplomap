use std::fmt::{Display, Formatter};

use utils::Log;
use alignment::hmm;
use alignment::c_minimap;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum UnknownRegions {
    Realign,
    KeepOld,
    Mapq0,
}

impl UnknownRegions {
    pub fn realign(self) -> bool {
        self == UnknownRegions::Realign
    }

    pub fn keep_old(self) -> bool {
        self == UnknownRegions::KeepOld
    }

    pub fn mapq0(self) -> bool {
        self == UnknownRegions::Mapq0
    }
}

impl std::str::FromStr for UnknownRegions {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "realign" => Ok(UnknownRegions::Realign),
            "keep-old" => Ok(UnknownRegions::KeepOld),
            "mapq0" => Ok(UnknownRegions::Mapq0),
            s => Err(format!("Unknown option {}", s)),
        }
    }
}

impl Display for UnknownRegions {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            UnknownRegions::Realign => write!(f, "realign"),
            UnknownRegions::KeepOld => write!(f, "keep-old"),
            UnknownRegions::Mapq0 => write!(f, "mapq0"),
        }
    }
}

#[derive(Clone)]
pub struct Priors {
    // Priors for 0/0, 0/1 and 1/1
    pub values: [Log; 3],
}

impl Priors {
    pub fn new(p00: f64) -> Priors {
        let p01 = p00 * (1.0 - p00);
        let p11 = 1.0 - p00 - p01;
        debug!("Genotype priors: P(0/0) = {:.2e}, P(0/1) = {:.2e}, P(1/1) = {:.2e}", p00, p01, p11);
        debug!("    P(0/0, 0/0) = {:.2e}", p00 * p00);
        debug!("    P(0/0, 0/1) = {:.2e}", p00 * p01);
        debug!("    P(0/0, 1/1) = {:.2e}", p00 * p11);
        debug!("    P(0/1, 0/1) = {:.2e}", p01 * p01);
        debug!("    P(0/1, 1/1) = {:.2e}", p01 * p11);
        debug!("    P(1/1, 0/0) = {:.2e}", p11 * p11);

        Priors {
            values: [
                Log::from_real(p00),
                Log::from_real(p01),
                Log::from_real(p11)],
        }
    }
}

#[derive(Clone)]
pub struct Parameters {
    pub psv_sub_complexity: f64,
    pub psv_indel_complexity: f64,
    pub psv_size_diff: u32,

    pub priors: Priors,
    pub read_psv_impact: Log,
    pub genotyping_min_mapq: u8,
    pub ambiguous_fold_diff: f32,
    pub ambiguous_reads: f32,
    pub high_cn_perc: f32,

    pub relative_padding: f32,
    pub unknown_regions: UnknownRegions,
    pub filtering_kmer: u32,
    pub filtering_p_value: f64,
    pub max_locations: u32,

    pub minimap_kmer_size: u8,
    pub minimap_preset: c_minimap::Preset,

    pub conflicts_p_value: f64,
    pub min_conflicts: u32,
    pub hmm_params: hmm::Parameters,
    pub skip_mapq: Option<u8>,
    pub use_m_in_cigar: bool,
    pub n_secondary: u16,
}

impl Default for Parameters {
    fn default() -> Parameters {
        Parameters {
            psv_sub_complexity: 0.6,
            psv_indel_complexity: 0.8,
            psv_size_diff: 10,

            priors: Priors::new(0.95),
            read_psv_impact: Log::from_log10(-3.0),
            genotyping_min_mapq: 30,
            ambiguous_fold_diff: 4.0,
            ambiguous_reads: 30.0,
            high_cn_perc: 50.0,

            relative_padding: 0.01,
            unknown_regions: UnknownRegions::Realign,
            filtering_kmer: 11,
            filtering_p_value: 1e-4,
            max_locations: 50,

            minimap_kmer_size: 11,
            minimap_preset: c_minimap::Preset::PacBio,

            conflicts_p_value: 0.05,
            min_conflicts: 5,
            hmm_params: hmm::Parameters::default(),
            skip_mapq: None,
            use_m_in_cigar: true,
            n_secondary: 0,
        }
    }
}
