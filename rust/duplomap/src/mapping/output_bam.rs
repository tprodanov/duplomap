use std::collections::{HashSet, HashMap};
use std::process::Command;
use std::path::Path;
use std::fs::{self, File};
use std::io::{self, BufWriter, Write, BufReader, BufRead};
use std::string::String;
use std::cmp::min;

use bio::utils::Text;
use num_format::{Locale, ToFormattedString};
use flate2::Compression;
use flate2::write::GzEncoder;
use bam::{RecordReader, RecordWriter, Record};
use bam::record::tags::TagValue;

use biology::spans::{Span, SpanTools, PositiveSpan};
use biology::Sequence;
use mapping::heavy_read::InformationRow;

pub fn sort(output_directory: &Path, samtools: &str, threads: u16) {
    let mut command = Command::new(samtools);
    command.arg("sort")
        .arg("-o").arg(output_directory.join("realigned_reads.bam"))
        .arg(output_directory.join("unsorted.bam"))
        .arg("-@").arg(threads.to_string());
    debug!("    {:?}", command);
    let sort_output = command.output().expect("Failed to run samtools sort");
    if !sort_output.status.success() {
        crit!("Samtools sort finished with non-zero status: {}",
            sort_output.status.code().unwrap_or(1));
        let stderr = String::from_utf8_lossy(&sort_output.stderr);
        debug!("Samtools sort stderr:\n{}", stderr);
        panic!("Stopping");
    }

    fs::remove_file(output_directory.join("unsorted.bam"))
        .unwrap_or_else(|_| error!("Failed to remove a temporary file"));
}

pub fn merge_csv(output_directory: &Path, filename: &str, names: &Vec<String>)
        -> std::io::Result<()> {
    let f = File::create(output_directory.join(format!("{}.gz", filename)))?;
    let mut output = BufWriter::new(GzEncoder::new(f, Compression::default()));
    for (i, db_name) in names.iter().enumerate() {
        let inp = BufReader::new(File::open(output_directory.join(db_name).join(filename))?);
        for line in inp.lines().skip(min(1, i)) {
            writeln!(output, "{}", line?)?;
        }
    }
    Ok(())
}

const MAPQ_THRESHOLDS: [u8; 4] = [0, 10, 20, 60];

struct InformationStats {
    total_reads: u64,
    unmappable_reads: u64,
    true_loc_known: bool,

    mapped_before: Vec<u64>,
    mapped_after: Vec<u64>,
    correct_before: Vec<u64>,
    correct_after: Vec<u64>,
}

impl InformationStats {
    fn new() -> Self {
        Self {
            total_reads: 0,
            unmappable_reads: 0,
            true_loc_known: false,

            mapped_before: vec![0; MAPQ_THRESHOLDS.len()],
            mapped_after: vec![0; MAPQ_THRESHOLDS.len()],
            correct_before: vec![0; MAPQ_THRESHOLDS.len()],
            correct_after: vec![0; MAPQ_THRESHOLDS.len()],
        }
    }

    fn update(&mut self, record: &InformationRow) {
        if !record.primary {
            return;
        }

        self.total_reads += 1;
        if record.unique_part == 0 && record.pair_psvs.0.unwrap_or(record.total_psvs) < 3 {
            self.unmappable_reads += 1;
        }

        let correct_before = record.correct_before.unwrap_or(false);
        let correct_after = record.correct_after.unwrap_or(false);
        self.true_loc_known |= record.correct_before.is_some();

        for (i, &mapq) in MAPQ_THRESHOLDS.iter().enumerate() {
            if record.mapq_before >= mapq {
                self.mapped_before[i] += 1;
                if correct_before {
                    self.correct_before[i] += 1;
                }
            }
            if record.mapq_after >= mapq {
                self.mapped_after[i] += 1;
                if correct_after {
                    self.correct_after[i] += 1;
                }
            }
        }
    }

    fn summarize<P: AsRef<Path>>(&self, path: P) -> std::io::Result<()> {
        let mut summary = Vec::new();
        writeln!(summary, "Analyzed total {} reads", self.total_reads.to_formatted_string(&Locale::en))?;
        if self.total_reads != 0 {
            let total_reads = self.total_reads as f32;
            writeln!(summary, "Reads that do not overlap unique region and overlap less than 3 PSVs: {} ({:4.1}%)",
                self.unmappable_reads.to_formatted_string(&Locale::en),
                100.0 * self.unmappable_reads as f32 / total_reads)?;

            writeln!(summary, "Mapped reads:")?;

            for (i, &mapq) in MAPQ_THRESHOLDS.iter().enumerate() {
                if mapq == 0 {
                    continue;
                }
                writeln!(summary, "    Mapping quality >= {}:", mapq)?;
                writeln!(summary, "        Before: {:>12} ({:4.1}%)",
                    self.mapped_before[i].to_formatted_string(&Locale::en),
                    100.0 * self.mapped_before[i] as f32 / total_reads)?;
                writeln!(summary, "        After:  {:>12} ({:4.1}%)",
                    self.mapped_after[i].to_formatted_string(&Locale::en),
                    100.0 * self.mapped_after[i] as f32 / total_reads)?;
            }
        }

        if self.true_loc_known {
            writeln!(summary, "\nCorrectly mapped reads:")?;
        }
        for (i, &mapq) in MAPQ_THRESHOLDS.iter().enumerate() {
            if !self.true_loc_known || (self.mapped_before[i] == 0 && self.mapped_after[i] == 0) {
                continue;
            }
            writeln!(summary, "    Mapping quality >= {}:", mapq)?;
            if self.mapped_before[i] != 0 {
                writeln!(summary, "        Before: {:>12} ({:4.1}%)",
                    self.correct_before[i].to_formatted_string(&Locale::en),
                    100.0 * self.correct_before[i] as f32 / self.mapped_before[i] as f32)?;
            }
            if self.mapped_after[i] != 0 {
                writeln!(summary, "        After:  {:>12} ({:4.1}%)",
                    self.correct_after[i].to_formatted_string(&Locale::en),
                    100.0 * self.correct_after[i] as f32 / self.mapped_after[i] as f32)?;
            }
        }
        let summary_str = std::str::from_utf8(&summary).unwrap();
        info!("\n{}", summary_str);

        let mut outp = File::create(path)?;
        write!(outp, "{}", summary_str)
    }
}

pub fn merge_information(output_directory: &Path, db_names: &Vec<String>,
        conflicts: &Conflicts, db_indices: &HashMap<String, u32>) -> Result<(), String> {
    let path = output_directory.join("information.csv.gz");
    let f = File::create(&path)
        .map_err(|_| format!("Failed to open output file {}", path.display()))?;
    let gz_writer = GzEncoder::new(f, Compression::default());
    let mut writer = csv::WriterBuilder::new()
        .delimiter(b'\t')
        .from_writer(gz_writer);

    let mut stats = InformationStats::new();
    for db_name in db_names.iter() {
        let path = output_directory.join(db_name).join("information.csv");
        let mut reader = csv::ReaderBuilder::new().delimiter(b'\t').from_path(&path)
            .map_err(|_| format!("Failed to open temporary file {}", path.display()))?;

        for row in reader.deserialize() {
            let mut record: InformationRow = row
                .map_err(|e| format!("Failed to parse information.csv row {:?}", e))?;
            let row_db = db_indices[&record.database];
            if let Some(entries) = conflicts.get(record.name.as_bytes()) {
                record.primary = false;
                for (i, entry) in entries.iter().enumerate() {
                    if row_db == entry.db_index {
                        record.mapq_after = entry.new_mapq;
                        if i == 0 {
                            record.primary = true;
                        }
                        break;
                    }
                }
            }
            stats.update(&record);
            writer.serialize(record)
                .map_err(|e| format!("Failed to write a record to information.csv: {:?}", e))?;
        }
    }
    if let Err(e) = stats.summarize(output_directory.join("summary.txt")) {
        error!("{}", e);
    }
    Ok(())
}

pub struct ReadEntry {
    db_index: u32,
    new_mapq: u8,
}

fn resolve_conflict(name: &Text, entries: &[(PositiveSpan, u8, u32)]) -> Vec<ReadEntry> {
    let mut has_conflict = false;
    let mut is_within = vec![false; entries.len()];
    for i in 0..entries.len() {
        for j in (i + 1)..entries.len() {
            let perc = entries[i].0.intersection_percentage(&entries[j].0);
            if perc <= 80.0 {
                has_conflict = true;
            } else if perc >= 90.0 {
                is_within[j] = true;
            }
        }
    }

    if has_conflict {
        warn!("Read {} has conflicting alignments for different components:", name.to_str());
        entries.iter().enumerate().filter(|(i, _)| !is_within[*i])
            .map(|(_, (span, mapq, db_index))| {
                warn!("    {:?} with MAPQ {}", span, mapq);
                ReadEntry {
                    db_index: *db_index,
                    new_mapq: 0,
                }
            }).collect()
    } else {
        trace!("Resolved conflict for read {}", name.to_str());
        vec![ReadEntry {
            db_index: entries[0].2,
            new_mapq: entries[0].1,
        }]
    }
}

type Conflicts = HashMap<Text, Vec<ReadEntry>>;

// Returns names of all changed reads, as well as conflicts.
pub fn load_names(reader: &mut impl RecordReader, db_indices: &HashMap<String, u32>) -> (HashSet<Text>, Conflicts) {
    let mut entries = HashMap::<Text, Vec<(PositiveSpan, u8, u32)>>::new();
    let mut record = Record::new();
    while reader.read_into(&mut record).unwrap_or_else(|e| panic!("{}", e)) {
        if record.flag().is_secondary() || record.flag().is_supplementary() {
            continue;
        }
        let name = record.name().to_vec();
        let aln_span = PositiveSpan::new(record.ref_id() as u32,
            record.start() as u32, record.calculate_end() as u32);
        let db_index = if let Some(TagValue::String(db_name, _)) = record.tags().get(b"db") {
            db_indices[std::str::from_utf8(db_name).unwrap()]
        } else {
            panic!("Read has no tag 'db'")
        };
        entries.entry(name).or_insert_with(|| Vec::with_capacity(1))
            .push((aln_span, record.mapq(), db_index));
    }

    let conflicts = entries.iter_mut().filter_map(|(name, read_alns)| {
        if read_alns.len() == 1 {
            None
        } else {
            read_alns.sort_by(|a, b| b.0.len().cmp(&a.0.len()));
            Some((name.clone(), resolve_conflict(name, read_alns)))
        }
    }).collect();
    (entries.into_iter().map(|(k, _)| k).collect(), conflicts)
}

fn write_changed_record(writer: &mut impl RecordWriter, record: &mut Record,
        conflicts: &Conflicts, db_indices: &HashMap<String, u32>) -> io::Result<()> {
    match conflicts.get(record.name()) {
        Some(entries) => {
            let db_index = if let Some(TagValue::String(db_name, _)) = record.tags().get(b"db") {
                db_indices[std::str::from_utf8(db_name).unwrap()]
            } else {
                panic!("Read has no tag 'db'")
            };
            for i in 0..entries.len() {
                if entries[i].db_index == db_index {
                    record.set_mapq(entries[i].new_mapq);
                    if i > 0 {
                        record.flag_mut().set_secondary(true);
                    }
                    writer.write(&record)?;
                    return Ok(());
                }
            }
        },
        None => {
            writer.write(&record)?;
        },
    }
    Ok(())
}

fn less(a: &Record, b: &Record) -> bool {
    // Convert to u32 to make -1 a big number.
    let a_rid = a.ref_id() as u32;
    let b_rid = b.ref_id() as u32;
    a_rid < b_rid || (a_rid == b_rid && a.start() < b.start())
}

pub fn combine_bams(writer: &mut impl RecordWriter,
        changed_bam: &mut impl RecordReader, initial_bam: &mut impl RecordReader,
        skip_initial: bool, names: &HashSet<Text>, conflicts: &Conflicts,
        db_indices: &HashMap<String, u32>) -> io::Result<()> {
    if skip_initial {
        info!("Writing bam file (only reads from duplicated regions)");
    } else {
        info!("Writing bam file (all reads)");
    }
    let mut changed_record = Record::new();
    let mut have_changed = changed_bam.read_into(&mut changed_record)?;
    let mut initial_record = Record::new();
    let mut have_initial = !skip_initial && initial_bam.read_into(&mut initial_record)?;

    while have_changed && have_initial {
        if less(&changed_record, &initial_record) {
            write_changed_record(writer, &mut changed_record, conflicts, db_indices)?;
            have_changed = changed_bam.read_into(&mut changed_record)?;
        } else {
            if !names.contains(initial_record.name()) {
                writer.write(&initial_record)?;
            }
            have_initial = initial_bam.read_into(&mut initial_record)?;
        }
    }
    while have_initial {
        if !names.contains(initial_record.name()) {
            writer.write(&initial_record)?;
        }
        have_initial = initial_bam.read_into(&mut initial_record)?;
    }
    while have_changed {
        write_changed_record(writer, &mut changed_record, conflicts, db_indices)?;
        have_changed = changed_bam.read_into(&mut changed_record)?;
    }
    Ok(())
}

pub fn index_bam(path: &str, samtools: &str, threads: u16) {
    info!("Indexing output bam file");
    let mut command = Command::new(samtools);
    command.arg("index")
        .arg(path)
        .arg("-@").arg(threads.saturating_sub(1).to_string());
    debug!("    {:?}", command);
    let index_output = match command.output() {
        Err(e) => {
            error!("Failed to run samtools index: {}", e);
            return;
        },
        Ok(value) => value,
    };

    if !index_output.status.success() {
        error!("Samtools index finished with non-zero status: {}",
            index_output.status.code().unwrap_or(1));
        let stderr = String::from_utf8_lossy(&index_output.stderr);
        debug!("Samtools index stderr:\n{}", stderr);
    }
}
