use std::path::Path;
use std::io;
use std::fs;

use biology::Genome;
use database::HeavyDatabase;
use database::heavy_database::PsvId;
use utils::vcf::{self, VcfWrite};

fn write_header(writer: &mut impl VcfWrite, genome: &Genome) -> io::Result<()> {
    writer.write_line("##fileformat=VCFv4.2")?;
    for i in 0..genome.count_chromosomes() as u32 {
        writer.add_chromosome(genome.chrom_name(i).to_string(), genome.chrom_len(i))?;
    }

    writer.write_line("##FILTER=<ID=ambiguous,Description=\"The PSV had many ambiguously aligned reads\
        after the first iteration\">")?;
    writer.write_line("##INFO=<ID=db,Number=1,Type=String,Description=\"Database Name\">")?;
    writer.write_line("##INFO=<ID=psv,Number=1,Type=String,Description=\"PSV Hash\">")?;
    writer.write_line("##INFO=<ID=paralog,Number=1,Type=String,\
        Description=\"Paralogous Position\">")?;
    writer.write_line("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">")?;
    writer.write_line("##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">")?;
    writer.write_line("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth\">")?;
    writer.write_line("##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Genotype Likelihoods\">")?;
    writer.write_line("##FORMAT=<ID=sp,Number=3,Type=Integer,\
        Description=\"Allele Support: Ref,Alt,Neither\">")?;
    writer.write_line("##FORMAT=<ID=rel,Number=1,Type=Float,Description=\"Probability that the \
        PSV is reliable\">")?;
    writer.write_samples(&["SAMPLE"])
}

pub fn create<P: AsRef<Path>>(path: P, genome: &Genome) -> io::Result<vcf::Writer<fs::File>> {
    let mut writer = vcf::Writer::create(path)?;
    write_header(&mut writer, genome)?;
    Ok(writer)
}

pub fn write_component<W: io::Write>(writer: &mut vcf::Writer<W>, db: &HeavyDatabase, genome: &Genome)
        -> io::Result<()> {
    let mut record = vcf::Record::new();
    for i in 0..db.psvs().len() {
        let psv = db.psv_at(PsvId::new(i));
        for j in 0..2 {
            if psv.genotype(j).read_depth() == 0 {
                continue;
            }
            record.clear();
            psv.to_vcf(&mut record, j, genome, db.name());
            psv.genotype(j).update_vcf_record(&mut record, psv.active());
            record.push_format("rel", &[&format!("{:.3}", psv.p_reliable().real())]);
            writer.write(&record)?;
        }
    }
    Ok(())
}

pub fn sort(output: &Path, genome: &Genome) -> io::Result<()> {
    let mut reader = vcf::Reader::load(output.join("unsorted.vcf"))?;
    let mut records = reader.records(genome).collect::<io::Result<Vec<_>>>()?;
    records.sort_by(|a, b| a.cmp(b));

    let mut writer = vcf::Writer::create_compressed(output.join("psvs.vcf.gz"))?;
    write_header(&mut writer, genome)?;
    for record in records {
        writer.write(&record)?;
    }

    fs::remove_file(output.join("unsorted.vcf")).unwrap_or_else(|_| error!("Failed to remove a temporary file"));
    Ok(())
}