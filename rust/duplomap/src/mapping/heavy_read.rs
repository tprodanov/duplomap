use std::collections::{HashSet, HashMap};
use std::cmp::{min, max};
use std::sync::Arc;
use std::fmt::Write;

use bio::utils::TextSlice;
use slog_scope;
use slog::Drain;
use bam::{self, RecordWriter};
use bam::record::tags::TagValue;
use serde::{Serialize, Serializer};
use serde::de::{Deserialize, Deserializer, Error};

use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan};
use biology::genome::{Genome, Sequence};
use biology::IdConverter;
use alignment::Alignment;
use alignment::c_minimap;
use alignment::indexed_alignment::IndexedAlignment;
use alignment::hmm;
use mapping::half_matrix::HalfMatrix;
use mapping::locations;
use mapping::filtering;
use mapping::parameters::Parameters;
use database::heavy_database::{HeavyDatabase, PsvId, WindowId};
use database::psv::Psv;
use database::genotype::{GenotypeValue, GenotypeUpdate};
use utils::{Log, log_numbers};
use utils::common::{NaOption, CsvGzWriter};

struct PsvProb {
    psv_id: PsvId,
    allele_0: Log,
    allele_1: Log,
    same_order: bool,
}

impl PsvProb {
    fn new(alignment1_span: &PositiveSpan, alignment2_span: &PositiveSpan, psv_id: PsvId,
            database: &HeavyDatabase) -> Option<Self> {
        let aln_spans = [alignment1_span, alignment2_span];
        let psv = database.psv_at(psv_id);
        let spans = [psv.span(0), psv.span(1)];

        let same_order;
        if aln_spans[0].intersects(spans[0]) {
            if aln_spans[0].intersects(spans[1]) {
                // If one alignment intersects both spans
                same_order = (aln_spans[0].start_comparator() < aln_spans[1].start_comparator())
                        == (spans[0].start_comparator() < spans[1].start_comparator());
            } else {
                same_order = true;
            }
        } else {
            same_order = false;
        }
        assert!(aln_spans[0].intersects(spans[1 - same_order as usize]));
        if !aln_spans[1].intersects(spans[same_order as usize]) {
            None
        } else {
            Some(PsvProb {
                allele_0: Log::ZERO,
                allele_1: Log::ZERO,
                psv_id, same_order,
            })
        }
    }

    fn init_probabilities(&mut self, indexed_aln1: &IndexedAlignment, cache: &mut RealignmentCache,
            database: &HeavyDatabase, hmm_params: &hmm::Parameters) {
        let psv = database.psv_at(self.psv_id);
        let (q_start, q_seq) = indexed_aln1.subsequence(psv.span(1 - self.same_order as usize));
        let probs = get_or_realign(q_start, &q_seq, self.psv_id, &psv, cache, hmm_params);
        self.allele_0 = probs[1 - self.same_order as usize];
        self.allele_1 = probs[self.same_order as usize];
    }

    // Returns false if was not used.
    fn update_location_probabilities(&self, database: &HeavyDatabase,
            prob_1: &mut Log, prob_2: &mut Log, params: &Parameters) -> bool {
        let psv = database.psv_at(self.psv_id);
        if !psv.use_for_loc_probs() {
            return false;
        }
        let reliable = psv.p_reliable();
        let unreliable = Log::ONE - reliable;
        *prob_1 *= max(params.read_psv_impact, reliable * self.allele_0 + unreliable);
        *prob_2 *= max(params.read_psv_impact, reliable * self.allele_1 + unreliable);
        true
    }
}

fn construct_pairwise_psvs(alignments: &Vec<LazyAlignment>, caches: &mut Vec<RealignmentCache>,
        database: &HeavyDatabase, hmm_params: &hmm::Parameters)
        -> HalfMatrix<Vec<PsvProb>> {
    let n_alns = alignments.len();
    let mut res: HalfMatrix<Vec<_>> = HalfMatrix::new(max(1, n_alns));
    if n_alns <= 1 {
        return res;
    }

    for i in 0..n_alns {
        for j in i + 1..n_alns {
            let element = res.at_mut(i, j);
            let mut same_psvs = 0;

            for ialn in alignments[i].get() {
                let span1 = ialn.alignment().aln_span();
                let psvs1: HashSet<PsvId> = database.find_psv_ids(span1).collect();
                for span2 in alignments[j].spans() {
                    let psvs2: HashSet<PsvId> = database.find_psv_ids(&span2).collect();

                    for &psv_id in psvs1.intersection(&psvs2) {
                        let mut psv_prob = match PsvProb::new(span1, &span2, psv_id, database) {
                            Some(value) => value,
                            None => {
                                same_psvs += 1;
                                continue;
                            },
                        };
                        psv_prob.init_probabilities(ialn, &mut caches[i], database, hmm_params);
                        element.push(psv_prob);
                    }
                }
            }
            if same_psvs > 0 {
                debug!("            Pair {} {} had {} same PSVs!", i, j, same_psvs);
            }
        }
    }
    res
}

fn filter_supplementary(alignments: &mut Vec<Alignment>, genome: &Genome) {
    let mut pr_i = None;
    for (i, alignment) in alignments.iter().enumerate() {
        if !alignment.supplementary {
            assert!(pr_i.is_none(), "There are at least two primary, non supplementary \
                pr_i for the same query");
            pr_i = Some(i);
        }
    }
    let pr_i = pr_i.expect("There is no primary alignmnets for the read");

    let mut keep = vec![true; alignments.len()];
    let n = alignments.len();

    for (i, aln) in alignments.iter().enumerate() {
        if aln.aln_span().chrom_id() != alignments[pr_i].aln_span().chrom_id()
                || aln.strand() != alignments[pr_i].strand() {
            keep[i] = false;
        }
    }

    for i in 0..n - 1 {
        if !keep[i] {
            continue;
        }
        let aln_i = &alignments[i];

        for j in i + 1..n {
            let aln_j = &alignments[j];
            if keep[j] && (aln_i.aln_span().intersects(aln_j.aln_span())
                    || aln_i.query_range().intersects(aln_j.query_range())) {
                if i == pr_i
                        || (j != pr_i && aln_i.query_range().len() >= aln_j.query_range().len()) {
                    keep[j] = false;
                } else {
                    keep[i] = false;
                    break;
                }
            }
        }
    }
    let mut i = 0;
    alignments.retain(|aln| {
        if !keep[i] {
            let aln_span = aln.aln_span();
            let query_range = aln.query_range();
            debug!("            Discarding     {} (len: {}). Query: {} (len: {})",
                aln_span.format(genome), aln_span.len(), query_range, query_range.len());
        }
        i += 1;
        keep[i - 1]
    });
}

fn parse_sa(record: &bam::Record) {
    if let Some(TagValue::String(value, _)) = record.tags().get(b"SA") {
        let value_str = if let Ok(value_str) = std::str::from_utf8(value) {
            value_str
        } else {
            error!("Input read has non UTF-8 SA tag");
            return;
        };
        for aln in value_str.split(';') {
            if aln.is_empty() {
                continue;
            }
            let aln: Vec<_> = aln.split(',').collect();
            if aln.len() < 5 {
                error!("Input read has incorrect SA tag (too short)");
                return;
            }
            debug!("        Supplementary alignment at {}:{} ({} strand), mapq: {}",
                aln[0], aln[1], aln[2], aln[4]);
        }
    }
}

struct InitialAlignment {
    alignment: Alignment,
    mapping_quality: u8,
    sequence: Arc<bam::record::Sequence>,
    qualities: bam::record::Qualities,
    rg_tag: Option<Vec<u8>>,
}

impl InitialAlignment {
    pub fn new(record: &bam::Record, id_converter: &IdConverter, genome: &mut Genome,
            hmm_params: &hmm::Parameters) -> Result<Self, &'static str> {
        let mapping_quality = record.mapq();
        let sequence = record.sequence().clone();

        let alignment = Alignment::from_bam_record(record, id_converter)?;
        debug!("    Initial alignment: {}, mapq: {}",
            alignment.aln_span().format(genome), mapping_quality);
        parse_sa(record);
        if slog_scope::with_logger(|logger| logger.is_trace_enabled()) {
            let r_seq = genome.fetch(alignment.aln_span());
            let stats = alignment.get_stats(&sequence, true, &r_seq, hmm_params);
            alignment.write_stats(&stats, genome);
        }

        let rg_tag = if let Some(TagValue::String(group, _)) = record.tags().get(b"RG") {
            Some(group.to_vec())
        } else {
            None
        };

        Ok(InitialAlignment {
            alignment,
            mapping_quality,
            sequence: Arc::new(sequence),
            qualities: record.qualities().clone(),
            rg_tag,
        })
    }
}

// u32 - query position.
type RealignmentCache = HashMap<(PsvId, u32), [Log; 2]>;

fn get_or_realign<'a>(q_pos: u32, q_seq: TextSlice, psv_id: PsvId, psv: &Psv,
        cache: &'a mut RealignmentCache, params: &hmm::Parameters) -> &'a [Log; 2] {
    if !cache.contains_key(&(psv_id, q_pos)) {
        let mut probs = if q_seq.len() >= 4 * max(psv.sequence(0).len(), psv.sequence(1).len()) {
            [Log::ONE; 2]
        } else {
            [hmm::align(q_seq, psv.sequence(0), params), hmm::align(q_seq, psv.sequence(1), params)]
        };
        log_numbers::normalize(&mut probs);
        cache.insert((psv_id, q_pos), probs);
    }
    &cache[&(psv_id, q_pos)]
}

enum LazyAlignment {
    Aligned(Vec<IndexedAlignment>),
    Location(DirectionalSpan),
}

impl LazyAlignment {
    fn new(location: DirectionalSpan) -> Self {
        LazyAlignment::Location(location)
    }

    fn from_alignment(alignment: Alignment, sequence: Arc<bam::record::Sequence>, r_seq: TextSlice,
            genome: &Genome, hmm_params: &hmm::Parameters) -> LazyAlignment {
        LazyAlignment::Aligned(
            vec![IndexedAlignment::new(sequence, r_seq, alignment, genome, hmm_params)])
    }

    fn is_aligned(&self) -> bool {
        match self {
            LazyAlignment::Aligned(_) => true,
            LazyAlignment::Location(_) => false,
        }
    }

    fn is_location(&self) -> bool {
        match self {
            LazyAlignment::Aligned(_) => false,
            LazyAlignment::Location(_) => true,
        }
    }

    fn get(&self) -> &[IndexedAlignment] {
        match self {
            LazyAlignment::Aligned(ialns) => ialns,
            _ => panic!("Indexed alignment should be already defined"),
        }
    }

    fn get_primary(&self) -> &IndexedAlignment {
        self.get().iter().filter(|ialn| ialn.alignment().primary).next().unwrap()
    }

    fn location(&self) -> Option<&DirectionalSpan> {
        match self {
            LazyAlignment::Location(location) => Some(location),
            _ => None,
        }
    }

    fn spans(&self) -> Vec<PositiveSpan> {
        match self {
            LazyAlignment::Aligned(ialns) => ialns.iter()
                .map(|ialn| ialn.alignment().aln_span().clone()).collect(),
            LazyAlignment::Location(location) => vec![location.to_positive()],
        }
    }

    fn upgrade_using_alignment(&mut self, alignment: Alignment, sequence: Arc<bam::record::Sequence>,
            r_seq: TextSlice, genome: &Genome, hmm_params: &hmm::Parameters) {
        match self {
            LazyAlignment::Aligned(_) => panic!("Cannot upgrade aligned LazyAlignment"),
            LazyAlignment::Location(location) =>
                assert!(location.contains(alignment.aln_span()),
                    "Cannot upgrade LazyAlignment: location does not contain the new alignment"),
        };
        *self = LazyAlignment::Aligned(
            vec![IndexedAlignment::new(sequence, r_seq, alignment, genome, hmm_params)]);
    }

    fn try_upgrade(&mut self, index: usize,
            sequence: Arc<bam::record::Sequence>, hard_clipping: (u32, u32),
            genome: &mut Genome, params: &Parameters) -> bool {
        let location = match self {
            LazyAlignment::Aligned(_) => { return true; },
            LazyAlignment::Location(location) => location.clone(),
        };

        let pos_location = location.to_positive();
        let r_seq = genome.fetch(&pos_location);

        let mut alignments = c_minimap::align(sequence.to_vec_acgtn_only(), r_seq.to_vec(),
            &pos_location, hard_clipping, params.minimap_kmer_size as i32, location.strand(),
            params.minimap_preset, params.use_m_in_cigar);
        let loc_str = location.format(genome);
        if alignments.is_empty() {
            debug!("        Location {}: {}. Zero alignments", index, loc_str);
            return false;
        } else if alignments.len() == 1 {
            debug!("        Location {}: {}. Aligned successfully", index, loc_str);
        } else {
            debug!("        Location {}: {}. Primary + {} supplementary alignment(s):",
                index, loc_str, alignments.len() - 1);
        }

        filter_supplementary(&mut alignments, genome);
        let ialns = alignments.into_iter().map(|aln| {
            let r_start = aln.aln_span().start() - pos_location.start();
            let r_end = aln.aln_span().end() - pos_location.start();
            IndexedAlignment::new(Arc::clone(&sequence), &r_seq[r_start as usize..r_end as usize],
                aln, genome, &params.hmm_params)
        }).collect();
        *self = LazyAlignment::Aligned(ialns);
        true
    }
}

fn should_remove(lazy_alignments: &[LazyAlignment], i: usize, genome: &Genome) -> bool {
    const INTERSECTION: f64 = 90.0;
    for j in 0..lazy_alignments.len() {
        if i == j || !lazy_alignments[j].is_aligned() {
            continue;
        }
        let span_i = lazy_alignments[i].get_primary().alignment().aln_span();
        let span_j = lazy_alignments[j].get_primary().alignment().aln_span();
        let perc = span_i.intersection_percentage(span_j);
        if span_i.len() <= span_j.len() && perc >= INTERSECTION {
            debug!("    Removing alignment {}: {} and {} singificantly intersect",
                i, span_i.format(genome), span_j.format(genome));
            return true;
        }
    }
    false
}

fn transform_lazy_alignments(lazy_alignments: &mut Vec<LazyAlignment>, initial: &InitialAlignment,
        hard_clipping: (u32, u32), genome: &mut Genome, params: &Parameters) {
    if lazy_alignments.is_empty() {
        return;
    }
    for i in (0..lazy_alignments.len() - 1).rev() {
        if !lazy_alignments[i].try_upgrade(i, Arc::clone(&initial.sequence), hard_clipping, genome, params) {
            lazy_alignments.remove(i);
        }
    }
    if lazy_alignments.len() > 1 {
        debug!("        Not aligning to location {}: {} yet", lazy_alignments.len() - 1,
            lazy_alignments[lazy_alignments.len() - 1].location().unwrap().format(genome));
    }

    for i in (0..lazy_alignments.len() - 1).rev() {
        if should_remove(&lazy_alignments, i, genome) {
            lazy_alignments.remove(i);
        }
    }

    if lazy_alignments.len() != 1 {
        return;
    }
    if lazy_alignments[0].location()
            .expect("LazyAlignments[0] should not be transformed yet")
            .contains(initial.alignment.aln_span()) {
        debug!("    Single location, which contains the initial alignment");
        let mut alignment = initial.alignment.clone();
        alignment.set_strand(true);
        let r_seq = genome.fetch(alignment.aln_span());
        lazy_alignments[0].upgrade_using_alignment(alignment, Arc::clone(&initial.sequence),
            &r_seq, genome, &params.hmm_params);
    } else {
        debug!("    Single location, which does not contain the initial alignment");
        if !lazy_alignments[0].try_upgrade(0, Arc::clone(&initial.sequence), hard_clipping, genome, params) {
            lazy_alignments.remove(0);
        }
    }
}

pub fn get_true_location(name: &str, genome: &Genome) -> Option<PositiveSpan> {
    name.splitn(2, '/').next()
        .and_then(|value| value.rsplitn(2, '=').next())
        .and_then(|value| PositiveSpan::from_string(value, genome).ok())
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct InformationRow {
    pub name: String,
    pub read_len: u32,
    pub primary: bool,

    pub database: String,
    pub aln_before: String,
    pub aligned_read_len_before: u32,
    pub aln_after: String,
    pub aligned_read_len_after: String,

    pub unique_part: u32,
    pub total_psvs: u32,
    pub pair_psvs: NaOption<u32>,
    pub conflicts: String,

    pub mapq_before: u8,
    pub mapq_after: u8,
    reason: Reason,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub correct_before: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub correct_after: Option<bool>,
}

formatted!(Precision3, "{:.3}");

#[derive(serde::Serialize)]
struct ReadPsvsRow<'a> {
    name: &'a str,
    chrom: &'a str,
    pos: u32,
    psv_id: String,
    gt: GenotypeValue,
    gt_qual: u8,
    prob0: Precision3<f64>,
    prob1: Precision3<f64>,
    seq: &'a str,
}

fn binomial_test(part: u32, total: u32, p: f64) -> f64 {
    use statrs::distribution::{Univariate, Binomial};
    let binomial = Binomial::new(p, total as u64).unwrap();
    1.0 - binomial.cdf((part as f64) - 1.0)
}

#[derive(Clone, Copy)]
enum Reason {
    Unknown,
    Lcs,
    Psvs,
    Conflicts,
    NoBest,
    HighMapq,
    HighCopyNum,
    UnknownRef,
    ZeroAlns,
}

impl Reason {
    // to_str instead of Display, because here we can produce &'static str
    fn to_str(&self) -> &'static str {
        match self {
            Reason::Unknown => "unknown",
            Reason::Lcs => "lcs",
            Reason::Psvs => "psvs",
            Reason::Conflicts => "confl",
            Reason::NoBest => "no_best",
            Reason::HighMapq => "high_mapq",
            Reason::HighCopyNum => "high_copy_num",
            Reason::UnknownRef => "unknown_ref",
            Reason::ZeroAlns => "zero_alns"
        }
    }
}

impl Serialize for Reason {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_str(self.to_str())
    }
}

impl<'de> Deserialize<'de> for Reason {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let in_value = <&str>::deserialize(deserializer)?;
        match in_value {
            "unknown" => Ok(Reason::Unknown),
            "lcs" => Ok(Reason::Lcs),
            "psvs" => Ok(Reason::Psvs),
            "confl" => Ok(Reason::Conflicts),
            "no_best" => Ok(Reason::NoBest),
            "high_mapq" => Ok(Reason::HighMapq),
            "high_copy_num" => Ok(Reason::HighCopyNum),
            "unknown_ref" => Ok(Reason::UnknownRef),
            "zero_alns" => Ok(Reason::ZeroAlns),
            variant => Err(D::Error::unknown_variant(variant,
                &["?", "lcs", "psvs", "confl", "no_best", "high_mapq", "unknown_ref", "zero_alns"])),
        }
    }
}

fn generate_tag(initial: &Alignment, new_aln: &Alignment, initial_mapq: u8,
        new_mapq: &(u8, Reason)) -> String {
    let locations = if initial.aln_span().intersection_percentage(new_aln.aln_span()) >= 60.0
        { "same" } else { "diff" };
    let mapq1 = if initial_mapq >= 30 { "high" } else { "low" };
    let mapq2 = if new_mapq.0 >= 30 { "high" } else { "low" };
    format!("{},{}->{},{}", locations, mapq1, mapq2, new_mapq.1.to_str())
}

const CONFLICT_PROB_DIFF: f64 = 10.0;

pub struct HeavyRead {
    name: String,
    lazy_alignments: Vec<LazyAlignment>,
    realignment_cache: Vec<RealignmentCache>,
    psvs: HalfMatrix<Vec<PsvProb>>,
    best_is_defined: bool,
    ordered_locs: Vec<u16>,
    mapping_quality: (u8, Reason),
    initial: InitialAlignment,
    conflict_psvs: u32,
    appl_psvs: u32,
}

fn correct_aln(true_location: &PositiveSpan, aln_span: &PositiveSpan) -> bool {
    if !true_location.intersects(aln_span) {
        return false;
    }
    const MAX_TAIL_SIZE: u32 = 100;
    const MIN_OVERLAP: f32 = 0.25;
    aln_span.end().saturating_sub(true_location.end()) <= MAX_TAIL_SIZE
        && true_location.start().saturating_sub(aln_span.start()) <= MAX_TAIL_SIZE
        && true_location.intersection_size(aln_span) as f32 / true_location.len() as f32 >= MIN_OVERLAP
}

impl HeavyRead {
    pub fn new(mut record: bam::Record, database: &HeavyDatabase, genome: &mut Genome,
            id_converter: &IdConverter, params: &Parameters) -> Result<HeavyRead, &'static str> {
        use mapping::parameters::UnknownRegions;

        let name = std::str::from_utf8(record.name()).map_err(|_| "Read name is not in UTF-8")?.to_string();
        debug!("Read {} (len: {})", name, record.sequence().len());
        let initial_alignment = InitialAlignment::new(&mut record, id_converter, genome, &params.hmm_params)?;
        if let Some(mapq_threshold) = params.skip_mapq {
            if record.mapq() >= mapq_threshold {
                return HeavyRead::new_without_realignment(name, initial_alignment, genome, params, Reason::HighMapq);
            }
        }
        std::mem::drop(record);
        let high_copy_num_perc = database.high_copy_num_perc(initial_alignment.alignment.aln_span());
        if high_copy_num_perc >= params.high_cn_perc {
            debug!("    Read intersects high copy number regions by {:.1}%. Do not realign", high_copy_num_perc);
            return HeavyRead::new_without_realignment(name, initial_alignment, genome, params, Reason::HighCopyNum);
        } else if high_copy_num_perc >= 10.0 {
            debug!("    Read intersects high copy number regions by {:.1}%", high_copy_num_perc);
        }

        let locs = locations::find_all_locations(&initial_alignment.alignment, database,
            genome, params.relative_padding);
        let locs = filtering::filter_locations(&*initial_alignment.sequence, &locs, genome, params);
        let n_locations = locs.len();
        let mut mapping_quality = (0, Reason::Unknown);
        if n_locations == 0 {
            mapping_quality = match params.unknown_regions {
                UnknownRegions::Realign | UnknownRegions::KeepOld =>
                    (initial_alignment.mapping_quality, Reason::UnknownRef),
                UnknownRegions::Mapq0 => (0, Reason::UnknownRef),
            };
        }

        let hard_clipping = (
            initial_alignment.alignment.cigar().hard_clipping(true),
            initial_alignment.alignment.cigar().hard_clipping(false));

        let mut alignments = locs.into_iter().map(|location| LazyAlignment::new(location)).collect();
        transform_lazy_alignments(&mut alignments, &initial_alignment, hard_clipping, genome, params);
        if n_locations != 0 && alignments.is_empty() {
            error!("Read {} did not align to any possible location", name);
            mapping_quality = (0, Reason::ZeroAlns);
        }
        if alignments.len() as u32 > params.max_locations {
            error!("Read {} has {} possible locations (more than threshold {})",
                name, alignments.len(), params.max_locations);
            alignments.clear();
        }
        let mut caches = vec![RealignmentCache::new(); alignments.len()];

        let psvs = construct_pairwise_psvs(&alignments, &mut caches, database, &params.hmm_params);
        if alignments.len() == 1 && params.unknown_regions == UnknownRegions::Realign {
            mapping_quality = (254, Reason::Lcs);
        }

        Ok(HeavyRead {
            name, psvs,
            lazy_alignments: alignments,
            realignment_cache: caches,
            best_is_defined: false,
            ordered_locs: vec![],
            mapping_quality,
            initial: initial_alignment,
            conflict_psvs: 0,
            appl_psvs: 0,
        })
    }

    fn new_without_realignment(name: String, initial_alignment: InitialAlignment, genome: &mut Genome,
            params: &Parameters, reason: Reason) -> Result<HeavyRead, &'static str> {
        let r_seq = genome.fetch(initial_alignment.alignment.aln_span());
        let alignments = vec![LazyAlignment::from_alignment(initial_alignment.alignment.clone(),
            Arc::clone(&initial_alignment.sequence), &r_seq, genome, &params.hmm_params)];
        let caches = vec![RealignmentCache::new()];
        let psvs = HalfMatrix::new(1);

        Ok(HeavyRead {
            name, psvs,
            lazy_alignments: alignments,
            realignment_cache: caches,
            best_is_defined: false,
            ordered_locs: vec![],
            mapping_quality: (initial_alignment.mapping_quality, reason),
            initial: initial_alignment,
            conflict_psvs: 0,
            appl_psvs: 0,
        })
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn count_possible_alignments(&self) -> usize {
        self.lazy_alignments.len()
    }

    fn select_best(&mut self, pairwise: &HalfMatrix<Log>, corresponds_to_original: Option<usize>) {
        self.ordered_locs.clear();
        assert!(pairwise.side() > 1, "Cannot select two best locations having only one location");
        let at = |i: usize, j: usize| {
            if i < j {
                *pairwise.at(i, j)
            } else if i == j {
                Log::ONE
            } else {
                pairwise.at(j, i).inverse()
            }
        };

        'outer: for i in 0..pairwise.side() {
            for j in 0..pairwise.side() {
                if at(i, j) < Log::ONE {
                    continue 'outer;
                }
            }
            self.ordered_locs.push(i as u16);
        }

        if self.ordered_locs.is_empty() {
            debug!("        Cannot select the best location");
            // Stores sum of logs of all negative elements in the correspodning row.
            let sum_negative: Vec<_> = (0..pairwise.side())
                .map(|i| (0..pairwise.side()).fold(0.0, |acc, j| acc + at(i, j).log10().min(0.0))).collect();
            self.ordered_locs.extend(0..pairwise.side() as u16);
            // Sort indices by `-sum_negative[i]`.
            self.ordered_locs.sort_by(|&a, &b|
                sum_negative[b as usize].partial_cmp(&sum_negative[a as usize]).unwrap());
            self.best_is_defined = false;
            self.mapping_quality = (0, Reason::NoBest);
            debug!("    Locations order: {:?}, mapq of the best location: 0", self.ordered_locs);
            return;
        }

        if self.ordered_locs.len() > 1 {
            if let Some(original_loc) = corresponds_to_original {
                if self.ordered_locs.contains(&(original_loc as u16)) {
                    self.ordered_locs[0] = original_loc as u16;
                }
            }
            // Unless one of the locations corresponds to the original, select a random location.
            self.ordered_locs.truncate(1);
        }

        let best = self.ordered_locs[0];
        let ratio_to_best: Vec<_> = (0..pairwise.side()).map(|i| at(best as usize, i)).collect();
        self.ordered_locs.extend((0..pairwise.side() as u16).filter(|&i| i != best));
        (&mut self.ordered_locs[1..]).sort_by(|&a, &b| ratio_to_best[a as usize].cmp(&ratio_to_best[b as usize]));
        let second_best = self.ordered_locs[1];
        let mapq = log_numbers::phred_from_ratio(at(best as usize, second_best as usize));
        self.mapping_quality = (mapq.min(254.0) as u8, Reason::Psvs);
        self.best_is_defined = true;
        debug!("    Locations order: {:?}, mapq of the best location: {}",
            self.ordered_locs, self.mapping_quality.0);
    }

    pub fn update_location_probabilities(&mut self, database: &HeavyDatabase, genome: &mut Genome,
            params: &Parameters, iteration: u32) {
        let n_alns = self.lazy_alignments.len();
        if n_alns == 0 {
            return;
        }
        if n_alns == 1 {
            self.ordered_locs.clear();
            self.ordered_locs.push(0);
            self.best_is_defined = true;
            return;
        }
        debug!("    Iter {}. Updating location probabilities for {}", iteration, self.name);
        let mut pairwise = HalfMatrix::<Log>::new(n_alns);

        for (i, j, psvs) in self.psvs.enumerate() {
            trace!("        Pair {} {}", i, j);
            let mut prob_i = Log::ONE;
            let mut prob_j = Log::ONE;
            let mut ignored_psvs = 0;
            for psv_prob in psvs.iter() {
                if !psv_prob.update_location_probabilities(database, &mut prob_i, &mut prob_j, params) {
                    ignored_psvs += 1;
                }
                trace!("            After PSV {}:  {:?}  {:?}", database.psv_at(psv_prob.psv_id).hash(), prob_i, prob_j);
            }

            if ignored_psvs > 0 {
                debug!("        Pair {} {}: Probabilities {:?}  {:?}   Log10 ratio = {:.3}.  \
                    Used {} PSVs, skipped {}", i, j, prob_i, prob_j, (prob_i / prob_j).log10(),
                    psvs.len() - ignored_psvs, ignored_psvs);
            } else {
                debug!("        Pair {} {}: Probabilities {:?}  {:?}   Log10 ratio = {:.3}.  \
                    Used {} PSVs", i, j, prob_i, prob_j, (prob_i / prob_j).log10(), psvs.len());
            }
            *pairwise.at_mut(i, j) = prob_i / prob_j;
        }

        trace!("    Selecting best location");
        let mut corresponds_to_original = None;
        let mut best_intersection = 0;
        for (i, lazy_aln) in self.lazy_alignments.iter().enumerate() {
            let intersection = match lazy_aln {
                LazyAlignment::Aligned(_) => lazy_aln.get_primary().alignment().aln_span()
                        .intersection_size(self.initial.alignment.aln_span()),
                LazyAlignment::Location(loc) => loc.intersection_size(
                        self.initial.alignment.aln_span()),
            };
            if intersection > best_intersection {
                best_intersection = intersection;
                corresponds_to_original = Some(i);
            }
        }
        self.select_best(&pairwise, corresponds_to_original);

        if self.best_is_defined {
            let best = self.ordered_locs[0] as usize;
            if self.lazy_alignments[best].is_location() {
                debug!("        Best location not aligned yet, aligning");
                let hard_clipping =
                    (self.initial.alignment.cigar().hard_clipping(true),
                    self.initial.alignment.cigar().hard_clipping(false));
                let could_align = if !self.lazy_alignments[best].try_upgrade(best,
                        Arc::clone(&self.initial.sequence), hard_clipping, genome, params) {
                    false
                } else {
                    !should_remove(&self.lazy_alignments, best, genome)
                };

                if !could_align {
                    debug!("    Searching for a new best location");
                    self.lazy_alignments.remove(best);
                    self.realignment_cache.remove(best);
                    let n_alns = self.lazy_alignments.len();
                    let mut new_psvs: HalfMatrix<Vec<PsvProb>> = HalfMatrix::new(n_alns);
                    for i in 0..n_alns {
                        for j in (i + 1)..n_alns {
                            let i_old = if i >= best { i + 1 } else { i };
                            let j_old = if j >= best { j + 1 } else { j };
                            *new_psvs.at_mut(i, j) = std::mem::replace(
                                self.psvs.at_mut(i_old, j_old), Vec::new());
                        }
                    }
                    self.psvs = new_psvs;
                    self.update_location_probabilities(database, genome, params, iteration);
                }
            }
        }
    }

    pub fn compare_with_true_loc(&self, genome: &Genome) -> (bool, bool) {
        let true_location = match get_true_location(&self.name, genome) {
            Some(value) => value,
            None => {
                panic!("Reads are marked as --generated, but the true location is not \
                    in the read name {}", self.name);
            }
        };

        let correct_before = correct_aln(&true_location, self.initial.alignment.aln_span());
        let correct_after = if self.best_is_defined {
            correct_aln(&true_location, self.lazy_alignments[self.ordered_locs[0] as usize]
                .get_primary().alignment().aln_span())
        } else {
            correct_before
        };
        (correct_before, correct_after)
    }

    pub fn write_information<W: std::io::Write>(&self, writer: &mut csv::Writer<W>,
            database: &HeavyDatabase, genome: &Genome, generated: bool) -> Result<(), csv::Error> {
        let alignments = if self.best_is_defined {
            let mut alns: Vec<_> = self.lazy_alignments[self.ordered_locs[0] as usize].get().iter()
                .map(|ialn| ialn.alignment()).collect();
            alns.sort_by(|a, b| b.primary.cmp(&a.primary));
            alns
        } else {
            vec![&self.initial.alignment]
        };

        let total_psvs = alignments.iter()
            .map(|aln| database.find_psv_ids(aln.aln_span()).count() as u32).sum();

        let aligned_read_len_before = self.initial.alignment.query_range().len();
        let aligned_read_len_after = alignments.iter()
            .map(|aln| aln.query_range().len().to_string()).collect::<Vec<_>>().join(",");
        let aln_after = alignments.iter().map(|aln| aln.aln_span().format(genome).to_string())
            .collect::<Vec<_>>().join(",");

        let unique_part = alignments.iter().map(|aln| database.unique_size(aln.aln_span())).sum();
        let pair_psvs = NaOption(
            if self.best_is_defined && self.ordered_locs.len() > 1 {
                let best = self.ordered_locs[0] as usize;
                let second_best = self.ordered_locs[1] as usize;
                Some(self.psvs.at(min(best, second_best), max(best, second_best)).len() as u32)
            } else {
                None
            });

        let (correct_before, correct_after) = if generated {
            let (before, after) = self.compare_with_true_loc(genome);
            (Some(before), Some(after))
        } else {
            (None, None)
        };

        writer.serialize(InformationRow {
            name: self.name.to_string(),
            read_len: self.initial.sequence.len() as u32,
            primary: true,

            database: database.name().to_string(),
            aln_before: self.initial.alignment.aln_span().format(genome).to_string(),
            aligned_read_len_before,
            aln_after,
            aligned_read_len_after,

            unique_part,
            total_psvs,
            pair_psvs,
            conflicts: format!("{}/{}", self.conflict_psvs, self.appl_psvs),

            mapq_before: self.initial.mapping_quality,
            mapq_after: self.mapping_quality.0,
            reason: self.mapping_quality.1,
            correct_before,
            correct_after,
        })
    }

    pub fn update_window_coverage_map(&self, coverage_map: &mut HashMap<WindowId, [f64; 2]>,
            database: &HeavyDatabase) {
        let locations = if self.lazy_alignments.is_empty() {
            vec![self.initial.alignment.aln_span().clone()]
        } else if self.mapping_quality.0 >= 30 && self.best_is_defined {
            self.lazy_alignments[self.ordered_locs[0] as usize].spans()
        } else {
            self.lazy_alignments.iter().flat_map(|lazy_aln| lazy_aln.spans()).collect()
        };
        let value = 1.0 / locations.len() as f64;

        for location in locations {
            for window_id in database.find_window_ids(&location) {
                let window = database.window_at(window_id);
                for i in 0..2 {
                    if location.intersects(&window[i]) {
                        coverage_map.entry(window_id).or_insert_with(|| [0.0, 0.0])[i] += value;
                    }
                }
            }
        }
    }

    pub fn reduce_mapq_if_conflicting(&mut self, conflict_rate: f64, p_threshold: f64) {
        const CONFLICTING_MAPQ: u8 = 5;
        if self.mapping_quality.0 <= CONFLICTING_MAPQ || self.conflict_psvs == 0 {
            return;
        }
        let p_value = binomial_test(self.conflict_psvs, self.appl_psvs, conflict_rate);
        let perc = 100.0 * self.conflict_psvs as f64 / self.appl_psvs as f64;
        if p_value < p_threshold {
            debug!("        Read {} contradicts {} PSVs out of {} ({:.1}%). \
                Binomial p-value = {:.3e}. MAPQ {} -> {}",
                self.name, self.conflict_psvs, self.appl_psvs, perc, p_value,
                self.mapping_quality.0, CONFLICTING_MAPQ);
            self.mapping_quality = (CONFLICTING_MAPQ, Reason::Conflicts);
        } else if perc >= conflict_rate * 100.0 {
            trace!("        [not significant] Read {} contradicts {} PSVs out of {} ({:.1}%). \
                Binomial p-value = {:.3e}. MAPQ: {}",
                self.name, self.conflict_psvs, self.appl_psvs, perc, p_value,
                self.mapping_quality.0);
        }
    }

    pub fn update_genotype_probabilities(&mut self, database: &HeavyDatabase, params: &Parameters)
            -> Vec<GenotypeUpdate> {
        if self.mapping_quality.0 < params.genotyping_min_mapq {
            return Vec::with_capacity(0);
        }

        if !self.best_is_defined {
            trace!("        Skipping (no best alignment)");
            return Vec::with_capacity(0);
        }

        let ialns = &self.lazy_alignments[self.ordered_locs[0] as usize].get();
        trace!("        Best location has {} alignments. (MAPQ: {})", ialns.len(), self.mapping_quality.0);
        let mut res = Vec::new();
        for ialn in ialns.iter() {
            let aln_span = ialn.alignment().aln_span();

            let mut cache = &mut self.realignment_cache[self.ordered_locs[0] as usize];
            for (psv_span, psv_id) in database.find_psvs(aln_span) {
                let psv = database.psv_at(psv_id);
                let i = psv.corresponds_to(&psv_span).unwrap();
                let span = psv.span(i);
                let (q_start, q_seq) = ialn.subsequence(span);
                let allele_probs = get_or_realign(q_start, &q_seq, psv_id, &psv,
                    &mut cache, &params.hmm_params);
                res.push(GenotypeUpdate::new(psv_id, i,
                    max(params.read_psv_impact, allele_probs[i]),
                    max(params.read_psv_impact, allele_probs[1 - i])));
            }
        }
        res
    }

    pub fn count_conflicts(&mut self, database: &HeavyDatabase, genome: &Genome,
            params: &Parameters, writer: &mut Option<CsvGzWriter>) -> (u32, u32) {
        if !self.best_is_defined {
            self.conflict_psvs = 0;
            self.appl_psvs = 0;
            return (0, 0);
        }

        let mut appl_psvs = 0;
        let mut conflict_psvs = 0;
        let mut skipped_psvs = 0;
        for ialn in self.lazy_alignments[self.ordered_locs[0] as usize].get().iter() {
            let aln_span = ialn.alignment().aln_span();
            for (psv_span, psv_id) in database.find_psvs(aln_span) {
                let psv = database.psv_at(psv_id);
                let i = psv.corresponds_to(&psv_span).unwrap();
                let span = psv.span(i);

                if !aln_span.intersects(span) {
                    continue;
                }
                let use_psv = psv.genotype(i).use_for_conflicts();
                if !use_psv {
                    skipped_psvs += 1;
                    if writer.is_none() {
                        continue;
                    }
                }

                let best_genotype = psv.genotype(i).best_genotype();
                let genotype_quality = psv.genotype(i).genotype_quality();
                let (q_start, q_seq) = ialn.subsequence(span);
                let allele_probs = get_or_realign(q_start, &q_seq, psv_id, &psv,
                    &mut self.realignment_cache[self.ordered_locs[0] as usize], &params.hmm_params);
                let prob0 = allele_probs[i];
                let prob1 = allele_probs[1 - i];

                if let Some(ref mut output) = writer {
                    let mut psv_id = psv.hash();
                    write!(psv_id, ":{}", i).unwrap();

                    let forward_seq = if span.strand() {
                        q_seq
                    } else {
                        q_seq.reverse_complement()
                    };
                    let row = ReadPsvsRow {
                        name: &self.name,
                        chrom: genome.chrom_name(span.chrom_id()),
                        pos: psv.var_span(i).start() + 1,
                        psv_id,
                        gt: best_genotype,
                        gt_qual: genotype_quality.min(std::u8::MAX as f64) as u8,
                        prob0: Precision3(prob0.log10()),
                        prob1: Precision3(prob1.log10()),
                        seq: forward_seq.to_str(),
                    };
                    output.serialize(&row)
                        .unwrap_or_else(|e| error!("Failed to write read PSVs: {}", e));
                }

                if use_psv && (max(prob0, prob1) / min(prob0, prob1)).real() >= CONFLICT_PROB_DIFF {
                    appl_psvs += 1;
                    if (best_genotype == GenotypeValue::HomRef && prob1 > prob0)
                            || (best_genotype == GenotypeValue::HomAlt && prob0 > prob1) {
                        conflict_psvs += 1;
                    }
                }
            }
        }
        self.conflict_psvs = conflict_psvs;
        self.appl_psvs = appl_psvs;
        debug!("        Read {} conflicts with {} PSVs out of {} ({:.1}%), skipped {} PSVs",
            self.name, conflict_psvs, appl_psvs, 100.0 * conflict_psvs as f32 / appl_psvs as f32, skipped_psvs);
        (conflict_psvs, appl_psvs)
    }

    pub fn align_secondary(&mut self, genome: &mut Genome, params: &Parameters) {
        for i in (0..min(params.n_secondary as usize + 1, self.ordered_locs.len())).rev() {
            let loc = self.ordered_locs[i] as usize;
            if !self.lazy_alignments[loc].is_location() {
                continue;
            }
            debug!("        Location {} is not aligned yet, aligning", loc);
            let hard_clipping =
                (self.initial.alignment.cigar().hard_clipping(true),
                self.initial.alignment.cigar().hard_clipping(false));
            let could_align = if !self.lazy_alignments[loc].try_upgrade(loc,
                    Arc::clone(&self.initial.sequence), hard_clipping, genome, params) {
                false
            } else {
                !should_remove(&self.lazy_alignments, loc, genome)
            };

            if !could_align {
                debug!("        Could not align location {}, removing it", loc);
                self.lazy_alignments.remove(loc);
                self.realignment_cache.remove(loc);
                self.ordered_locs.remove(i);
            }
        }
    }

    fn fill_record(&self, record: &mut bam::Record, alignment: &Alignment,
            id_converter: &IdConverter, genome: &Genome, db_name: &str,
            primary: bool, is_original: bool) -> Result<(), String> {
        record.clear();
        record.set_name(self.name.bytes());
        record.flag_mut().set_secondary(!primary);
        record.flag_mut().set_supplementary(alignment.supplementary);

        record.set_cigar(alignment.cigar().to_string().bytes())
            .map_err(|e| format!("Could not set cigar: {}", e))?;
        if !is_original && !alignment.strand() {
            record.set_seq_qual(self.initial.sequence.rev_compl(..),
                self.initial.qualities.raw().iter().rev().cloned())
                .map_err(|e| format!("Could not set sequence and qualities: {}", e))?;
        } else {
            record.set_raw_seq_qual(self.initial.sequence.raw(),
                self.initial.qualities.raw().iter().cloned(),
                self.initial.sequence.len())
                .map_err(|e| format!("Could not set sequence and qualities: {}", e))?;
        }
        let strand = if is_original {
            alignment.strand()
        } else {
            self.initial.alignment.strand() == alignment.strand()
        };
        record.flag_mut().set_strand(strand);

        record.set_ref_id(id_converter.from_main(alignment.aln_span().chrom_id())
            .ok_or_else(|| "Cannot write read: Chromosome not in the input bam header".to_string())?
            as i32);
        record.set_start(alignment.aln_span().start() as i32);
        record.set_mapq(if primary { self.mapping_quality.0 } else { 0 });

        if let Some(group) = &self.initial.rg_tag {
            record.tags_mut().push_string(b"RG", group);
        }
        if alignment.score >= 0 {
            record.tags_mut().push_num(b"AS", alignment.score);
        }
        if alignment.edit_distance >= 0 {
            record.tags_mut().push_num(b"NM", alignment.edit_distance);
        }

        if primary {
            record.tags_mut().push_string(b"re", generate_tag(&self.initial.alignment, alignment,
                self.initial.mapping_quality, &self.mapping_quality).as_bytes());
        }
        record.tags_mut().push_string(b"oa",
            self.initial.alignment.aln_span().format(genome).to_string().as_bytes());
        record.tags_mut().push_num(b"om", self.initial.mapping_quality);
        if primary && self.appl_psvs > 0 {
            record.tags_mut().push_array(b"co", &[self.conflict_psvs, self.appl_psvs]);
        }
        record.tags_mut().push_string(b"db", db_name.as_bytes());
        record.tags_mut().push_char(b"tp", if primary { b'P' } else { b'S' });
        Ok(())
    }

    pub fn write(&self, record: &mut bam::Record, id_converter: &IdConverter, genome: &Genome,
            db_name: &str, writer: &mut impl RecordWriter, n_secondary: u16) -> Result<(), String> {
        let mut corresponds_to_original = std::u16::MAX;
        let mut ordered_locs = Vec::new();

        const BEST_ORIGINAL: u16 = std::u16::MAX;
        if !self.best_is_defined {
            let mut best_intersection = 0;
            for (i, lazy_aln) in self.lazy_alignments.iter().enumerate() {
                let intersection = match lazy_aln {
                    LazyAlignment::Aligned(_) => lazy_aln.get_primary().alignment().aln_span()
                        .intersection_size(self.initial.alignment.aln_span()),
                    LazyAlignment::Location(loc) => loc.intersection_size(
                        self.initial.alignment.aln_span()),
                };
                if intersection > best_intersection {
                    best_intersection = intersection;
                    corresponds_to_original = i as u16;
                }
            }
            ordered_locs.push(BEST_ORIGINAL);
        }
        ordered_locs.extend(self.ordered_locs.iter().cloned().filter(|&i| i != corresponds_to_original)
            .take(n_secondary as usize + self.best_is_defined as usize));

        let mut se_flags = Vec::new();
        if ordered_locs.len() > 1 {
            for &loc in ordered_locs.iter() {
                if loc == BEST_ORIGINAL {
                    se_flags.push(self.initial.alignment.aln_span().format(genome).to_string());
                } else {
                    se_flags.push(self.lazy_alignments[loc as usize].get_primary()
                       .alignment().aln_span().format(genome).to_string());
                }
            }
        }

        for (i, &loc) in ordered_locs.iter().enumerate() {
            let mut sa_flags = Vec::new();
            if loc != BEST_ORIGINAL {
                let aln_parts = self.lazy_alignments[loc as usize].get();
                if aln_parts.len() > 1 {
                    sa_flags.extend(aln_parts.iter().map(|ialn| {
                            let aln = ialn.alignment();
                            format!("{},{},{},{},{},{};", genome.chrom_name(aln.aln_span().chrom_id()),
                                aln.aln_span().start() + 1,
                                if self.initial.alignment.strand() == aln.strand() { '+' } else { '-' },
                                aln.cigar(), self.mapping_quality.0, aln.edit_distance)
                        }));
                }
            }

            let alignments = if loc == BEST_ORIGINAL {
                vec![&self.initial.alignment]
            } else {
                self.lazy_alignments[loc as usize].get().iter().map(|aln_part| aln_part.alignment()).collect()
            };

            for (part_ind, alignment) in alignments.iter().enumerate() {
                let primary = i == 0;
                self.fill_record(record, alignment, id_converter, genome, db_name, primary, loc == BEST_ORIGINAL)?;

                if !sa_flags.is_empty() {
                    let mut tag = String::new();
                    for (j, sa) in sa_flags.iter().enumerate() {
                        if part_ind != j {
                            tag += sa;
                        }
                    }
                    record.tags_mut().push_string(b"SA", tag.as_bytes());
                }
                if !alignment.supplementary && !se_flags.is_empty() {
                    let tag = se_flags.iter().enumerate()
                        .filter_map(|(j, se)| if i == j { None } else { Some(se as &str) })
                        .collect::<Vec<_>>().join(",");
                    record.tags_mut().push_string(b"se", tag.as_bytes());
                }

                writer.write(record).map_err(|e| format!("Cannot write a record: {}", e))?;
            }
        }
        Ok(())
    }

    pub fn mapping_quality(&self) -> u8 {
        self.mapping_quality.0
    }

    pub fn best_location(&self) -> Option<(u16, u8)> {
        if self.best_is_defined {
            Some((self.ordered_locs[0], self.mapping_quality.0))
        } else {
            None
        }
    }
}
