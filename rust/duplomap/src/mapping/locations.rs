use std::collections::HashMap;
use std::cmp::{min, max};

use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan};
use biology::Genome;
use database::heavy_database::{HeavyDatabase, WindowId};
use alignment::Alignment;

struct AlignedWindow {
    span: DirectionalSpan,
    window_id: WindowId,
    aln_same_strand: bool,
    dist_to_start: u32,
    dist_to_end: u32,
    aln_index: u32,
}

impl AlignedWindow {
    fn extend(windows: &mut Vec<AlignedWindow>, span: &PositiveSpan, window_id: WindowId,
            alignment: &Alignment, aln_indices: &HashMap<WindowId, Vec<u32>>,
            database: &HeavyDatabase, padding: u32) {
        let window = database.window_at(window_id);
        let original_window = if span.undirectional_eq(&window[0]) {
            &window[0]
        } else {
            assert!(span.undirectional_eq(&window[1]), "Span does not correspond to the WindowId");
            &window[1]
        };

        let aln_span = alignment.aln_span();
        let query_range = alignment.query_range();
        let dist_to_start = original_window.start().saturating_sub(aln_span.start())
            + query_range.start() + padding;
        let dist_to_end = aln_span.end().saturating_sub(original_window.end())
            + alignment.query_len() - query_range.end() + padding;

        for &aln_index in aln_indices[&window_id].iter() {
            for i in 0..2 {
                let aln_same_strand = window[i].strand() == original_window.strand();
                windows.push(AlignedWindow {
                    span: window[i].clone(),
                    dist_to_start: if aln_same_strand { dist_to_start } else { dist_to_end },
                    dist_to_end: if aln_same_strand { dist_to_end } else { dist_to_start },
                    aln_same_strand, window_id, aln_index,
                });
            }
        }
    }
}

impl std::fmt::Debug for AlignedWindow {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?} W{}, Aln index {}, (distances {} {})",
            self.span, self.window_id.index(), self.aln_index, self.dist_to_start, self.dist_to_end)
    }
}

fn merged_len(left: &AlignedWindow, right: &AlignedWindow) -> u32 {
    if left.span.chrom_id() != right.span.chrom_id() {
        std::u32::MAX
    } else if left.aln_same_strand {
        right.span.end() + right.dist_to_end + left.dist_to_start - left.span.start()
    } else {
        left.span.end() + left.dist_to_end + right.dist_to_start - right.span.start()
    }
}

fn combine(left: &AlignedWindow, right: &AlignedWindow) -> OverflowSpan {
    if left.aln_same_strand {
        OverflowSpan {
            span: DirectionalSpan::new(left.span.chrom_id(),
                left.span.start().saturating_sub(left.dist_to_start),
                right.span.end() + right.dist_to_end, true),
            left_clipping: left.dist_to_start.saturating_sub(left.span.start()),
            right_clipping: 0,
        }
    } else {
        OverflowSpan {
            span: DirectionalSpan::new(left.span.chrom_id(),
                right.span.start().saturating_sub(right.dist_to_start),
                left.span.end() + left.dist_to_end, false),
            left_clipping: right.dist_to_start.saturating_sub(right.span.start()),
            right_clipping: 0,
        }
    }
}

// res: Vec<(location, n_windows)>
fn find_monotonic_sequences(aligned_windows: &[&AlignedWindow], min_windows: usize, max_size: u32,
        res: &mut Vec<(OverflowSpan, usize)>) {
    let n = aligned_windows.len();
    let mut j0 = 0;

    // for (i, window) in aligned_windows.iter().enumerate() {
    //     trace!("        {}: {:?}", i, window);
    // }

    for i in 0..n {
        for j in (max(j0, i + min_windows - 1)..n).rev() {
            let left = &aligned_windows[i];
            let right = &aligned_windows[j];
            if left.aln_index < right.aln_index || i == j {
                let new_len = merged_len(left, right);
                if new_len <= max_size {
                    trace!("        Using pair {} {}: {:?} AND {:?} (len: {})",
                        i, j, left, right, new_len);
                    res.push((combine(left, right), j - i));
                    j0 = j + 1;
                    break;
                } // else if new_len <= 2 * max_size {
                //    trace!("        Pair {} {} is too long: {} > {}", i, j, new_len, max_size);
                // }
            }
        }
    }
}

fn extend_short_locations(locations: &mut Vec<OverflowSpan>, genome: &Genome) {
    let max_span_len = match locations.iter().map(|loc| loc.span.len()).max() {
        Some(value) => value,
        None => return,
    };

    let mut min_left_clip = 0;
    let mut min_right_clip = 0;
    for loc in locations.iter_mut() {
        let chrom_len = genome.chrom_len(loc.span.chrom_id());
        if max_span_len > loc.span.len() || loc.span.end() > chrom_len {
            let add_left = (max_span_len - loc.span.len()) / 2;
            let add_right = max_span_len - loc.span.len() - add_left;

            let new_start = loc.span.start().saturating_sub(add_left);
            let new_end = loc.span.end() + add_right;
            loc.left_clipping += add_left.saturating_sub(loc.span.start());
            loc.right_clipping += new_end.saturating_sub(chrom_len);

            loc.span = DirectionalSpan::new(loc.span.chrom_id(),
                new_start, min(new_end, chrom_len), loc.span.strand());
        }
        min_left_clip = min(min_left_clip, loc.left_clipping);
        min_right_clip = min(min_right_clip, loc.right_clipping);
    }

    if min_left_clip > 0 || min_right_clip > 0 {
        for loc in locations.iter_mut() {
            loc.left_clipping -= min_left_clip;
            loc.right_clipping -= min_right_clip;
        }
    }
}

fn calculate_min_num_of_windows(windows: &[(PositiveSpan, WindowId)]) -> usize {
    let mut last_window = &windows[0].0;
    let mut count = 0;
    for (window, _) in windows.iter() {
        if window.start() >= last_window.end() {
            last_window = window;
            count += 1;
        }
    }
    max(1, count / 2)
}

pub struct OverflowSpan {
    pub span: DirectionalSpan,
    pub left_clipping: u32,
    pub right_clipping: u32,
}

pub fn find_all_locations(alignment: &Alignment, database: &HeavyDatabase,
        genome: &Genome, relative_padding: f32) -> Vec<OverflowSpan> {
    debug!("    Looking for possible locations");
    let aln_span = alignment.aln_span();
    let query_len = alignment.query_len();
    let query_range = alignment.query_range();
    let mut windows: Vec<_> = database.find_windows(aln_span).collect();
    assert!(!windows.is_empty(), "Read does not intersect any pairwise windows");

    trace!("        Original alignment intersects {} windows", windows.len());
    windows.sort_by(|a, b| a.0.start_comparator().cmp(&b.0.start_comparator()));
    let left_window = &windows[0].0;
    let right_window = &windows[windows.len() - 1].0;
    let aln_len = query_len + query_range.start() - query_range.end()
        + max(aln_span.end(), right_window.end())
        - min(aln_span.start(), left_window.start());
    let padding = (query_len as f32 * relative_padding) as u32;

    let min_windows = calculate_min_num_of_windows(&windows);
    // let min_windows_span = right_window.end().saturating_sub(left_window.start()) / 2;
    let max_size = (1.2 * (max(query_len, aln_len) + padding * 4) as f32) as u32;
    trace!("    Padding: {}, Max location size: {}", padding, max_size);

    let mut aln_indices = HashMap::with_capacity(windows.len());
    for (i, (_span, window_id)) in windows.iter().enumerate() {
        aln_indices.entry(*window_id).or_insert_with(|| Vec::with_capacity(1)).push(i as u32);
    }

    let mut aligned_windows = Vec::new();
    for (span, window_id) in windows.iter() {
        AlignedWindow::extend(&mut aligned_windows, span, *window_id, &alignment, &aln_indices,
            database, padding);
    }
    aligned_windows.sort_by(|a, b| (a.span.chrom_id(), a.span.start(), a.aln_index)
        .cmp(&(b.span.chrom_id(), b.span.start(), b.aln_index)));

    let mut locations = Vec::new();
    trace!("        Windows +");
    let pos_windows: Vec<_> = aligned_windows.iter()
        .filter(|aligned_window| aligned_window.aln_same_strand).collect();
    find_monotonic_sequences(&pos_windows, min_windows, max_size, &mut locations);

    trace!("        Windows -");
    let neg_windows: Vec<_> = aligned_windows.iter().rev()
        .filter(|aligned_window| !aligned_window.aln_same_strand).collect();
    find_monotonic_sequences(&neg_windows, min_windows, max_size, &mut locations);
    assert!(!locations.is_empty(), "Found 0 locations!");
    locations.sort_by(|a, b| b.1.cmp(&a.1));
    let mut locations = locations.into_iter().map(|(loc, _)| loc).collect();
    extend_short_locations(&mut locations, genome);
    locations
}
