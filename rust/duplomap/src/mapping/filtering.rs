use std::cmp::{min, max, Ordering};
use std::fmt::{Display, Formatter, Write as fmtWrite};
use std::ops::{Range, RangeBounds};

use statrs::function::gamma::ln_gamma;
use statrs::distribution::{ChiSquared, Univariate};
use petgraph::graph::{DiGraph, NodeIndex};
use petgraph::algo::tarjan_scc;

use biology::spans::{DirectionalSpan, Span, SpanTools};
use biology::genome::{self, Genome, Kmer};
use mapping::Parameters;
use mapping::locations::OverflowSpan;
use utils::common::adjust_window_size;

fn matrix_ln_prob(a: f64, b: f64, c: f64, d: f64) -> f64 {
    ln_gamma(a + b + 1.0) + ln_gamma(c + d + 1.0) + ln_gamma(a + c + 1.0) + ln_gamma(b + d + 1.0)
        - ln_gamma(a + 1.0) - ln_gamma(b + 1.0) - ln_gamma(c + 1.0) - ln_gamma(d + 1.0)
        - ln_gamma(a + b + c + d + 1.0)
}

fn fisher_exact(a11: u32, a12: u32, a21: u32, a22: u32) -> f64 {
    // if large - chi squared
    let observed_prob = matrix_ln_prob(a11 as f64, a12 as f64, a21 as f64, a22 as f64);

    let mut res = 0.0;
    for a in a11.saturating_sub(a22)..1 + a11 + min(a12, a21) {
        let b = a11 + a12 - a;
        let c = a11 + a21 - a;
        let d = a22 + a - a11;
        let current = matrix_ln_prob(a as f64, b as f64, c as f64, d as f64);
        if current <= observed_prob {
            res += current.exp();
        }
    }
    res
}

fn chi_squared(a11: u32, a12: u32, a21: u32, a22: u32) -> f64 {
    let observed = [a11 as f64, a12 as f64, a21 as f64, a22 as f64];
    let sum_rows = [observed[0] + observed[1], observed[2] + observed[3]];
    let sum_cols = [observed[0] + observed[2], observed[1] + observed[3]];
    let total = sum_rows[0] + sum_rows[1];
    let statistic = (0..4).map(|i| {
        let expected = sum_rows[i / 2] * sum_cols[i % 2] / total;
        (observed[i] - expected).powi(2) / expected
    }).sum();
    1.0 - ChiSquared::new(1.0).unwrap().cdf(statistic)
}

fn homogeneity_test(a11: u32, a12: u32, a21: u32, a22: u32) -> f64 {
    const THRESHOLD: u32 = 10;
    if a11 >= THRESHOLD && a12 >= THRESHOLD && a21 >= THRESHOLD && a22 >= THRESHOLD {
        chi_squared(a11, a12, a21, a22)
    } else {
        fisher_exact(a11, a12, a21, a22)
    }
}

type Matches = Vec<(u32, u32)>;

fn construct_kmer_matches(kmers1: &[Kmer], kmers2: &[Kmer]) -> Matches {
    let mut matches = Vec::new();
    let mut start1 = 0;
    let mut start2 = 0;
    while start1 < kmers1.len() && start2 < kmers2.len() {
        let kmer1 = kmers1[start1].kmer;
        let kmer2 = kmers2[start2].kmer;

        if kmer1 < kmer2 {
            start1 += 1;
            continue;
        }
        if kmer2 < kmer1 {
            start2 += 1;
            continue
        }

        let mut end1 = start1 + 1;
        while end1 < kmers1.len() && kmers1[end1].kmer == kmer1 {
            end1 += 1;
        }
        let mut end2 = start2 + 1;
        while end2 < kmers2.len() && kmers2[end2].kmer == kmer2 {
            end2 += 1;
        }

        for i in start1..end1 {
            for j in start2..end2 {
                matches.push((kmers1[i].index, kmers2[j].index));
            }
        }
        start1 = end1;
        start2 = end2;
    }
    matches.sort();
    matches
}

fn lcs_to_strings(lcs1: &[(u32, u32)], lcs2: &[(u32, u32)], length: usize) -> (String, String) {
    let window = adjust_window_size(length, 500);
    let n_windows = (length - 1) / window + 1;

    let mut values = vec![0; 2 * n_windows];
    for &(read_pos, _) in lcs1.iter() {
        values[read_pos as usize / window] += 1;
    }
    for &(read_pos, _) in lcs2.iter() {
        values[n_windows + read_pos as usize / window] += 1;
    }

    let mut res1 = String::new();
    let mut res2 = String::new();
    for i in 0..n_windows {
        let a = values[i];
        let b = values[i + n_windows];
        let m = max(a, b);
        if m == 0 {
            res1.push(',');
            res2.push(',');
        } else if m < 10 {
            write!(res1, "{},", a).unwrap();
            write!(res2, "{},", b).unwrap();
        } else if m < 100 {
            write!(res1, "{:2},", a).unwrap();
            write!(res2, "{:2},", b).unwrap();
        } else {
            write!(res1, "{:3},", a).unwrap();
            write!(res2, "{:3},", b).unwrap();
        }
    }
    res1.pop();
    res2.pop();
    (res1, res2)
}

#[derive(Clone)]
enum UnknownRegion {
    None,
    // Unknown region is on the left, store end: u32.
    Left(u32),
    // Unknown region is on the left, store start: u32.
    Right(u32),
    // Unknown regions on both ends, store (end of left, start of right).
    Both(u32, u32),
}

impl UnknownRegion {
    fn is_none(&self) -> bool {
        match self {
            UnknownRegion::None => true,
            _ => false,
        }
    }

    fn is_some(&self) -> bool {
        match self {
            UnknownRegion::None => false,
            _ => true,
        }
    }

    fn left_unknown(&self) -> bool {
        match self {
            UnknownRegion::Left(_) | UnknownRegion::Both(_, _) => true,
            _ => false,
        }
    }

    fn right_unknown(&self) -> bool {
        match self {
            UnknownRegion::Right(_) | UnknownRegion::Both(_, _) => true,
            _ => false,
        }
    }
}

impl Display for UnknownRegion {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            UnknownRegion::None => write!(f, "None"),
            UnknownRegion::Left(end) => write!(f, "..{}", end),
            UnknownRegion::Right(start) => write!(f, "{}..", start),
            UnknownRegion::Both(l_end, r_start) => write!(f, "[..{}, {}..]", l_end, r_start),
        }
    }
}

struct Location {
    index: usize,
    // Location k-mers, sorted by kmer value, then by index.
    kmers: Vec<Kmer>,
    // Soretd location k-mer positions.
    kmer_pos: Vec<u32>,
    // lcs between read and the location: (read_pos, loc_pos).
    read_lcs: Vec<(u32, u32)>,
    // Regions in the location that contain many Ns.
    unknown_region: UnknownRegion,
}

impl Location {
    fn new(index: usize, location: &OverflowSpan, query_kmers: &[Kmer],
            genome: &mut Genome, k: u32) -> Option<Self> {
        let mut kmers = Vec::new();
        let mut kmer_pos = Vec::new();
        let loc_seq = genome.fetch(&location.span);
        let mut exp_index = 0;
        let mut unknown_region = UnknownRegion::None;
        let mut n_kmers = 0;
        for mut kmer in genome::kmers_without_n(loc_seq.iter().cloned(), k as usize) {
            kmer.index += location.left_clipping;
            n_kmers += kmer.index - exp_index;
            if kmer.index - exp_index >= 2 * k {
                if exp_index > 0 {
                    debug!("        Location has unknown region in the middle: {}-{}. \
                        Skipping the read", exp_index, kmer.index);
                    return None;
                }
                unknown_region = UnknownRegion::Left(kmer.index);
            }

            kmer_pos.push(kmer.index);
            kmers.push(kmer);
            exp_index = kmer.index + 1;
        }
        let len = location.span.len() + location.left_clipping + location.right_clipping;
        let end_index = (len + 1).saturating_sub(k);
        n_kmers += end_index - exp_index;

        if end_index - exp_index >= 2 * k {
            unknown_region = match unknown_region {
                UnknownRegion::None => UnknownRegion::Right(exp_index),
                UnknownRegion::Left(end) => UnknownRegion::Both(end, exp_index),
                _ => unreachable!(),
            };
        }
        kmers.sort_by(Kmer::kmer_first);

        let matches = construct_kmer_matches(query_kmers, &kmers);
        let lcs = bio::alignment::sparse::lcskpp(&matches, k as usize);
        let read_lcs: Vec<_> = lcs.path.iter().map(|&i| matches[i]).collect();

        if n_kmers > 0 {
            debug!("            lcs: {}. k-mers with N: {}. Unknown region: {}",
                read_lcs.len(), n_kmers, unknown_region);
        } else {
            debug!("            lcs: {}", read_lcs.len());
        }
        Some(Location { index, kmers, kmer_pos, read_lcs, unknown_region })
    }
}

fn difference<T, F: Fn(&T) -> u32>(a: &[T], b: &[T], transform: F, k: u32) -> UniqueCounts {
    let mut i = 0;
    let mut j = 0;
    let mut res = UniqueCounts::default();
    let mut next_x = 0;
    let mut next_y = 0;

    while i < a.len() && j < b.len() {
        let x = transform(&a[i]);
        let y = transform(&b[j]);
        match x.cmp(&y) {
            Ordering::Equal => {
                i += 1;
                j += 1;
            },
            Ordering::Less => {
                i += 1;
                res.all[0] += 1;
                if x >= next_x {
                    res.non_ov[0] += 1;
                    next_x = x + k;
                }
            },
            Ordering::Greater => {
                j += 1;
                res.all[1] += 1;
                if y >= next_y {
                    res.non_ov[1] += 1;
                    next_y = y + k;
                }
            },
        }
    }

    res.all[0] += (a.len() - i) as u32;
    for x in a[i..].iter().map(&transform) {
        if x >= next_x {
            res.non_ov[0] += 1;
            next_x = x + k;
        }
    }
    res.all[1] += (b.len() - j) as u32;
    for y in b[j..].iter().map(&transform) {
        if y >= next_y {
            res.non_ov[1] += 1;
            next_y = y + k;
        }
    }
    res
}

// LCS: (read_pos, loc_pos). Returns two loc positions. If no matches: returns u32::MAX.
fn first_matching_position(lcs_i: &[(u32, u32)], lcs_j: &[(u32, u32)]) -> (u32, u32) {
    let mut i = 0;
    let mut j = 0;
    while i < lcs_i.len() && j < lcs_j.len() {
        match lcs_i[i].0.cmp(&lcs_j[j].0) {
            Ordering::Equal => {
                return (lcs_i[i].1, lcs_j[j].1);
            },
            Ordering::Less => {
                i += 1;
            },
            Ordering::Greater => {
                j += 1;
            },
        }
    }
    (std::u32::MAX, std::u32::MAX)
}

// LCS: (read_pos, loc_pos). Returns two loc positions. If no matches: returns 0.
fn after_last_matching_position(lcs_i: &[(u32, u32)], lcs_j: &[(u32, u32)]) -> (u32, u32) {
    let mut i = lcs_i.len();
    let mut j = lcs_j.len();
    while i > 0 && j > 0 {
        match lcs_i[i - 1].0.cmp(&lcs_j[j - 1].0) {
            Ordering::Equal => {
                return (lcs_i[i - 1].1 + 1, lcs_j[j - 1].1 + 1);
            },
            Ordering::Less => {
                j -= 1;
            },
            Ordering::Greater => {
                i -= 1;
            },
        }
    }
    (0, 0)
}

// Returns ranges of location positions for each location that can be used.
fn get_loc_known_regions(loc_i: &Location, loc_j: &Location) -> [Range<u32>; 2] {
    let (start1, start2) = if loc_i.unknown_region.left_unknown()
            || loc_j.unknown_region.left_unknown() {
        first_matching_position(&loc_i.read_lcs, &loc_j.read_lcs)
    } else {
        (0, 0)
    };
    let (end1, end2) = if loc_i.unknown_region.right_unknown()
            || loc_j.unknown_region.right_unknown() {
        after_last_matching_position(&loc_i.read_lcs, &loc_j.read_lcs)
    } else {
        (std::u32::MAX, std::u32::MAX)
    };
    [start1..end1, start2..end2]
}

fn not_on_lcs<I, R>(kmer_pos: &[u32], mut lcs_pos: I, k: u32, incl_range: R) -> u32
where I: Iterator<Item = u32>,
    R: RangeBounds<u32>
{
    let mut next_pos = 0;
    let mut curr_lcs = lcs_pos.next();
    let mut res = 0;

    for &pos in kmer_pos.iter() {
        match curr_lcs {
            Some(value) => {
                assert!(pos <= value);
                if value == pos {
                    curr_lcs = lcs_pos.next();
                    continue;
                }
            },
            None => {},
        }
        if pos >= next_pos && incl_range.contains(&pos) {
            res += 1;
            next_pos = pos + k;
        }
    }
    res
}

#[derive(Default)]
struct UniqueCounts {
    all: [u32; 2],
    non_ov: [u32; 2],
}

struct Counts {
    loc_read: UniqueCounts,
    loc_loc: UniqueCounts,
}

impl Counts {
    fn ratio(&self, i: usize) -> f32 {
        self.loc_read.non_ov[i] as f32 / self.loc_loc.non_ov[i] as f32
    }

    fn ratio_all(&self, i: usize) -> f32 {
        self.loc_read.all[i] as f32 / self.loc_loc.all[i] as f32
    }
}

fn count_unique_with_ns(loc_i: &Location, loc_j: &Location, k: u32) -> Counts {
    let i = loc_i.index;
    let j = loc_j.index;
    let [known_pos_i, known_pos_j] = get_loc_known_regions(loc_i, loc_j);
    debug!("            Keep positions in loc-{}: {:?}, loc-{}: {:?}",
        i, known_pos_i, j, known_pos_j);

    let kmers_i: Vec<_> = loc_i.kmers.iter().filter(|kmer| known_pos_i.contains(&kmer.index))
        .cloned().collect();
    let kmers_j: Vec<_> = loc_j.kmers.iter().filter(|kmer| known_pos_j.contains(&kmer.index))
        .cloned().collect();
    let matches = construct_kmer_matches(&kmers_i, &kmers_j);
    let lcs = bio::alignment::sparse::lcskpp(&matches, k as usize);

    let lcs_ir: Vec<_> = loc_i.read_lcs.iter().filter_map(|(read_pos, loc_pos)|
        if known_pos_i.contains(&loc_pos) { Some(read_pos) } else { None }).cloned().collect();
    let lcs_jr: Vec<_> = loc_j.read_lcs.iter().filter_map(|(read_pos, loc_pos)|
        if known_pos_j.contains(&loc_pos) { Some(read_pos) } else { None }).cloned().collect();
    debug!("            lcs(L{},R) was reduced by {} and lcs(L{},R) by {}",
        i, loc_i.read_lcs.len() - lcs_ir.len(), j, loc_j.read_lcs.len() - lcs_jr.len());

    let mut loc_loc = UniqueCounts::default();
    loc_loc.all[0] = (kmers_i.len() - lcs.path.len()) as u32;
    loc_loc.all[1] = (kmers_j.len() - lcs.path.len()) as u32;
    loc_loc.non_ov[0] = not_on_lcs(&loc_i.kmer_pos, lcs.path.iter().map(|&i| matches[i].0), k, known_pos_i);
    loc_loc.non_ov[1] = not_on_lcs(&loc_j.kmer_pos, lcs.path.iter().map(|&i| matches[i].1), k, known_pos_j);

    Counts {
        loc_read: difference(&lcs_ir, &lcs_jr, |x| *x, k),
        loc_loc,
    }
}

fn count_unique_without_ns(loc_i: &Location, loc_j: &Location, k: u32) -> Counts {
    let matches = construct_kmer_matches(&loc_i.kmers, &loc_j.kmers);
    let lcs = bio::alignment::sparse::lcskpp(&matches, k as usize);
    let mut loc_loc = UniqueCounts::default();
    loc_loc.all[0] = (loc_i.kmers.len() - lcs.path.len()) as u32;
    loc_loc.all[1] = (loc_j.kmers.len() - lcs.path.len()) as u32;
    loc_loc.non_ov[0] = not_on_lcs(&loc_i.kmer_pos, lcs.path.iter().map(|&i| matches[i].0), k, ..);
    loc_loc.non_ov[1] = not_on_lcs(&loc_j.kmer_pos, lcs.path.iter().map(|&i| matches[i].1), k, ..);
    Counts {
        loc_read: difference(&loc_i.read_lcs, &loc_j.read_lcs, |(read_pos, _loc_pos)| *read_pos, k),
        loc_loc,
    }
}

fn can_use_p_value(locs: &[&Location; 2], counts: &Counts) -> bool {
    let best = if counts.ratio(0) > counts.ratio(1) { 0 } else { 1 };
    let worst = 1 - best;
    let i = locs[best].index;
    let j = locs[worst].index;

    if counts.loc_read.all[worst] >= counts.loc_read.all[best] {
        debug!("            Location {} is better, but unique lcs(L{},R) >= unique LCS(L{},R)", i, j, i);
        false
    } else if counts.loc_read.non_ov[worst] >= counts.loc_read.non_ov[best] {
        debug!("            Location {} is better, but non-overl unique lcs(L{},R) >= non-overl unique LCS(L{},R)",
            i, j, i);
        false
    } else if locs[worst].read_lcs.len() >= locs[best].read_lcs.len() {
        debug!("            Location {} is better, but lcs(L{},R) >= LCS(L{},R)", i, j, i);
        false
    } else {
        true
    }
}

// Return pair of indices (better, worse) if there is a significant difference.
fn compare_lcs(loc_i: &Location, loc_j: &Location, query_len: usize, k: u32, params: &Parameters)
        -> Option<(usize, usize)> {
    let counts = if loc_i.unknown_region.is_none() && loc_j.unknown_region.is_none() {
        count_unique_without_ns(loc_i, loc_j, k)
    } else {
        count_unique_with_ns(loc_i, loc_j, k)
    };

    let indices = [loc_i.index, loc_j.index];
    let perc = [100.0 * counts.ratio(0), 100.0 * counts.ratio(1)];
    for i in 0..2 {
        debug!("            {:<3} Non-overl: {:5} / {:5} = {:4.1}%.   All: {:5} / {:5} = {:4.1}%",
            indices[i], counts.loc_read.non_ov[i], counts.loc_loc.non_ov[i], perc[i],
            counts.loc_read.all[i], counts.loc_loc.all[i], 100.0 * counts.ratio_all(i));
    }

    if perc[0] >= 5.0 && perc[1] >= 5.0 {
        let (lcs_str1, lcs_str2) = lcs_to_strings(&loc_i.read_lcs, &loc_j.read_lcs, query_len);
        debug!("            windows LCS(L{}, R): [{}]", indices[0], lcs_str1);
        debug!("            windows LCS(L{}, R): [{}]", indices[1], lcs_str2);
    }

    let p_value = homogeneity_test(
        counts.loc_read.non_ov[0], counts.loc_loc.non_ov[0].saturating_sub(counts.loc_read.non_ov[0]),
        counts.loc_read.non_ov[1], counts.loc_loc.non_ov[1].saturating_sub(counts.loc_read.non_ov[1]));

    if p_value < params.filtering_p_value && can_use_p_value(&[loc_i, loc_j], &counts) {
        let (better, worse) = if perc[0] > perc[1] {
            (indices[0], indices[1])
        } else {
            (indices[1], indices[0])
        };
        debug!("            p-value: {:.3e} ({} >> {} significantly)", p_value, better, worse);
        Some((better, worse))
    } else {
        debug!("            p-value: {:.3e}", p_value);
        None
    }
}

fn merge_overlapping_locations(locations: &mut Vec<DirectionalSpan>, overlap_perc: f64,
        genome: &Genome) {
    let mut i = 0;
    while i < locations.len() - 1 {
        for j in (i + 1..locations.len()).rev() {
            let perc = locations[i].intersection_percentage(&locations[j]);
            if locations[i].strand() == locations[j].strand() && perc >= overlap_perc {
                debug!("            Combining {} and {}", locations[i].format(genome),
                    locations[j].format(genome));
                locations[i] = locations[i].combine(&locations[j]);
                locations.remove(j);
                break;
            }
        }
        i += 1;
    }
}

pub fn filter_locations(query_seq: &bam::record::Sequence, location_coords: &[OverflowSpan],
        genome: &mut Genome, params: &Parameters) -> Vec<DirectionalSpan> {
    if location_coords.is_empty() {
        return vec![];
    }
    debug!("    Filtering locations");

    let k = params.filtering_kmer;
    let mut query_kmers: Vec<_> = genome::kmers_without_n(query_seq.subseq(..), k as usize)
        .collect();
    query_kmers.sort_by(Kmer::kmer_first);
    let mut locations = Vec::with_capacity(location_coords.len());
    let mut return_empty = false;
    for (i, coords) in location_coords.iter().enumerate() {
        let len = coords.span.len() + coords.left_clipping + coords.right_clipping;
        if coords.left_clipping == 0 && coords.right_clipping == 0 {
            debug!("        {}: {} (len: {})", i, coords.span.format(genome), len);
        } else {
            debug!("        {}: {} (+ out of bounds: {}, {}) (len: {})", i,
                coords.span.format(genome), coords.left_clipping, coords.right_clipping, len);
        }
        if return_empty {
            continue;
        }
        if let Some(location) = Location::new(i, coords, &query_kmers, genome, k) {
            if !params.unknown_regions.realign() && location.unknown_region.is_some() {
                debug!("    [--unknown-regions {}] The location has unknown region. Skipping the read",
                    params.unknown_regions);
                return_empty = true;
            } else {
                locations.push(location);
            }
        } else {
            return_empty = true;
        }
    }
    if return_empty {
        return vec![];
    }

    let n = location_coords.len();
    let mut graph: DiGraph<(), ()> = DiGraph::with_capacity(n, n * n);
    for _ in 0..n {
        graph.add_node(());
    }

    for i in 0..n {
        for j in (i + 1)..n {
            debug!("        Pair {} and {}", i, j);
            if let Some((better, worse)) = compare_lcs(&locations[i], &locations[j], query_seq.len(), k, &params) {
                graph.add_edge(NodeIndex::new(worse), NodeIndex::new(better), ());
            } else {
                graph.add_edge(NodeIndex::new(i), NodeIndex::new(j), ());
                graph.add_edge(NodeIndex::new(j), NodeIndex::new(i), ());
            }
        }
    }

    let mut sink_component: Vec<_> = tarjan_scc(&graph)[0].iter().map(|i| i.index()).collect();
    if sink_component.len() == 1 {
        debug!("        Keep location {}", sink_component[0]);
        return vec![location_coords[sink_component[0]].span.clone()];
    }

    // Bigger is better
    sink_component.sort_by(|&i, &j| locations[j].read_lcs.len().cmp(&locations[i].read_lcs.len()));
    debug!("        Keep locations {:?} (sorted by lcs length). Numbers will be reassigned", sink_component);
    let mut res = sink_component.into_iter().map(|i| location_coords[i].span.clone()).collect();
    merge_overlapping_locations(&mut res, 90.0, genome);
    res
}
