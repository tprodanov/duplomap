use std::io::{Read, Seek};
use std::collections::HashSet;

use bam;

use biology::{Genome, Sequence};
use biology::spans::{Span, SpanTools, PositiveSpan};
use biology::IdConverter;

pub fn fetch<R: Read + Seek>(reader: &mut bam::IndexedReader<R>,
        duplicated_regions: &Vec<PositiveSpan>, id_converter: &IdConverter, genome: &Genome)
        -> Vec<bam::Record> {
    let mut res = Vec::new();
    let mut names = HashSet::new();
    for region in duplicated_regions.iter() {
        // trace!("Fetching reads from {}", region.format(genome));
        let bam_chrom_id = match id_converter.from_main(region.chrom_id()) {
            Some(value) => value,
            None => {
                error!("Duplication covers chromosome {}, which is not in the bam file",
                    genome.chrom_name(region.chrom_id()));
                break;
            }
        };

        let viewer = reader.fetch_by(&bam::Region::new(bam_chrom_id as u32, region.start() as u32, region.end() as u32),
            |record| !record.flag().is_secondary() && !record.flag().is_supplementary())
            .unwrap_or_else(|e| panic!("Error while fetching reads from {}: {}", region.format(genome), e));
        for record in viewer {
            match record {
                Ok(record) => {
                    if !names.contains(record.name()) {
                        names.insert(record.name().to_vec());
                        res.push(record);
                    }
                },
                Err(e) => {
                    let prev_name = res.last().map_or_else(|| "None".to_string(),
                        |record| record.name().to_str().to_string());
                    error!("Error while fetching reads from {}. Failed to read a bam record \
                        ({:?}), previous record: {}", region.format(genome), e, prev_name);
                    panic!("Failed to read bam file");
                },
            }
        }
    }
    res
}
