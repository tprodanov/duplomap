use std::cmp::{min, max};
use std::ops::Range;

use petgraph::Direction;
use petgraph::algo;
use petgraph::graph::{NodeIndex, IndexType, DiGraph};
use pbr::ProgressBar;
use bam::record::cigar::{Cigar, Operation};

use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan, SpanSequence};
use biology::{Genome, SpanSet};
use alignment::c_minimap;
use alignment::Alignment;
use database::duplications::Duplication;
use database::Parameters;

fn reconstruct_path<Ix: IndexType>(dynamic: &Vec<(Option<NodeIndex<Ix>>, u32, u32)>) -> Vec<NodeIndex<Ix>> {
    let mut best_min = 0;
    let mut best_start = NodeIndex::new(0);
    for i in 0..dynamic.len() {
        let current = min(dynamic[i].1, dynamic[i].2);
        if current >= best_min {
            best_min = current;
            best_start = NodeIndex::new(i);
        }
    }

    let mut current = best_start;
    let mut res = Vec::new();
    loop {
        res.push(current);
        match dynamic[current.index()].0 {
            Some(value) => { current = value; },
            None => { break; },
        }
    }
    res.reverse();
    res
}

fn find_path<'a, E, Ix>(alns_graph: &DiGraph<&'a Alignment, E, Ix>) -> Vec<&'a Alignment>
where Ix: IndexType
{
    let mut prev = vec![(None, 0, 0); alns_graph.node_count()];
    let top_order = algo::toposort(alns_graph, None)
        .expect("The alignments graph contains a cycle");

    for node in top_order {
        let mut best_q_size = 0;
        let mut best_r_size = 0;
        let mut best_prev = None;
        let aln1 = &alns_graph[node];

        for neighbor in alns_graph.neighbors_directed(node, Direction::Incoming) {
            let aln2 = &alns_graph[neighbor];
            let q_size = prev[neighbor.index()].1
                + aln1.query_range().end() - aln2.query_range().end();
            let r_size = prev[neighbor.index()].2
                + aln1.aln_span().end() - aln2.aln_span().end();
            if min(q_size, r_size) >= min(best_q_size, best_r_size) {
                best_q_size = q_size;
                best_r_size = r_size;
                best_prev = Some(neighbor);
            }
        }
        prev[node.index()] = (best_prev, best_q_size, best_r_size);
    }
    let order = reconstruct_path(&prev);
    order.iter().map(|&i| alns_graph[i]).collect()
}

pub fn order<'a>(alignments: &Vec<&'a Alignment>) -> Vec<&'a Alignment> {
    let n_alns = alignments.len();
    let mut alns_graph: DiGraph<&Alignment, ()> = DiGraph::with_capacity(n_alns, n_alns * n_alns);
    let nodes: Vec<_> = alignments.iter().map(|aln| alns_graph.add_node(aln)).collect();

    for i in 0..n_alns {
        for j in 0..n_alns {
            if i == j {
                continue;
            }

            let r_span1 = alignments[i].aln_span();
            let r_span2 = alignments[j].aln_span();
            let q_start1 = alignments[i].query_range().start();
            let q_end1 = alignments[i].query_range().end();
            let q_start2 = alignments[j].query_range().start();
            let q_end2 = alignments[j].query_range().end();

            if r_span1.start() < r_span2.start() && r_span1.end() < r_span2.end()
                    && q_start1 < q_start2 && q_end1 < q_end2 {
                alns_graph.add_edge(nodes[i], nodes[j], ());
            }
        }
    }
    find_path(&alns_graph)
}

fn from_matching_pairs(pairs: &[(u32, u32)]) -> Cigar {
    assert!(!pairs.is_empty(), "matching pairs should be non-empty");
    let mut cigar = Cigar::new();
    let (mut q_exp, mut r_exp) = pairs[0];
    let mut current_match = 0;

    for &(q_pos, r_pos) in pairs {
        if (q_pos > q_exp || r_pos > r_exp) && current_match > 0 {
            cigar.push(current_match, Operation::AlnMatch);
            current_match = 0;
        }
        if q_pos > q_exp {
            cigar.push(q_pos - q_exp, Operation::Insertion);
        }
        if r_pos > r_exp {
            cigar.push(r_pos - r_exp, Operation::Deletion);
        }
        current_match += 1;
        q_exp = q_pos + 1;
        r_exp = r_pos + 1;
    }

    cigar.push(current_match, Operation::AlnMatch);
    cigar.shrink_to_fit();
    cigar
}

fn construct_matching_pairs<'a, I>(alignments: I) -> Vec<(u32, u32)>
where I: Iterator<Item = &'a Alignment>
{
    let mut res = Vec::new();
    let mut prev_q_end = 0;
    let mut prev_r_end = 0;

    for aln in alignments {
        for (q_pos, r_pos) in aln.get_matching_pairs() {
            if q_pos >= prev_q_end && r_pos >= prev_r_end {
                res.push((q_pos, r_pos));
            }
        }
        if let Some((q_pos, r_pos)) = res.last() {
            prev_q_end = q_pos + 1;
            prev_r_end = r_pos + 1;
        }
    }
    res
}

fn outside_high_copy_num_regions(pos: u32, high_cn_regions: &[Range<u32>], forb_i: &mut usize) -> bool {
    while *forb_i < high_cn_regions.len() && pos >= high_cn_regions[*forb_i].end {
        *forb_i += 1;
    }
    *forb_i == high_cn_regions.len() || pos < high_cn_regions[*forb_i].start
}

fn remove_inside_high_copy_num_regions(pairs: &[(u32, u32)], q_high_cn: &[Range<u32>], r_high_cn: &[Range<u32>])
        -> Vec<(u32, u32)> {
    let mut q_i = 0;
    let mut r_i = 0;
    let mut res = Vec::new();
    for &(q_pos, r_pos) in pairs {
        if outside_high_copy_num_regions(q_pos, q_high_cn, &mut q_i)
                && outside_high_copy_num_regions(r_pos, r_high_cn, &mut r_i) {
            res.push((q_pos, r_pos))
        }
    }
    res
}

fn split_matching_pairs(pairs: &[(u32, u32)], max_distance: u32) -> Vec<Range<usize>> {
    let mut res = Vec::new();
    let mut start_ix = 0;
    let (mut q_exp, mut r_exp) = pairs[0];

    for (i, &(q_pos, r_pos)) in pairs.iter().enumerate() {
        if q_pos > q_exp + max_distance || r_pos > r_exp + max_distance {
            res.push(start_ix .. i);
            start_ix = i;
        }
        q_exp = q_pos + 1;
        r_exp = r_pos + 1;
    }
    res.push(start_ix .. pairs.len());
    res
}

/// Joins alignments, than removes regions intersecting high copy num regions,
/// than splits alignments if they are too far.
pub fn transform_and_extend_alignments(region1: &DirectionalSpan, region2: &PositiveSpan,
        alignments: &Vec<Alignment>, q_high_cn: &[Range<u32>], r_high_cn: &[Range<u32>],
        max_distance: u32, min_size: u32, dup_alignments: &mut Vec<DupAlignment>) {
    assert!(!alignments.is_empty(), "Cannot transform empty list of alignments");

    let primary = alignments.iter().filter(|aln| aln.primary && !aln.supplementary)
        .next().expect("Cannot join alignments without a primary alignment");
    let good_alignments: Vec<_> = alignments.iter().filter(|aln| {
        if aln.strand() != primary.strand() {
            warn!("Cannot join alignments from different strands");
            return false;
        }
        if aln.aln_span().chrom_id() != primary.aln_span().chrom_id() {
            warn!("Cannot join alignments from different chromosomes (id = {} and id = {})",
                aln.aln_span().chrom_id(),
                primary.aln_span().chrom_id());
            return false;
        }
        true
    }).collect();

    let ordered: Vec<&Alignment> = order(&good_alignments);
    let mut matching_pairs = construct_matching_pairs(ordered.into_iter());
    if !q_high_cn.is_empty() || !r_high_cn.is_empty() {
        debug!("            With high cn: {} aligned bases", matching_pairs.len());
        matching_pairs = remove_inside_high_copy_num_regions(&matching_pairs, q_high_cn, r_high_cn);
        debug!("            Without:      {}", matching_pairs.len());
    }
    if matching_pairs.is_empty() {
        return;
    }

    for Range { start: start_ix, end: end_ix } in split_matching_pairs(&matching_pairs, max_distance) {
        let (start1, start2) = matching_pairs[start_ix];
        let end1 = matching_pairs[end_ix - 1].0 + 1;
        let end2 = matching_pairs[end_ix - 1].1 + 1;
        debug!("        Aln subregion: {}..{} - {}..{}", start1, end1, start2, end2);
        if end1 - start1 < min_size || end2 - start2 < min_size {
            continue;
        }
        let cigar = from_matching_pairs(&matching_pairs[start_ix..end_ix]);
        dup_alignments.push(
            DupAlignment::new(region1.extract_region(start1, end1), region2.extract_region(start2, end2), cigar));
    }
}

pub fn align_pair(span_seq1: &SpanSequence<DirectionalSpan>, span_seq2: &SpanSequence<PositiveSpan>, genome: &Genome,
        params: &Parameters, high_cn_set: &SpanSet<()>, dup_alignments: &mut Vec<DupAlignment>) {
    let span1 = span_seq1.span();
    let span2 = span_seq2.span();

    debug!("    Aligning {} and {} (len {} and {})", span1.format(genome), span2.format(genome),
        span1.len(), span2.len());
    let mut high_cn1 = Vec::new();
    for (region, _) in high_cn_set.find_pairs(span1) {
        if region.contains(span1) {
            debug!("        Region 1 completely covered by high copy num region {}", region.format(genome));
            return;
        }
        high_cn1.push(if span1.strand() {
            region.start().saturating_sub(span1.start()) .. min(region.end(), span1.end()) - span1.start()
        } else {
            span1.end().saturating_sub(region.end()) .. span1.end() - max(region.start(), span1.start())
        });
        debug!("            High c.n. region 1: {}, {:?}", region.format(genome), high_cn1[high_cn1.len() - 1]);
    }
    let mut high_cn2 = Vec::new();
    for (region, _) in high_cn_set.find_pairs(span2) {
        if region.contains(span2) {
            debug!("        Region 2 completely covered by high copy num region {}", region.format(genome));
            return;
        }
        high_cn2.push(region.start().saturating_sub(span2.start()) .. min(region.end(), span2.end()) - span2.start());
        debug!("            High c.n. region 2: {}, {:?}", region.format(genome), high_cn2[high_cn2.len() - 1]);
    }
    if high_cn1.len() > 0 || high_cn2.len() > 0 {
        debug!("        Region 1 overlaps {} high copy num regions, Region 2 overlaps {} high copy num regions",
            high_cn1.len(), high_cn2.len());
    }

    let ref_span = PositiveSpan::new(0, 0, span_seq2.sequence().len() as u32);
    let alignments = c_minimap::align(
        span_seq1.sequence().to_vec(), span_seq2.sequence().to_vec(), &ref_span, (0, 0),
        params.minimap_kmer_size as i32, true, c_minimap::Preset::SimilarSequences, true);
    if alignments.is_empty() {
        warn!("{} and {} did not align", span1.format(genome), span2.format(genome));
        return;
    } else if alignments.len() > 1 {
        debug!("        There are {} alignments", alignments.len());
    }

    const MAX_DISTANCE: u32 = 100;
    const MIN_SIZE: u32 = 50;
    transform_and_extend_alignments(span1, span2, &alignments, &high_cn1, &high_cn2,
        MAX_DISTANCE, MIN_SIZE, dup_alignments);
    debug!("    Total alignments: {}", dup_alignments.len());
}

pub struct DupAlignment {
    region1: DirectionalSpan,
    region2: PositiveSpan,
    // region2 is reference, region1 is query (rev-compl if !strand).
    cigar: Cigar,
}

impl DupAlignment {
    pub fn new(region1: DirectionalSpan, region2: PositiveSpan, cigar: Cigar) -> Self {
        Self { region1, region2, cigar }
    }

    pub fn region1(&self) -> &DirectionalSpan {
        &self.region1
    }

    pub fn region2(&self) -> &PositiveSpan {
        &self.region2
    }

    pub fn cigar(&self) -> &Cigar {
        &self.cigar
    }
}

pub fn align_all<T: std::io::Write>(duplications: &[Duplication], genome: &mut Genome, params: &Parameters,
        high_cn_set: &SpanSet<()>, pbar: &mut ProgressBar<T>) -> Vec<DupAlignment> {
    info!("Aligning {} duplications", duplications.len());
    let mut res = Vec::new();
    for dupl in duplications {
        let span_seq1 = SpanSequence::new(dupl.span1.clone(), genome);
        let span_seq2 = SpanSequence::new(dupl.span2.clone(), genome);

        align_pair(&span_seq1, &span_seq2, genome, params, high_cn_set, &mut res);
        pbar.inc();
    }
    res
}
