use std::cmp::min;

use bio::utils::TextSlice;
use bio::pattern_matching::kmp::KMP;

use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan};
use biology::Genome;
use database::{Database, Parameters};
use database::diverse;
use database::align_dup::DupAlignment;
use utils::common::adjust_window_size;

fn contains(text: TextSlice, pattern: &KMP) -> bool {
    pattern.find_all(text).next().is_some()
}

fn is_anchor(sequence: TextSlice, pos: usize, anchor_size: usize, area_radius: usize) -> bool {
    let seq_len = sequence.len();
    if pos + anchor_size > seq_len {
        return false;
    }
    let left_area = &sequence[pos.saturating_sub(area_radius)
        ..(pos + anchor_size / 2)];
    let right_area = &sequence[(pos + anchor_size / 2)
        ..min(pos + anchor_size + area_radius, seq_len)];

    let anchor = KMP::new(&sequence[pos..pos + anchor_size]);
    let left_anchor = KMP::new(&sequence[pos..pos + anchor_size / 2 + 1]);
    let right_anchor = KMP::new(&sequence[pos + anchor_size / 2 - 1..pos + anchor_size]);
    !(contains(left_area, &anchor) || contains(right_area, &anchor)
        || contains(left_area, &left_anchor) || contains(right_area, &right_anchor))
}

type MatchingPairs = Vec<(u32, u32)>;

fn extend_psvs(psvs: &mut Vec<[DirectionalSpan; 2]>,
        span1: &DirectionalSpan, seq1: TextSlice, span2: &PositiveSpan, seq2: TextSlice,
        matching_pairs: &MatchingPairs, genome: &Genome, anchor_size: usize, area_radius: usize) {
    let mut expected1 = matching_pairs[0].0 as usize;
    let mut expected2 = matching_pairs[0].1 as usize;
    let mut consecutive = 0;

    let mut last_anchor = (expected1, expected2);
    let mut current_start = None;
    for (pos1, pos2) in matching_pairs {
        let pos1 = *pos1 as usize;
        let pos2 = *pos2 as usize;
        if seq1[pos1] != seq2[pos2] {
            continue;
        }

        if expected1 != pos1 || expected2 != pos2 {
            consecutive = 0;
            if current_start.is_none() {
                current_start = Some(last_anchor);
            }
        }
        consecutive += 1;
        expected1 = pos1 + 1;
        expected2 = pos2 + 1;

        if consecutive >= anchor_size {
            let anchor_pos1 = pos1 + 1 - anchor_size;
            let anchor_pos2 = pos2 + 1 - anchor_size;
            if is_anchor(seq1, anchor_pos1, anchor_size, area_radius)
                    && is_anchor(seq2, anchor_pos2, anchor_size, area_radius) {
                last_anchor = (anchor_pos1, anchor_pos2);

                if let Some((start1, start2)) = current_start {
                    let psv_span1 = span1.extract_region(start1 as u32, pos1 as u32 + 1);
                    let psv_span2 = span2.extract_region(start2 as u32, pos2 as u32 + 1)
                        .to_directional(true);
                    if seq1[start1..pos1 + 1] != seq2[start2..pos2 + 1] {
                        psvs.push([psv_span1, psv_span2]);
                    } else {
                        warn!("Attempted to add PSV with the same sequences {} and {}",
                            psv_span1.format(genome), psv_span2.format(genome));
                    }
                    current_start = None;
                }
            }
        }
    }

    let (last1, last2) = matching_pairs[matching_pairs.len() - 1];
    if let Some((start1, start2)) = current_start {
        let psv_span1 = span1.extract_region(start1 as u32, last1 + 1);
        let psv_span2 = span2.extract_region(start2 as u32, last2 + 1).to_directional(true);
        if seq1[start1..last1 as usize + 1] != seq2[start2..last2 as usize + 1] {
            psvs.push([psv_span1, psv_span2]);
        } else {
            warn!("Attempted to add PSV with the same sequences {} and {}",
                psv_span1.format(genome), psv_span2.format(genome));
        }
    }
}

fn extend_windows(windows: &mut Vec<[DirectionalSpan; 2]>,
        span1: &DirectionalSpan, span2: &PositiveSpan, matching_pairs: &MatchingPairs) {
    let window = 100;
    let (first1, first2) = matching_pairs[0];
    let (last1, last2) = matching_pairs[matching_pairs.len() - 1];
    let window = adjust_window_size((last1 - first1) as usize, window);

    let mut current_start1 = first1;
    let mut current_start2 = first2;
    for &(pos1, pos2) in matching_pairs.iter() {
        if pos1 - current_start1 >= window as u32 {
            windows.push([span1.extract_region(current_start1, pos1),
                span2.extract_region(current_start2, pos2).to_directional(true)]);
            current_start1 = pos1;
            current_start2 = pos2;
        }
    }
    windows.push([span1.extract_region(current_start1, last1 + 1),
        span2.extract_region(current_start2, last2 + 1).to_directional(true)]);
}

fn extend_database(database: &mut Database, dup_aln: &DupAlignment, params: &Parameters, genome: &mut Genome) {
    let span1 = dup_aln.region1();
    let span2 = dup_aln.region2();
    debug!("    Extending database with pair {}  {}:+", span1.format(genome), span2.format(genome));
    let seq1 = genome.fetch(span1);
    let seq2 = genome.fetch(span2);

    let splitted = diverse::split_into_matching_pairs(&dup_aln.cigar().aligned_pairs(0).collect(),
        &seq1, &seq2, params.window_size as usize, params.min_windows as usize, params.window_similarity);
    debug!("        After splitting the alignment there are {} duplicated regions", splitted.len());

    for subpairs in splitted {
        let (first1, first2) = subpairs[0];
        let (last1, last2) = subpairs[subpairs.len() - 1];
        let region_span1 = span1.extract_region(first1, last1 + 1);
        let region_span2 = span2.extract_region(first2, last2 + 1);
        debug!("        Region {} {}:+   ({} {}, {} {})", region_span1.format(genome),
            region_span2.format(genome), first1, last1, first2, last2);
        if last1 - first1 < params.anchor_size || last2 - first2 < params.anchor_size {
            debug!("        Region is too short");
            continue;
        }

        database.duplicated_regions.push(region_span1.to_positive());
        database.duplicated_regions.push(region_span2);

        extend_psvs(&mut database.psvs, span1, &seq1, span2, &seq2, &subpairs, genome,
            params.anchor_size as usize, params.area_radius as usize);
        extend_windows(&mut database.windows, span1, span2, &subpairs);
    }
}

pub fn create_database<'a, I>(dup_alns: I, params: &Parameters, genome: &mut Genome) -> Database
where I: Iterator<Item = &'a DupAlignment>
{
    let mut db = Database::new();
    for dup_aln in dup_alns {
        extend_database(&mut db, dup_aln, params, genome)
    }
    db
}
