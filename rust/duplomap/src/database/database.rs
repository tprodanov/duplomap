use std::path::Path;
use std::fs::{self, File};
use std::io::{Write, Read, BufReader, BufRead};
use std::convert::TryFrom;

use byteorder::{BigEndian, WriteBytesExt, ReadBytesExt};
use flate2::bufread::GzDecoder;

use biology::spans::{self, Span, SpanTools, PositiveSpan, DirectionalSpan, SpanSequence};
use biology::{Genome, Sequence, IdConverter, SpanSet};
use mapping;
use database::HeavyDatabase;

pub struct Parameters {
    pub anchor_size: u32,
    pub area_radius: u32,

    pub window_size: u32,
    pub min_windows: u32,
    pub window_similarity: f64,

    pub exclude_seq_simil: f64,
    pub exclude_length: u32,
    pub components_distance: u32,

    pub high_copy_num: u8,
    pub high_copy_num_length: u32,

    pub minimap_kmer_size: u8,
}

impl Default for Parameters {
    fn default() -> Parameters {
        Parameters {
            anchor_size: 6,
            area_radius: 20,

            window_size: 100,
            min_windows: 3,
            window_similarity: 95.0,

            exclude_seq_simil: 0.97,
            exclude_length: 5_000,
            components_distance: 500,

            high_copy_num: 6,
            high_copy_num_length: 3000,

            minimap_kmer_size: 15,
        }
    }
}

pub struct Database {
    pub duplicated_regions: Vec<PositiveSpan>,
    pub psvs: Vec<[DirectionalSpan; 2]>,
    pub windows: Vec<[DirectionalSpan; 2]>,
    pub high_cn_regions: Vec<PositiveSpan>,
}

fn psv_lt(psv1: &[DirectionalSpan; 2], psv2: &[DirectionalSpan; 2]) -> std::cmp::Ordering {
    (psv1[0].to_tuple(), psv1[1].to_tuple()).cmp(&(psv2[0].to_tuple(), psv2[1].to_tuple()))
}

impl Database {
    pub fn new() -> Database {
        Database {
            duplicated_regions: Vec::new(),
            psvs: Vec::new(),
            windows: Vec::new(),
            high_cn_regions: Vec::new(),
        }
    }

    pub fn sort_and_merge(&mut self) {
        self.duplicated_regions.sort_by(|a, b| a.start_comparator().cmp(&b.start_comparator()));
        self.duplicated_regions = spans::merge(&self.duplicated_regions);
        self.windows.sort_by(|a, b| a[0].start_comparator().cmp(&b[0].start_comparator()));
        self.psvs.sort_by(|a, b| psv_lt(a, b));
        self.psvs.dedup();
    }

    pub fn set_high_cn_regions(&mut self, high_cn_set: &SpanSet<()>) {
        for region in self.duplicated_regions.iter() {
            self.high_cn_regions.extend(high_cn_set.find_pairs(&region.add_padding(10000)).map(|(r, _)| r));
        }
        self.high_cn_regions.sort_by(|a, b| a.start_comparator().cmp(&b.start_comparator()));
        self.high_cn_regions = spans::merge(&self.high_cn_regions);
    }

    pub fn print_psvs(&self, genome: &mut Genome) {
        for psv in self.psvs.iter() {
            for i in 0..2 {
                println!("{}", SpanSequence::new(psv[i].clone(), genome).format(genome));
            }
            println!();
        }
    }

    pub fn total_len(&self) -> u64 {
        self.duplicated_regions.iter().map(|span| span.len() as u64).sum()
    }

    pub fn dump<F: Write>(&self, f: &mut F, genome: &Genome) -> std::io::Result<()> {
        writeln!(f)?;
        writeln!(f, "# Duplicated regions")?;
        for region in self.duplicated_regions.iter() {
            writeln!(f, "{}", region.format(genome))?;
        }

        writeln!(f, "\n# Pairwise windows")?;
        for [span1, span2] in self.windows.iter() {
            writeln!(f, "{} {}", span1.format(genome), span2.format(genome))?;
        }

        writeln!(f, "\n# Pairwise PSVs")?;
        for [span1, span2] in self.psvs.iter() {
            writeln!(f, "{} {}", span1.format(genome), span2.format(genome))?;
        }

        writeln!(f, "\n# High copy number regions")?;
        for region in self.high_cn_regions.iter() {
            writeln!(f, "{}", region.format(genome))?;
        }
        Ok(())
    }

    pub fn load<P: AsRef<Path>>(path: P, genome: &Genome) -> Result<Database, String> {
        let path = path.as_ref();
        let file = File::open(path).map_err(|_| format!("Failed to open '{}'", path.display()))?;
        let reader = BufReader::new(file);
        let mut database = Database::new();

        let lines: Vec<_> = reader.lines().collect::<Result<_, _>>()
            .map_err(|_| "Failed to read database")?;
        let lines: Vec<_> = lines.into_iter().filter(|line| !line.starts_with('#')).collect();
        let mut iter = lines.iter();
        if iter.next().filter(|line| line.is_empty()).is_none() {
            return Err("Unexpected database format".to_string());
        }

        for line in &mut iter {
            if line.is_empty() {
                break;
            } else {
                database.duplicated_regions.push(PositiveSpan::from_string(&line, genome)?);
            }
        }

        for line in &mut iter {
            if line.is_empty() {
                break;
            } else {
                let spans: Vec<_> = line.split(' ').collect();
                assert!(spans.len() == 2, "Expected two entries on the line: \"{}\"", line);
                let span1 = DirectionalSpan::from_string(spans[0], genome)?;
                let span2 = DirectionalSpan::from_string(spans[1], genome)?;
                database.windows.push([span1, span2]);
            }
        }

        for line in &mut iter {
            let spans: Vec<_> = line.split(' ').collect();
            assert!(spans.len() == 2, "Expected two entries on the line: \"{}\"", line);
            let span1 = DirectionalSpan::from_string(spans[0], genome)?;
            let span2 = DirectionalSpan::from_string(spans[1], genome)?;
            database.psvs.push([span1, span2]);
        }
        Ok(database)
    }

    pub fn dump_binary<W: Write>(&self, mut f: W, genome: &Genome) -> std::io::Result<()> {
        let mut used = vec![false; genome.count_chromosomes()];
        for region in self.duplicated_regions.iter() {
            used[region.chrom_id() as usize] = true;
        }
        let count = u16::try_from(used.iter().fold(0, |a, b| a + *b as usize))
            .expect("Number of chromosomes is bigger than 16 bits");
        f.write_u16::<BigEndian>(count)?;
        for (i, chrom_used) in used.iter().enumerate() {
            if !*chrom_used {
                continue;
            }
            f.write_u16::<BigEndian>(i as u16)?;
            let name = genome.chrom_name(i as u32).as_bytes();
            if name.len() > std::u8::MAX as usize {
                warn!("Chromosome name is longer than 8 bits ({})", name.to_str());
                f.write_u8(std::u8::MAX)?;
                f.write_all(&name[..std::u8::MAX as usize])?;
            } else {
                f.write_u8(name.len() as u8)?;
                f.write_all(name)?;
            }
        }

        let duplicated_regions = u32::try_from(self.duplicated_regions.len())
            .expect("Number of duplicated regions is higher than 2^32");
        f.write_u32::<BigEndian>(duplicated_regions)?;
        for region in self.duplicated_regions.iter() {
            region.write_binary(&mut f)?;
        }

        let windows = u32::try_from(self.windows.len()).expect("Number of pairwise windows is higher than 2^32");
        f.write_u32::<BigEndian>(windows)?;
        for [span1, span2] in self.windows.iter() {
            span1.write_binary(&mut f)?;
            span2.write_binary(&mut f)?;
        }

        let psvs = u32::try_from(self.psvs.len()).expect("Number of PSVs is higher than 2^32");
        f.write_u32::<BigEndian>(psvs)?;
        for [span1, span2] in self.psvs.iter() {
            span1.write_binary(&mut f)?;
            span2.write_binary(&mut f)?;
        }

        let n_high_cn = u32::try_from(self.high_cn_regions.len())
            .expect("Number of high copy number regions is higher than 2^32");
        f.write_u32::<BigEndian>(n_high_cn)?;
        for region in self.high_cn_regions.iter() {
            region.write_binary(&mut f)?;
        }
        Ok(())
    }

    pub fn load_binary<F: Read>(f: &mut F, genome: &Genome) -> std::io::Result<Database> {
        let id_converter = IdConverter::from_database(genome, f)?;
        let mut database = Database::new();

        let n_duplicated_regions = f.read_u32::<BigEndian>()?;
        for _ in 0..n_duplicated_regions {
            database.duplicated_regions.push(PositiveSpan::from_binary(f, &id_converter)?);
        }

        let n_windows = f.read_u32::<BigEndian>()?;
        for _ in 0..n_windows {
            let span1 = DirectionalSpan::from_binary(f, &id_converter)?;
            let span2 = DirectionalSpan::from_binary(f, &id_converter)?;
            database.windows.push([span1, span2]);
        }

        let n_psvs = f.read_u32::<BigEndian>()?;
        for _ in 0..n_psvs {
            let span1 = DirectionalSpan::from_binary(f, &id_converter)?;
            let span2 = DirectionalSpan::from_binary(f, &id_converter)?;
            database.psvs.push([span1, span2]);
        }

        if let Ok(n_high_cn) = f.read_u32::<BigEndian>() {
            for _ in 0..n_high_cn {
                database.high_cn_regions.push(PositiveSpan::from_binary(f, &id_converter)?);
            }
        }
        Ok(database)
    }
}

pub struct NamedDatabase {
    pub name: String,
    pub database: Database,
}

impl NamedDatabase {
    pub fn new(name: String, database: Database) -> Self {
        Self { name, database }
    }

    pub fn to_heavy(self, genome: &mut Genome, params: &mapping::Parameters) -> HeavyDatabase {
        HeavyDatabase::new(self.name, self.database, genome, params)
    }
}

fn load_from_file<P: AsRef<Path>>(path: P, genome: &mut Genome) -> Result<NamedDatabase, String> {
    let path = path.as_ref();
    let file = BufReader::new(File::open(&path).map_err(|_| format!("Failed to open '{}'", path.display()))?);
    let mut decoder = GzDecoder::new(file);
    let curr_db = Database::load_binary(&mut decoder, genome).expect("Failed to load dabtabase");
    let name = path.file_stem().and_then(|os_str| os_str.to_str())
        .expect("Failed to extract database name").to_string();
    Ok(NamedDatabase::new(name, curr_db))
}

fn load_dir<P: AsRef<Path>>(path: P, genome: &mut Genome) -> Result<Vec<NamedDatabase>, String> {
    let path = path.as_ref();
    if !path.exists() {
        return Err(format!("Cannot load duplications: path '{}' does not exist", path.display()));
    }

    if path.is_file() {
        info!("Loading 1 database from '{}'", path.display());
        return match load_from_file(path, genome) {
            Ok(db) => Ok(vec![db]),
            Err(e) => Err(e),
        }
    }

    let mut files = Vec::new();
    for entry in fs::read_dir(path)
            .map_err(|_| format!("Failed to read directory '{}'", path.display()))? {
        let entry = match entry {
            Ok(value) => value,
            Err(e) => {
                error!("{}", e);
                continue;
            },
        };
        if !entry.file_type().map(|value| value.is_file()).unwrap_or(false) {
            continue;
        }
        if entry.file_name().to_str().map(|value| value.ends_with(".db")).unwrap_or(false) {
            files.push(entry.path());
        }
    }

    info!("Loading {} databases from '{}/*.db'", files.len(), path.display());
    files.into_iter().map(|path| load_from_file(path, genome)).collect()
}

pub fn load_multiple<P, I>(paths: I, genome: &mut Genome) -> Result<Vec<NamedDatabase>, String>
where P: AsRef<Path>,
      I: Iterator<Item = P>,
{
    let mut res = Vec::new();
    for path in paths {
        res.extend(load_dir(path, genome)?);
    }
    if res.is_empty() {
        panic!("No databases present");
    }
    Ok(res)
}
