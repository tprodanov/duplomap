use std::cmp::{min, max};

use biology::Genome;
use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan};
use biology::span_set::SpanSet;
use database::Database;
use database::psv::Psv;
use database::genotype::GenotypeUpdate;
use mapping::parameters::Parameters;
use utils::common::{self, GzWriter};

macro_rules! index_newtype {
    ($name:ident, $type:ty) => {
        #[derive(Clone, Copy, Hash, PartialEq, Eq)]
        pub struct $name($type);

        impl $name {
            pub fn new(value: usize) -> Self {
                Self(value as $type)
            }

            pub fn index(&self) -> usize {
                self.0 as usize
            }
        }
    };
}

index_newtype!(RegionId, u32);
index_newtype!(WindowId, u32);
index_newtype!(PsvId, u32);

pub struct HeavyDatabase {
    name: String,
    duplicated_regions: Vec<PositiveSpan>,
    region_searcher: SpanSet<RegionId>,
    windows: Vec<[DirectionalSpan; 2]>,
    window_searcher: SpanSet<WindowId>,
    psvs: Vec<Psv>,
    psv_searcher: SpanSet<PsvId>,
    high_cn_set: SpanSet<()>,
}

impl HeavyDatabase {
    pub fn new(name: String, database: Database, genome: &mut Genome, params: &Parameters) -> Self {
        let (duplicated_regions, windows, psvs) = {
            (database.duplicated_regions, database.windows, database.psvs)
        };

        let mut region_searcher = SpanSet::new(genome.count_chromosomes());
        for (i, region) in duplicated_regions.iter().enumerate() {
            region_searcher.insert(region, RegionId::new(i));
        }

        let mut window_searcher = SpanSet::new(genome.count_chromosomes());
        for (i, [window1, window2]) in windows.iter().enumerate() {
            window_searcher.insert(window1, WindowId::new(i));
            window_searcher.insert(window2, WindowId::new(i));
        }

        let total_psvs = psvs.len();
        let psvs: Vec<_> = psvs.into_iter()
            .map(|[span1, span2]| Psv::new(span1, span2, genome))
            .filter(|psv| psv.can_be_used(params))
            .collect();
        if psvs.len() < total_psvs {
            debug!("    Removed {} low complexity PSVs out of {}", total_psvs - psvs.len(), total_psvs);
        }

        let mut psv_searcher = SpanSet::new(genome.count_chromosomes());
        for (i, psv) in psvs.iter().enumerate() {
            psv_searcher.insert(psv.span(0), PsvId::new(i));
            psv_searcher.insert(psv.span(1), PsvId::new(i));
        }

        let mut high_cn_set = SpanSet::new(genome.count_chromosomes());
        for region in &database.high_cn_regions {
            high_cn_set.insert(region, ());
        }

        HeavyDatabase {
            name, duplicated_regions, region_searcher, windows, window_searcher, psvs, psv_searcher, high_cn_set }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn find_region_ids<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = RegionId> + 'a {
        self.region_searcher.find(span).cloned()
    }

    pub fn find_window_ids<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = WindowId> + 'a {
        self.window_searcher.find(span).cloned()
    }

    pub fn find_windows<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = (PositiveSpan, WindowId)> + 'a {
        self.window_searcher.find_pairs(span).map(|(interval, value)| (interval, *value))
    }

    pub fn find_psv_ids<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = PsvId> + 'a {
        self.psv_searcher.find(span).cloned()
    }

    pub fn find_psvs<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = (PositiveSpan, PsvId)> + 'a {
        self.psv_searcher.find_pairs(span).map(|(interval, value)| (interval, *value))
    }

    pub fn find_copy_num_regions<'a>(&'a self, span: &'a impl Span) -> impl Iterator<Item = PositiveSpan> + 'a {
        self.high_cn_set.find_pairs(span).map(|(interval, _)| interval)
    }

    pub fn high_copy_num_perc(&self, span: &impl SpanTools) -> f32 {
        let mut intersection = 0;
        for (high_cn_interval, _) in self.high_cn_set.find_pairs(span) {
            intersection += span.intersection_size(&high_cn_interval);
        }
        100.0 * intersection as f32 / span.len() as f32
    }

    pub fn region_at(&self, index: RegionId) -> &PositiveSpan {
        &self.duplicated_regions[index.index()]
    }

    pub fn window_at(&self, index: WindowId) -> &[DirectionalSpan; 2] {
        &self.windows[index.index()]
    }

    pub fn window_similarity(&self, window: &impl Span) -> f64 {
        let mut same_nt = vec![true; window.len() as usize];
        for psv_id in self.find_psv_ids(window) {
            let psv = self.psv_at(psv_id);
            let i = if psv.span(0).intersects(window) { 0 } else { 1 };
            let var_span = psv.var_span(i);
            for pos in max(window.start(), var_span.start())..min(window.end(), var_span.end()) {
                same_nt[(pos - window.start()) as usize] = false;
            }
        }
        100.0 * same_nt.into_iter().fold(0, |x, y| x + y as i32) as f64 / window.len() as f64
    }

    pub fn psv_at(&self, index: PsvId) -> &Psv {
        &self.psvs[index.index()]
    }

    pub fn psv_at_mut(&mut self, index: PsvId) -> &mut Psv {
        &mut self.psvs[index.index()]
    }

    pub fn duplicated_regions(&self) -> &Vec<PositiveSpan> {
        &self.duplicated_regions
    }

    pub fn psvs(&self) -> &Vec<Psv> {
        &self.psvs
    }

    pub fn update_genotypes(&mut self, updates: &[GenotypeUpdate], params: &Parameters, genome: &Genome,
            iteration: &str,  psv_writer: &mut Option<GzWriter>) {
        debug!("    Updating PSV genotypes from reads");
        for update in updates {
            self.psv_at_mut(update.psv_id())
                .genotype_mut(update.genotype_index())
                .apply_update(update, params.ambiguous_fold_diff);
        }

        debug!("    Estimating corrected genotype probabilities");
        for psv in self.psvs.iter_mut() {
            psv.update_genotypes(&params.priors, genome, iteration, psv_writer);
        }
    }

    pub fn select_active_psvs(&mut self, ambiguous_reads: f32) {
        debug!("    Selecting active PSVs");
        let mut removed_psvs = 0;
        for psv in self.psvs.iter_mut() {
            if !psv.check_ambiguous_reads(ambiguous_reads) {
                removed_psvs += 1;
            }
        }
        debug!("    Removed {} PSVs out of {}", removed_psvs, self.psvs.len());
    }

    pub fn unique_size(&self, location: &PositiveSpan) -> u32 {
        let mut intersection = 0;
        for (span, _) in self.region_searcher.find_pairs(location) {
            intersection += span.intersection_size(location);
        }
        location.len() - intersection
    }

    pub fn print_window_similarities(&self, default_window: usize) {
        println!("pos\tsimilarity");
        for region in self.duplicated_regions.iter() {
            println!("{}\t0.0", region.start());
            let window_size = common::adjust_window_size(region.len() as usize, default_window) as u32;
            for i in (0..region.len()).step_by(window_size as usize) {
                let window = region.extract_region(i, std::cmp::min(i + window_size, region.len()));
                let similarity = self.window_similarity(&window);
                println!("{}\t{:.1}", (window.start() + window.end()) / 2, similarity);
            }
            println!("{}\t0.0", region.end());
        }
    }
}
