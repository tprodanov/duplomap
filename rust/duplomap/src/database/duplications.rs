use std::collections::{HashSet, HashMap};
use std::path::Path;
use std::fmt::{self, Display, Formatter};
use std::io::Write;

use petgraph::algo::tarjan_scc;
use petgraph::graph::NodeIndex;
use petgraph::visit::EdgeRef;

use biology::spans::{Span, SpanTools, PositiveSpan, DirectionalSpan};
use biology::Genome;
use database::Parameters;
use database::align_dup::DupAlignment;
use utils::graph_map::UnGraphMap;

pub fn deduce_chrom_id(name: &str, genome: &Genome, incorrect_names: &mut HashSet<String>,
        substitutions: &mut HashMap<String, String>, where_str: &'static str) -> Option<u32> {
    if let Some(value) = genome.chrom_id(name) {
        return Some(value);
    }
    if substitutions.contains_key(name) {
        let res = genome.chrom_id(&substitutions[name]);
        assert!(res.is_some(), "Substitution name should be in the fasta file");
        return res;
    }
    if name.starts_with("chr") {
        let new_name = &name[3..];
        if let Some(value) = genome.chrom_id(new_name) {
            warn!("Using chromosome name {} (in fasta file) instead of {} (in {})",
                new_name, name, where_str);
            substitutions.insert(name.to_string(), new_name.to_string());
            return Some(value);
        }
    } else {
        let new_name = format!("chr{}", name);
        if let Some(value) = genome.chrom_id(&new_name) {
            warn!("Using chromosome name {} (in fasta file) instead of {} (in {})",
                new_name, name, where_str);
            substitutions.insert(name.to_string(), new_name);
            return Some(value);
        }
    }
    if !incorrect_names.contains(name) {
        warn!("Chromosome {} is in the duplications csv file but not in the fasta file", name);
        incorrect_names.insert(name.to_string());
    }
    return None;
}

fn parse_span(name: &str, start: &str, end: &str, genome: &Genome,
              incorrect_names: &mut HashSet<String>, substitutions: &mut HashMap<String, String>)
        -> Option<PositiveSpan> {
    let chrom_id = deduce_chrom_id(name, genome, incorrect_names, substitutions, "segmental duplications csv")?;
    let start = match start.parse() {
        Ok(value) => value,
        _ => {
            error!("Could not parse starting position {}", start);
            return None;
        },
    };
    let end = match end.parse() {
        Ok(value) => value,
        _ => {
            error!("Could not parse ending position {}", end);
            return None;
        },
    };
    Some(PositiveSpan::new(chrom_id, start, end))
}

/// Edge(index of DupAlignment. u32::MAX for overlapping)
struct Edge(u32);

impl Edge {
    fn is_overlapping(&self) -> bool {
        self.0 == std::u32::MAX
    }
}

impl Display for Edge {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        if self.is_overlapping() {
            write!(f, "P")
        } else {
            write!(f, "O")
        }
    }
}

/// Edge: index of DupAlignment. u32::MAX for overlapping.
type DuplicationsGraph = UnGraphMap<PositiveSpan, Edge>;

fn create_graph(duplications: &[DupAlignment], genome: &Genome, params: &Parameters) -> DuplicationsGraph {
    let mut graph = DuplicationsGraph::new();
    for (i, dupl) in duplications.iter().enumerate() {
        let span1 = dupl.region1();
        let span2 = dupl.region2();
        trace!("    Add paralogous edge between {} and {}", span1.format(genome), span2.format(genome));

        let v1 = graph.add_node(span1.to_positive());
        let v2 = graph.add_node(span2.clone());
        graph.add_edge(v1, v2, Edge(i as u32));
    }

    let mut nodes: Vec<_> = graph.node_indices().collect();
    nodes.sort_by(|&i, &j| graph[i].start_comparator().cmp(&graph[j].start_comparator()));
    let mut n_overlapping = 0;
    for i in 0..nodes.len() {
        for j in i + 1..nodes.len() {
            let a = nodes[i];
            let b = nodes[j];
            let distance = graph[a].sorted_distance(&graph[b]);
            if distance < params.components_distance {
                trace!("    Add overlapping edge between {} and {} (distance {})",
                    graph[a].format(genome), graph[b].format(genome), distance);
                n_overlapping += 1;
                graph.add_edge(a, b, Edge(std::u32::MAX));
            } else {
                trace!("    Do not add edge between {} and {} (distance {})",
                    graph[a].format(genome), graph[b].format(genome), distance);
                break;
            }
        }
    }
    info!("Total {} nodes and {} edges ({} paralogous and {} overlapping)",
        graph.node_count(), graph.edge_count(), duplications.len(), n_overlapping);
    graph
}

fn write_dot_component<W: Write>(graph: &DuplicationsGraph, component: &Vec<NodeIndex>, component_id: usize,
        genome: &Genome, out_dot: &mut W) -> Vec<u32> {
    let mut n_overlapping = 0;
    let mut res = Vec::new();

    for &i in component.iter() {
        let span1 = &graph[i];
        writeln!(out_dot, "    {} [label=\"{}. Component {}\"]", i.index(), span1.format(genome), component_id)
            .unwrap_or_else(|_| error!("Failed to write dot file"));
        for e in graph.edges(i) {
            let j = if e.source() == i {
                e.target()
            } else {
                e.source()
            };
            if i <= j {
                writeln!(out_dot, "    {} -- {} [label=\"{}\"]", i.index(), j.index(), e.weight())
                .unwrap_or_else(|_| error!("Failed to write dot file"));
                if e.weight().is_overlapping() {
                    n_overlapping += 1;
                } else {
                    res.push(e.weight().0);
                }
            }
        }
    }
    let n_paralogous = res.len();
    debug!("    Component {}: {} nodes and {} edges ({} paralogous and {} overlapping)",
        component_id, component.len(), n_paralogous + n_overlapping, n_paralogous, n_overlapping);
    res
}

#[derive(Debug, Clone)]
pub struct Duplication {
    pub span1: DirectionalSpan,
    pub span2: PositiveSpan,
    pub seq_simil: f64,
}

impl Duplication {
    pub fn len(&self) -> u32 {
        std::cmp::min(self.span1.len(), self.span2.len())
    }
}

/// Returns vector of components, each component is a vector of indices in `duplications`.
pub fn create_components<W: Write>(duplications: &[DupAlignment], genome: &Genome, params: &Parameters, out_dot: &mut W)
        -> Vec<Vec<u32>> {
    let mut res = Vec::new();
    let graph = create_graph(duplications, genome, params);
    let mut components = tarjan_scc(graph.graph());

    components.sort_by(|a, b| b.len().cmp(&a.len()));
    info!("Splitting graph into {} connected components", components.len());

    writeln!(out_dot, "graph {{").unwrap_or_else(|_| error!("Failed to write dot file"));
    for (i, component) in components.iter().enumerate() {
        let component_edges = write_dot_component(&graph, &component, i + 1, genome, out_dot);
        res.push(component_edges);
    }
    writeln!(out_dot, "}}").unwrap_or_else(|_| error!("Failed to write dot file"));
    res
}

fn row_get<'a>(row: &'a HashMap<String, String>, key: &str) -> &'a str {
    row.get(key).unwrap_or_else(|| panic!("Input CSV file does not have a column '{}'", key))
}

pub fn load<P: AsRef<Path>>(path: P, genome: &Genome) -> Vec<Duplication> {
    let mut reader = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_path(&path)
        .unwrap_or_else(|_| panic!("Failed to open a file {}", path.as_ref().display()));
    let mut incorrect_names = HashSet::new();
    let mut substitutions = HashMap::new();

    let mut res = Vec::new();
    let mut pairs = HashSet::new();

    for record in reader.deserialize() {
        let row: HashMap<String, String> = record.expect("Could not read a line");
        let span1 = parse_span(row_get(&row, "chrom"), row_get(&row, "chromStart"), row_get(&row, "chromEnd"),
            genome, &mut incorrect_names, &mut substitutions);
        let span2 = parse_span(row_get(&row, "otherChrom"), row_get(&row, "otherStart"), row_get(&row, "otherEnd"),
            genome, &mut incorrect_names, &mut substitutions);
        let (span1, span2) = match (span1, span2) {
            (Some(v1), Some(v2)) => (v1, v2),
            _ => continue,
        };
        let (span1, span2) = if span1.start_comparator() > span2.start_comparator() {
            (span2, span1)
        } else {
            (span1, span2)
        };
        let span1 = span1.to_directional(row_get(&row, "strand") == "+");
        let seq_simil: f64 = row_get(&row, "fracMatch").parse().unwrap_or_else(|_| {
            warn!("Could not parse fracMatch: {}", row["fracMatch"]);
            0.0
        });

        if !pairs.contains(&(span1.clone(), span2.clone())) {
            res.push(Duplication {
                span1: span1.clone(),
                span2: span2.clone(),
                seq_simil,
            });
            pairs.insert((span1, span2));
        }
    }
    res
}

pub fn filter(duplications: Vec<Duplication>, genome: &Genome, params: &Parameters) -> Vec<Duplication> {
    let mut res = Vec::new();
    let mut total_removed = 0;
    for duplication in duplications.into_iter() {
        let span1 = &duplication.span1;
        let span2 = &duplication.span2;
        if span1.len() < params.exclude_length || span2.len() < params.exclude_length {
            debug!("Discarding pair {} and {} as the sequences are too short",
                span1.format(genome), span2.format(genome));
            total_removed += 1;
            continue;
        }
        if duplication.seq_simil < params.exclude_seq_simil {
            debug!("Discarding pair {} and {} as it has low sequence similarity: {:.4}",
                span1.format(genome), span2.format(genome), duplication.seq_simil);
            total_removed += 1;
            continue;
        }
        if span1.strand() && span1.undirectional_eq(span2) {
            warn!("Duplications database contains self-duplication: {}. Skipping", span2.format(genome));
            total_removed += 1;
            continue;
        }
        res.push(duplication);
    }
    info!("Removed {} duplications", total_removed);
    res
}

pub fn find_high_copy_regions(duplications: &[Duplication], copy_num: u8, copy_num_length: u32) -> Vec<PositiveSpan> {
    // Vector of pairs (chrom_id, position, is_end).
    let mut endpoints = Vec::new();
    for dupl in duplications {
        endpoints.push((dupl.span1.chrom_id(), dupl.span1.start(), false));
        endpoints.push((dupl.span1.chrom_id(), dupl.span1.end(), true));
        endpoints.push((dupl.span2.chrom_id(), dupl.span2.start(), false));
        endpoints.push((dupl.span2.chrom_id(), dupl.span2.end(), true));
    }
    endpoints.sort();

    let mut res = Vec::new();
    let mut curr_copy_num = 0_u8;
    let mut curr_start = None;
    for &(chrom_id, pos, is_end) in endpoints.iter() {
        if is_end {
            curr_copy_num -= 1;
        } else {
            curr_copy_num += 1;
        }

        if let Some(start) = curr_start {
            if curr_copy_num < copy_num {
                if start + copy_num_length <= pos {
                    res.push(PositiveSpan::new(chrom_id, start, pos));
                }
                curr_start = None;
            }
        } else {
            if curr_copy_num >= copy_num {
                curr_start = Some(pos);
            }
        }
    }
    let total_len: u32 = res.iter().map(PositiveSpan::len).sum();
    info!("Removed {} regions with copy number >= {} and length >= {}. Sum length: {} bp",
        res.len(), copy_num, copy_num_length, total_len);
    res
}
