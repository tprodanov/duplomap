use std::path::Path;
use std::io::{Write, BufReader, BufRead};
use std::collections::{HashSet, HashMap};
use std::borrow::Borrow;
use std::fs::File;
use std::cmp::min;

use num_format::{Locale, ToFormattedString};
use regex::Regex;

use biology::spans::{self, Span, PositiveSpan, DirectionalSpan, SpanTools};
use biology::Genome;
use biology::span_set::SpanSet;
use database::Database;
use database::duplications as duplications_;

fn deserialize_attrs<'a>(string: &'a str, tags_sep: char, value_sep: char)
        -> HashMap<&'a str, &'a str> {
    let mut res = HashMap::new();
    for tag in string.split(tags_sep) {
        let key_value: Vec<_> = tag.splitn(2, value_sep).collect();
        if key_value.len() != 2 {
            warn!("Could not parse tag {}", tag);
            continue;
        }
        res.insert(key_value[0], key_value[1]);
    }
    res
}

#[derive(Debug)]
pub struct Gene {
    name: String,
    span: PositiveSpan,
    gene_type: String,
}

fn extract_attribute(attributes: &HashMap<&str, &str>, possible_keys: &[&str]) -> String {
    possible_keys.iter()
        .map(|key| attributes.get(key).map(Borrow::borrow))
        .filter_map(|x| x)
        .next().unwrap_or("NA").to_string()
}

impl Gene {
    fn new(chrom_id: u32, split_line: &Vec<&str>) -> Result<Gene, String> {
        if split_line.len() < 9 {
            return Err("Genome annotation: GFF line is too short".to_string());
        }

        let start = split_line[3].parse::<u32>().map_err(|_|
            format!("Genome annotation: Could nor parse start: {}", split_line[3]))? - 1;
        let end = split_line[4].parse().map_err(|_|
            format!("Genome annotation: Could nor parse end: {}", split_line[4]))?;
        let span = PositiveSpan::new(chrom_id, start, end);

        let attrs = deserialize_attrs(split_line[8], ';', '=');
        let name = extract_attribute(&attrs, &["gene_name", "name", "Name"]);
        let gene_type = extract_attribute(&attrs, &["gene_type", "biotype"]);
        Ok(Gene { name, span, gene_type })
    }

    fn span(&self) -> &PositiveSpan {
        &self.span
    }

    fn to_string(&self, intersection: u32, genome: &Genome) -> String {
        format!("{:15} Length: {:>10}, Intersection: {:>10}  ({:3.0}%)    {:25}  Type: {}",
            self.name, self.span.len().to_formatted_string(&Locale::en),
            intersection.to_formatted_string(&Locale::en),
            intersection as f64 * 100.0 / self.span.len() as f64,
            self.span.format(genome), self.gene_type)
    }
}

pub fn load_gff<P: AsRef<Path>>(path: P, genome: &Genome) -> Result<SpanSet<Gene>, String> {
    let path = path.as_ref();
    let file = File::open(path).map_err(|_| format!("Could not read file \"{}\"", path.display()))?;
    let reader = BufReader::new(file);

    let mut incorrect_chroms = HashSet::new();
    let mut substitutions = HashMap::new();
    let mut genes = Vec::new();
    for line in reader.lines() {
        let line = line.map_err(|_| "Could not read gff file")?;
        if line.starts_with('#') {
            continue;
        }
        let splitted: Vec<_> = line.split('\t').collect();

        match splitted.get(2) {
            Some(&"gene") => {},
            Some(_) => continue,
            None => {
                warn!("GFF line is too short: {}", line);
                continue;
            }
        }
        let chrom_id = match duplications_::deduce_chrom_id(&splitted[0], genome,
                &mut incorrect_chroms, &mut substitutions, "gene annotation") {
            Some(value) => value,
            None => continue,
        };

        genes.push(Gene::new(chrom_id, &splitted)?);
    }

    let mut span_set = SpanSet::new(genome.count_chromosomes());
    for gene in genes {
        span_set.insert(&gene.span().clone(), gene);
    }
    Ok(span_set)
}

fn replace_non_alphanumeric(name: &str) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"\W").unwrap();
    }
    let max_len = min(20, name.len());
    RE.replace_all(&name[..max_len], "_").to_string()
}

pub fn select_name(genes: &Vec<(&Gene, u32)>) -> String {
    for (gene, _) in genes.iter() {
        if gene.gene_type == "protein_coding" {
            return replace_non_alphanumeric(&gene.name);
        }
    }
    if !genes.is_empty() {
        replace_non_alphanumeric(&genes[0].0.name)
    } else {
        "".to_string()
    }
}

pub fn find_intersecting_genes<'a>(db: &Database, annotation: &'a SpanSet<Gene>)
        -> Vec<(&'a Gene, u32)> {
    let mut genes = HashMap::new();
    for region in db.duplicated_regions.iter() {
        for gene in annotation.find(region) {
            if !genes.contains_key(gene.span()) {
                genes.insert(gene.span().clone(), (gene, Vec::new()));
            }
            genes.get_mut(gene.span()).unwrap().1.push(
                region.intersect(gene.span()).expect("Regions do not intersect, but should"));
        }
    }

    let mut genes: Vec<(&Gene, u32)> = genes.into_iter().map(|(_, (gene, intersections))| {
        (gene, spans::merge(&intersections).iter().map(PositiveSpan::len).sum())
    }).collect();
    genes.sort_by(|a, b| (b.1, &b.0.name).cmp(&(a.1, &a.0.name)));
    genes
}

pub fn write_gene_annotation<F: Write>(f: &mut F, genes: &Vec<(&Gene, u32)>, genome: &Genome)
        -> std::io::Result<()> {
    if genes.is_empty() {
        return Ok(())
    }
    writeln!(f, "\n    Intersecting genes:")?;
    for (gene, intersection) in genes.iter() {
        writeln!(f, "        {}", gene.to_string(*intersection, genome))?;
    }
    Ok(())
}

fn get_copy_num_coverage(windows: &[[DirectionalSpan; 2]]) -> Vec<u32> {
    let mut endpoints = Vec::new();
    for [region1, region2] in windows.iter() {
        endpoints.push((region1.chrom_id(), region1.start(), false));
        endpoints.push((region2.chrom_id(), region2.start(), false));
        endpoints.push((region1.chrom_id(), region1.end(), true));
        endpoints.push((region2.chrom_id(), region2.end(), true));
    }
    endpoints.sort();
    let endpoints = endpoints;

    let mut copy_num_coverage = Vec::new();
    let mut curr_copy_number = 0;
    for i in 0..endpoints.len() {
        if curr_copy_number != 0 {
            let (prev_chrom, prev_pos, _) = &endpoints[i - 1];
            let (curr_chrom, curr_pos, _) = &endpoints[i];
            assert!(prev_chrom == curr_chrom, "Chromosome ends with non-zero copy number");

            let distance = curr_pos - prev_pos;
            if distance > 0 {
                if copy_num_coverage.len() <= curr_copy_number {
                    copy_num_coverage.resize(curr_copy_number + 1, 0);
                }
                copy_num_coverage[curr_copy_number] += distance;
            }
        }

        // Is end
        if endpoints[i].2 {
            curr_copy_number -= 1;
        } else {
            curr_copy_number += 1;
        }
    }
    copy_num_coverage
}

// Return: (total length, number of regions)
fn calculate_len(duplicated_regions: &Vec<PositiveSpan>, max_distance: u32) -> (u32, u32) {
    let mut total_len = 0;
    let mut regions_count = 0;
    for (i, curr) in duplicated_regions.iter().enumerate() {
        total_len += curr.len();
        regions_count += 1;

        if i != 0 {
            let prev = &duplicated_regions[i - 1];
            assert!(prev.start_comparator() < curr.start_comparator(), "Spans should be sorted");
            assert!(!prev.intersects(curr), "Spans should not intersect");

            let dist_from_prev = prev.sorted_distance(curr);
            if dist_from_prev < max_distance {
                total_len += dist_from_prev;
                regions_count -= 1;
            }
        }
    }
    (total_len, regions_count)
}

pub fn write_duplications<F, S>(db: &Database, description: &mut F, summary: &mut S)
    -> std::io::Result<()>
where F: Write,
      S: Write,
{
    let (total_len, regions_count) = calculate_len(&db.duplicated_regions, 1000);
    writeln!(description, "    Duplicated regions: {}", regions_count)?;
    writeln!(description, "        Sum length: {}", total_len.to_formatted_string(&Locale::en))?;
    writeln!(description, "        Mean length: {}", (total_len / regions_count).to_formatted_string(&Locale::en))?;
    writeln!(description, "    Pairwise PSVs: {}", db.psvs.len().to_formatted_string(&Locale::en))?;

    let copy_num_coverage = get_copy_num_coverage(&db.windows);
    let mut weighted_sum = 0;
    for (copy_num, &coverage) in copy_num_coverage.iter().enumerate() {
        weighted_sum += (copy_num as u32 + 1) * coverage;
        if coverage >= 1000 {
            writeln!(description, "        Copy number {}: ~ {:>6.1} kb", copy_num + 1, coverage as f64 / 1000.0)?;
        }
    }
    let mean_copy_num = weighted_sum as f64 / total_len as f64;
    writeln!(description, "    Mean copy number: {:.2}", mean_copy_num)?;

    writeln!(summary, "{}\t{}\t{}\t{:.2}\t{}",
        regions_count, total_len, total_len / regions_count, mean_copy_num, db.psvs.len())?;
    Ok(())
}
