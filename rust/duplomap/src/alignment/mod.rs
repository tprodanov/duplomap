pub mod c_minimap;
pub mod alignment;
pub mod indexed_alignment;
pub mod hmm;
pub mod estimate_params;
#[cfg(test)] mod tests;

pub use alignment::alignment::Alignment;
