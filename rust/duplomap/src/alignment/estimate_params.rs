use bam::{Record, RecordReader};
use bam::record::cigar::Class;

use biology::spans::{Span, PositiveSpan};
use biology::{Genome, IdConverter};
use alignment::hmm;
use utils::Log;

#[derive(Default, Debug)]
struct Counts {
    transitions: [[u64; 3]; 3],
    match_equal: u64,
    match_not_equal: u64,
}

fn add_record(record: &Record, genome: &mut Genome, id_converter: &IdConverter,
        counts: &mut Counts) {
    let mut prev_class = Class::Match;
    for (len, op) in record.cigar().iter() {
        let curr_class = op.class();
        if curr_class == Class::Hard {
            continue;
        }

        if prev_class == Class::Match || curr_class == Class::Match {
            counts.transitions[prev_class as usize][curr_class as usize] += 1;
        } else {
            counts.transitions[prev_class as usize][Class::Match as usize] += 1;
            counts.transitions[Class::Match as usize][curr_class as usize] += 1;
        }
        counts.transitions[curr_class as usize][curr_class as usize] += len.saturating_sub(1) as u64;
        prev_class = curr_class;
    }

    let q_seq = record.sequence();
    let ref_id = id_converter.into_main(record.ref_id() as u32)
        .expect("Add record expect a correct chromosome");
    let aln_span = PositiveSpan::new(ref_id as u32, record.start() as u32, record.calculate_end() as u32);
    let r_seq = genome.fetch(&aln_span);
    let r_start = aln_span.start() as usize;

    for (q_pos, r_pos) in record.matching_pairs() {
        let q_nt = q_seq.at_acgtn_only(q_pos as usize);
        let r_nt = r_seq[r_pos as usize - r_start];
        if q_nt == b'N' || r_nt == b'N' {
            continue;
        } else if q_nt != r_nt {
            counts.match_not_equal += 1;
        } else {
            counts.match_equal += 1;
        }
    }
}

impl Counts {
    fn to_parameters(&self) -> hmm::Parameters {
        assert!(self.transitions[Class::Deletion as usize][Class::Insertion as usize] == 0,
            "Deletion -> Insertion is a forbidden transition");
        assert!(self.transitions[Class::Insertion as usize][Class::Deletion as usize] == 0,
            "Insertion -> Deletion is a forbidden transition");

        let from_match = self.transitions[Class::Match as usize].iter().sum::<u64>() as f64;
        let from_insertion = self.transitions[Class::Insertion as usize].iter().sum::<u64>() as f64;
        let from_deletion = self.transitions[Class::Deletion as usize].iter().sum::<u64>() as f64;

        let transition_probs = if from_match == 0.0 || from_insertion == 0.0
                || from_deletion == 0.0 {
            warn!("Cannot estimate transition probabilities, using default");
            hmm::TransitionProbs::default()
        } else {
            hmm::TransitionProbs {
                match_from_match: Log::from_real(
                    self.transitions[Class::Match as usize][Class::Match as usize] as f64
                    / from_match),
                insertion_from_match: Log::from_real(
                    self.transitions[Class::Match as usize][Class::Insertion as usize] as f64
                    / from_match),
                deletion_from_match: Log::from_real(
                    self.transitions[Class::Match as usize][Class::Deletion as usize] as f64
                    / from_match),

                insertion_from_insertion: Log::from_real(
                    self.transitions[Class::Insertion as usize][Class::Insertion as usize] as f64
                    / from_insertion),
                match_from_insertion: Log::from_real(
                    self.transitions[Class::Insertion as usize][Class::Match as usize] as f64
                    / from_insertion),

                deletion_from_deletion: Log::from_real(
                    self.transitions[Class::Deletion as usize][Class::Deletion as usize] as f64
                    / from_deletion),
                match_from_deletion: Log::from_real(
                    self.transitions[Class::Deletion as usize][Class::Match as usize] as f64
                    / from_deletion),
            }
        };

        let emission_total = (self.match_equal + self.match_not_equal) as f64;
        let emission_probs = if emission_total == 0.0 {
            warn!("Cannot estimate emission probabilities, using default");
            hmm::EmissionProbs::default()
        } else {
            hmm::EmissionProbs {
                equal: Log::from_real(self.match_equal as f64 / emission_total),
                // 3 bases to mismatch to
                not_equal: Log::from_real(self.match_not_equal as f64 / emission_total / 3.0),
                insertion: Log::ONE,
                deletion: Log::ONE,
            }
        };
        hmm::Parameters {
            transition_probs, emission_probs,
            min_band_width: hmm::Parameters::default_band_width(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum CigarMode {
    Unknown,
    UseM,
    UseX,
}

impl CigarMode {
    fn use_m(self) -> bool {
        match self {
            CigarMode::Unknown | CigarMode::UseM => true,
            CigarMode::UseX => false,
        }
    }
}

fn get_cigar_mode(record: &Record) -> CigarMode {
    use bam::record::cigar::Operation;
    for (_, op) in record.cigar().iter() {
        match op {
            Operation::AlnMatch => return CigarMode::UseM,
            Operation::SeqMatch | Operation::SeqMismatch => return CigarMode::UseX,
            _ => {},
        }
    }
    CigarMode::Unknown
}

// Returns HMM parameters, and true if read CIGAR contains M instead of X or =.
pub fn estimate(mut reader: impl RecordReader, id_converter: &IdConverter, genome: &mut Genome,
        min_mapq: u8, max_reads: usize) -> (hmm::Parameters, bool) {
    info!("Estimating HMM parameters");
    let mut record = Record::new();
    let mut analyzed_reads = 0;
    let mut read_reads = 0;
    let read_max_reads = 10 * max_reads;

    let mut cigar_mode = CigarMode::Unknown;
    let mut counts = Counts::default();
    while analyzed_reads < max_reads && read_reads < read_max_reads {
        match reader.read_into(&mut record) {
            Ok(true) => {},
            Ok(false) => break,
            Err(e) => panic!("Failed to read a bam record: {}", e),
        };
        read_reads += 1;
        if cigar_mode == CigarMode::Unknown {
            cigar_mode = get_cigar_mode(&record);
        }

        if record.flag().any_bit(3844) || record.mapq() < min_mapq
                || id_converter.into_main(record.ref_id() as u32).is_none() {
            continue;
        }
        add_record(&record, genome, id_converter, &mut counts);
        analyzed_reads += 1;
    }
    if read_reads == read_max_reads && analyzed_reads < max_reads {
        warn!("Only {} reads of the {} analyzed reads were appropriate for parameters estimation",
            analyzed_reads, read_reads);
    }
    if analyzed_reads < max_reads && analyzed_reads < 100 {
        warn!("Cannot estimate emission probabilities, using default");
        return (hmm::Parameters::default(), cigar_mode.use_m());
    }

    let params = counts.to_parameters();
    debug!("Parameters estimated from {} reads with MAPQ >= {}", analyzed_reads, min_mapq);
    (params, cigar_mode.use_m())
}

// Return true if reads use M in CIGARs instead of X or =.
pub fn check_if_cigars_use_m(mut reader: impl RecordReader, max_reads: usize) -> bool {
    let mut record = Record::new();
    for _ in 0..max_reads {
        match reader.read_into(&mut record) {
            Ok(true) => {},
            Ok(false) => return true,
            Err(e) => panic!("Failed to read a bam record: {}", e),
        };
        let mode = get_cigar_mode(&record);
        if mode != CigarMode::Unknown {
            return mode.use_m();
        }
    }
    true
}
