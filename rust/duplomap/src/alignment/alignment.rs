use bio::utils::TextSlice;
use slog::Drain;

use bam::record::cigar::{Cigar, Operation, Class, AlignedPairs, MatchingPairs};
use bam::record::tags::TagValue;

use biology::spans::{Range, PositiveSpan, Span, SpanTools};
use biology::{Genome, IdConverter};
// use alignment::cigar::{Cigar, OperationType, Operation};
use utils::Log;
use alignment::hmm;

fn calculate_query_range(cigar: &Cigar, query_len: u32) -> Range {
    let n = cigar.len();
    let mut left_clipping = 0;
    for i in 0..n {
        let (len, op) = cigar.at(i);
        if op.consumes_ref() {
            break;
        }
        if op.consumes_query() {
            left_clipping += len;
        }
    }

    let mut right_clipping = 0;
    for i in (0..n).rev() {
        let (len, op) = cigar.at(i);
        if op.consumes_ref() {
            break;
        }
        if op.consumes_query() {
            right_clipping += len;
        }
    }

    Range::new(left_clipping, query_len - right_clipping)
}

#[derive(Clone)]
pub struct Alignment {
    query_len: u32,
    alignment_span: PositiveSpan,
    query_range: Range,
    cigar: Cigar,
    strand: bool,
    pub primary: bool,
    pub supplementary: bool,

    pub score: i32,
    pub edit_distance: i32,
}

pub struct Stats {
    pub matches: u32,
    pub mismatches: u32,
    pub insertions: u32,
    pub soft: u32,
    pub deletions: u32,
    pub score: f64,
    pub probability: Log,
}

impl Stats {
    pub fn new() -> Stats {
        Stats {
            matches: 0,
            mismatches: 0,
            insertions: 0,
            soft: 0,
            deletions: 0,
            score: 0.0,
            probability: Log::ONE,
        }
    }
}

impl Alignment {
    pub fn new(query_len: Option<u32>, alignment_span: PositiveSpan, cigar: Cigar, strand: bool)
            -> Alignment {
        let query_len = query_len.unwrap_or_else(|| cigar.calculate_query_len());
        let query_range = calculate_query_range(&cigar, query_len);
        Alignment {
            query_len, alignment_span, query_range, cigar, strand,

            primary: true,
            supplementary: false,
            score: -1,
            edit_distance: -1,
        }
    }

    pub fn query_len(&self) -> u32 {
        self.query_len
    }

    pub fn aln_span(&self) -> &PositiveSpan {
        &self.alignment_span
    }

    pub fn query_range(&self) -> &Range {
        &self.query_range
    }

    pub fn cigar(&self) -> &Cigar {
        &self.cigar
    }

    pub fn take_cigar(self) -> Cigar {
        self.cigar
    }

    pub fn strand(&self) -> bool {
        self.strand
    }

    pub fn set_strand(&mut self, strand: bool) {
        self.strand = strand
    }

    pub fn get_aligned_pairs(&self) -> AlignedPairs {
        self.cigar.aligned_pairs(self.alignment_span.start())
    }

    pub fn get_matching_pairs(&self) -> MatchingPairs {
        self.cigar.matching_pairs(self.alignment_span.start())
    }

    pub fn get_stats(&self, q_seq: &bam::record::Sequence, q_strand: bool, r_seq: TextSlice,
            hmm_params: &hmm::Parameters) -> Stats {
        const MATCH: f64 = 2.0;
        const MISMATCH: f64 = 4.0;
        const OPEN: [f64; 2] = [4.0, 24.0];
        const EXTEND: [f64; 2] = [2.0, 1.0];

        let mut stats = Stats::new();
        let mut q_pos = 0;
        let mut r_pos = 0;
        let mut prev_class = Class::Match;

        let q_len = q_seq.len();
        for (len, op) in self.cigar.iter() {
            let curr_class = op.class();
            match curr_class {
                Class::Match => {
                    for _ in 0..len {
                        let q_nt = if q_strand {
                            q_seq.at_acgtn_only(q_pos)
                        } else {
                            q_seq.compl_at_acgtn_only(q_len - q_pos - 1)
                        };
                        if q_nt == r_seq[r_pos] {
                            stats.matches += 1;
                            stats.score += MATCH;
                            stats.probability *= hmm_params.emission_probs.equal;
                        } else {
                            stats.mismatches += 1;
                            stats.score -= MISMATCH;
                            stats.probability *= hmm_params.emission_probs.not_equal;
                        }
                        q_pos += 1;
                        r_pos += 1;
                    }
                },
                Class::Insertion => {
                    q_pos += len as usize;
                    if op == Operation::Insertion {
                        stats.insertions += len;
                        stats.score -= (OPEN[0] + len as f64 * EXTEND[0])
                            .min(OPEN[1] + len as f64 * EXTEND[1]);
                    } else if op == Operation::Soft {
                        stats.soft += len;
                    }
                },
                Class::Deletion => {
                    r_pos += len as usize;
                    if op == Operation::Deletion {
                        stats.deletions += len;
                        stats.score -= (OPEN[0] + len as f64 * EXTEND[0])
                            .min(OPEN[1] + len as f64 * EXTEND[1]);
                    }
                },
                Class::Hard => continue,
            }
            stats.probability *= hmm_params.transition_probs.transition_prob(prev_class, curr_class);
            stats.probability *= hmm_params.transition_probs
                .transition_prob(curr_class, curr_class).pow(len.saturating_sub(1));
            prev_class = curr_class;
        }
        stats
    }

    pub fn write_stats(&self, stats: &Stats, genome: &Genome) {
        debug!("            {:14} {} (len: {}). Query: {} (len: {}). Strand: {}, \
            score: {:.1}, probability (log10): {:.1}",
            if self.supplementary { "Supplementary:" } else { "Primary:" },
            self.alignment_span.format(genome), self.alignment_span.len(),
            self.query_range, self.query_range.len(),
            if self.strand { '+' } else { '-' }, stats.score, stats.probability.log10());
        if !slog_scope::with_logger(|logger| logger.is_trace_enabled()) {
            return;
        }
        let aln_len = self.alignment_span.len() as f64;
        trace!("                Matches: {} ({:.1}%)",
            stats.matches, 100.0 * stats.matches as f64 / aln_len);
        trace!("                Mismatches: {} ({:.1}%)",
            stats.mismatches, 100.0 * stats.mismatches as f64 / aln_len);
        trace!("                Insertions: {} ({:.1}%)",
            stats.insertions, 100.0 * stats.insertions as f64 / aln_len);
        trace!("                Deletions: {} ({:.1}%)",
            stats.deletions, 100.0 * stats.deletions as f64 / aln_len);
        trace!("                Soft clipping: {} ({:.1}%)",
            stats.soft, 100.0 * stats.soft as f64 / aln_len);
    }

    pub fn from_bam_record(record: &bam::Record, id_converter: &IdConverter)
            -> Result<Alignment, &'static str> {
        if !record.flag().is_mapped() {
            return Err("Cannot construct an Alignment from unmapped record");
        }
        let chrom_id = id_converter.into_main(record.ref_id() as u32)
            .ok_or("Could not process a record as its chromosome is not in the genome")?;
        let alignment_span = PositiveSpan::new(chrom_id,
            record.start() as u32, record.calculate_end() as u32);
        let query_length = match record.sequence().len() {
            0 => None,
            len => Some(len as u32),
        };

        let mut aln = Alignment::new(query_length, alignment_span, record.cigar().clone(),
            !record.flag().is_reverse_strand());
        aln.primary = !record.flag().is_secondary();
        aln.supplementary = record.flag().is_supplementary();
        if let Some(TagValue::Int(value, _)) = record.tags().get(b"NM") {
            aln.edit_distance = value as i32;
        }
        if let Some(TagValue::Int(value, _)) = record.tags().get(b"AS") {
            aln.score = value as i32;
        }
        Ok(aln)
    }
}
