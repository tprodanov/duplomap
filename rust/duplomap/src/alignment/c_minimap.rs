use std::ffi::CString;
use std::os::raw;
use std::slice;

use bio::utils::Text;
use bam::record::cigar::{Cigar, Operation};

use alignment::Alignment;
use biology::spans::PositiveSpan;

#[repr(C)]
struct PafExtra {
    capacity: u32,
    dp_score: i32,
    dp_max: i32,
    dp_max2: i32,
    n_ambi_and_dummy2: u32,
    n_cigar: u32,
}

#[repr(C)]
struct Paf {
    id: i32,
    n_minimizers: i32,
    ref_id: i32,
    score: i32,
    query_start: i32,
    query_end: i32,
    ref_start: i32,
    ref_end: i32,
    parent: i32,
    subsc: i32,
    a_offset: i32,
    mlen: i32,
    blen: i32,
    n_sub: i32,
    score0: i32,
    dummy: u32,
    hash: u32,
    div: raw::c_float,
    extra: *const PafExtra,
}

#[link(name = "align_pair", kind = "static")]
extern {
    fn align_pair(q_seq: *const raw::c_char, q_len: i32, r_seq: *const raw::c_char,
        kmer: i32, flag: i32, n_hits: *mut i32, buffer: *mut raw::c_void, preset: i32)
        -> *mut Paf;

    fn free_hits(hits: *mut Paf, n_hits: i32);

    fn create_buffer() -> *mut raw::c_void;

    fn free_buffer(buffer: *mut raw::c_void);
}

struct BufferWrapper {
    buffer: *mut raw::c_void,
}

impl BufferWrapper {
    fn new() -> BufferWrapper {
        unsafe {
            BufferWrapper {
                buffer: create_buffer()
            }
        }
    }
}

impl Drop for BufferWrapper {
    fn drop(&mut self) {
        unsafe {
            free_buffer(self.buffer);
        }
    }
}

thread_local! {
    static BUFFER: BufferWrapper = BufferWrapper::new();
}

fn to_alignment(paf: &Paf, strand: bool, query_len: u32, ref_span: &PositiveSpan,
        hard_clipping: (u32, u32)) -> Alignment {
    let (soft, hard) = if strand {
        ((paf.query_start as u32, query_len - paf.query_end as u32), hard_clipping)
    } else {
        ((query_len - paf.query_end as u32, paf.query_start as u32),
            (hard_clipping.1, hard_clipping.0))
    };
    let raw_cigar = unsafe {
        let n_cigar_ptr = &(*paf.extra).n_cigar as *const u32;
        slice::from_raw_parts(n_cigar_ptr.offset(1), (*paf.extra).n_cigar as usize)
    };
    let mut cigar = Cigar::new();
    cigar.push(hard.0, Operation::Hard);
    cigar.push(soft.0, Operation::Soft);
    cigar.extend_from_raw(raw_cigar.iter().cloned());
    cigar.push(soft.1, Operation::Soft);
    cigar.push(hard.1, Operation::Hard);

    let aln_span = ref_span.extract_region(paf.ref_start as u32, paf.ref_end as u32);
    let mut aln = Alignment::new(Some(query_len), aln_span, cigar, strand);
    aln.score = paf.score;
    let n_ambiguous = unsafe {
        (*paf.extra).n_ambi_and_dummy2 & 0x3FFFFFFF
    };
    aln.edit_distance = paf.blen - paf.mlen + n_ambiguous as i32;
    aln
}

#[derive(Clone, Copy)]
pub enum Preset {
    SimilarSequences = 0,
    PacBio = 1,
    Nanopore = 2,
}

const USE_CIGAR: i32 = 0x004;
const NO_SECONDARY: i32 = 0x4000;
const FORWARD_ONLY: i32 = 0x100000;
const REVERSE_ONLY: i32 = 0x200000;
const USE_EQX: i32 = 0x4000000;

pub fn align(q_seq: Text, r_seq: Text, ref_span: &PositiveSpan, hard_clipping: (u32, u32),
        kmer: i32, strand: bool, preset: Preset, use_m: bool) -> Vec<Alignment> {
    let c_r_seq = CString::new(r_seq).unwrap();
    let q_len = q_seq.len();
    let c_q_seq = CString::new(q_seq).unwrap();

    let mut flag = USE_CIGAR | NO_SECONDARY;
    flag |= if strand {
        FORWARD_ONLY
    } else {
        REVERSE_ONLY
    };
    if !use_m {
        flag |= USE_EQX;
    }

    let mut n_hits = 0_i32;
    // let buffer = BufferWrapper::new();
    let hits = unsafe {
        BUFFER.with(|buffer|
            align_pair(c_q_seq.as_ptr(), q_len as i32, c_r_seq.as_ptr(),
                kmer, flag, &mut n_hits, buffer.buffer, preset as i32))
    };
    let best_score_ix: usize;
    let mut alignments: Vec<_> = {
        let pafs = unsafe {
            slice::from_raw_parts(hits, n_hits as usize)
        };
        best_score_ix = (0..pafs.len()).max_by(|&i, &j| pafs[i].score.cmp(&pafs[j].score))
            .unwrap_or(0);
        pafs.iter()
            .map(|paf| to_alignment(&paf, strand, q_len as u32, ref_span, hard_clipping))
            .collect()
    };
    for i in 0..alignments.len() {
        if i != best_score_ix {
            alignments[i].supplementary = true;
        }
    }
    unsafe {
        free_hits(hits, n_hits);
    }
    alignments
}

#[cfg(test)]
mod test {
    use biology::genome;
    use biology::spans::PositiveSpan;
    use alignment::c_minimap;

    #[test]
    fn test_c_alignment() {
        let reference = genome::generate(100_000);
        let query = reference[20_000..40_000].to_vec();
        let res = c_minimap::align(&query, &reference, &PositiveSpan::new(0, 0, reference.len()),
            (0, 0), 15, true, c_minimap::Preset::SimilarSequences);
        println!("{} alignments", res.len());
        for aln in res {
            println!("{:#?}", aln);
        }
    }
}
