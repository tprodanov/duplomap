use alignment::*;
use alignment::cigar::*;
use alignment::join;
use biology::spans::*;

// =================== CIGAR ===================

#[test]
fn general_cigar() {
    use alignment::cigar::OperationType::*;

    let cigar_str = "5M2I2M3D3M";
    let cigar = Cigar::from_str(cigar_str).unwrap();
    assert_eq!(cigar.to_string(), cigar_str);
    assert_eq!(cigar.len(), 5);
    assert_eq!(cigar.find_clipping(Match, Side::Left), 5);
    assert_eq!(cigar.find_clipping(Match, Side::Right), 3);
    assert_eq!(cigar.find_clipping(Hard, Side::Right), 0);
    assert_eq!(cigar.iter().map(|x| x.0).collect::<Vec<_>>(), vec![5, 2, 2, 3, 3]);
    assert!(cigar.iter().map(|x| x.1.get_type()).collect::<Vec<_>>() ==
               vec![Match, Insertion, Match, Deletion, Match]);
}

#[test]
fn cigar_clipping() {
    use alignment::cigar::OperationType::*;
    use alignment::cigar::Side::*;

    let cigar_str = "5M2I2M3D3M";
    let cigar = Cigar::with_clipping(cigar_str, (2, 1), (0, 0)).unwrap();
    assert_eq!(cigar.to_string(), format!("{}{}{}", "2S", cigar_str, "1S"));
    assert_eq!(cigar.len(), 7);
    assert_eq!(cigar.find_clipping(Insertion, Left), 2);
    assert_eq!(cigar.find_clipping(Insertion, Right), 1);
    assert_eq!(cigar.find_clipping(Hard, Left), 0);
    assert_eq!(cigar.find_clipping(Hard, Right), 0);

    let cigar = Cigar::with_clipping(cigar_str, (0, 1), (0, 0)).unwrap();
    assert_eq!(cigar.to_string(), format!("{}{}", cigar_str, "1S"));
    assert_eq!(cigar.len(), 6);
    assert_eq!(cigar.find_clipping(Insertion, Left), 0);
}

#[test]
fn cigar_hard_clipping() {
    use alignment::cigar::OperationType::*;
    use alignment::cigar::Side::*;

    let cigar_str = "10H30M8H";
    let cigar = Cigar::from_str(cigar_str).unwrap();
    assert_eq!(cigar.to_string(), cigar_str);
    assert_eq!(cigar.len(), 3);
    assert_eq!(cigar.find_clipping(Insertion, Left), 0);
    assert_eq!(cigar.find_clipping(Insertion, Right), 0);
    assert_eq!(cigar.find_clipping(Hard, Left), 10);
    assert_eq!(cigar.find_clipping(Hard, Right), 8);
}

// =================== Alignment ===================

#[test]
fn alignment_general() {
    let aln_span = PositiveSpan::new(0, 2, 8);
    // 0123456-7890
    // AACGGAG-CAGT
    // --CG-ACACT--
    let cigar = Cigar::with_clipping("2M1D2M1I1M", (0, 1), (0, 0)).unwrap();
    let aln = Alignment::new(None, aln_span, cigar, true);
    assert_eq!(aln.query_range().start(), 0);
    assert_eq!(aln.query_range().end(), 6);
    assert_eq!(aln.get_matching_pairs(), vec![(0, 2), (1, 3), (2, 5), (3, 6), (5, 7)]);
}

#[test]
fn joining_simple_alns() {
    let aln_span1 = PositiveSpan::new(0, 20, 40);
    let cigar1 = Cigar::with_clipping("20M", (0, 40), (0, 0)).unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let aln_span2 = PositiveSpan::new(0, 50, 80);
    let cigar2 = Cigar::with_clipping("30M", (30, 0), (0, 0)).unwrap();
    let aln2 = Alignment::new(None, aln_span2, cigar2, true);

    let aln = join::join_alignments(&vec![aln2, aln1]);
    assert_eq!(aln.aln_span().start(), 20);
    assert_eq!(aln.aln_span().end(), 80);
    let cigar_str = aln.cigar().to_string();
    assert!(cigar_str == "20M10D10I30M" || cigar_str == "20M10I10D30M",
            "Unexpected CIGAR: {}", cigar_str);
    let pairs = aln.get_matching_pairs();
    assert_eq!(pairs[0], (0, 20));
    assert_eq!(pairs[pairs.len() - 1], (59, 79));
    assert_eq!(pairs.len(), 50);
}

#[test]
fn joining_alns_with_hard_clipping() {
    let aln_span1 = PositiveSpan::new(0, 25, 40);
    let cigar1 = Cigar::with_clipping("15M", (5, 40), (0, 0)).unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let cigar2 = Cigar::from_str("30H30M").unwrap();
    let aln_span2 = PositiveSpan::new(0, 50, 80);
    let aln2 = Alignment::new(None, aln_span2, cigar2, true);

    let aln = join::join_alignments(&vec![aln1, aln2]);
    assert_eq!(aln.aln_span().start(), 25);
    assert_eq!(aln.aln_span().end(), 80);
    assert_eq!(aln.query_range().start(), 5);
    let cigar_str = aln.cigar().to_string();
    assert!(cigar_str == "5S15M10D10I30M" || cigar_str == "5S15M10I10D30M",
            "Unexpected CIGAR: {}", cigar_str);
    let pairs = aln.get_matching_pairs();
    assert_eq!(pairs[0], (5, 25));
    assert_eq!(pairs[pairs.len() - 1], (59, 79));
}

#[test]
fn joining_intersecting_alns() {
    let aln_span1 = PositiveSpan::new(0, 20, 40);
    let cigar1 = Cigar::with_clipping("20M", (0, 40), (0, 0)).unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let aln_span2 = PositiveSpan::new(0, 30, 60);
    let cigar2 = Cigar::with_clipping("30M", (10, 20), (0, 0)).unwrap();
    let aln2 = Alignment::new(None, aln_span2, cigar2, true);

    let aln_span3 = PositiveSpan::new(0, 50, 80);
    let cigar3 = Cigar::with_clipping("30M", (30, 0), (0, 0)).unwrap();
    let aln3 = Alignment::new(None, aln_span3, cigar3, true);

    let aln = join::join_alignments(&vec![aln2, aln3, aln1]);
    assert_eq!(aln.aln_span().start(), 20);
    assert_eq!(aln.aln_span().end(), 80);
    assert_eq!(aln.cigar().to_string(), "60M");
    assert_eq!(aln.get_matching_pairs(), (0..60).zip(20..80).collect::<Vec<_>>());
}

#[test]
fn joining_contradicting_alns() {
    // Query: aln1 aln9 aln2
    // Reference: aln9 aln1 aln2
    // aln9 should be discarded
    let aln_span1 = PositiveSpan::new(0, 30, 40);
    let cigar1 = Cigar::with_clipping("10M", (10, 40), (0, 0)).unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let aln_span2 = PositiveSpan::new(0, 50, 80);
    let cigar2 = Cigar::with_clipping("30M", (30, 0), (0, 0)).unwrap();
    let aln2 = Alignment::new(None, aln_span2, cigar2, true);

    let aln_span9 = PositiveSpan::new(0, 20, 55);
    let cigar9 = Cigar::with_clipping("35M", (25, 0), (0, 0)).unwrap();
    let aln9 = Alignment::new(None, aln_span9, cigar9, true);

    let aln = join::join_alignments(&vec![aln1, aln2, aln9]);
    assert_eq!(aln.aln_span().start(), 30);
    assert_eq!(aln.aln_span().end(), 80);
    let cigar_str = aln.cigar().to_string();
    assert!(cigar_str == "10S10M10D10I30M" || cigar_str == "10S10M10I10D30M",
            "Unexpected CIGAR: {}", cigar_str);
    let pairs = aln.get_matching_pairs();
    assert_eq!(pairs[0], (10, 30));
    assert_eq!(pairs[pairs.len() - 1], (59, 79));
    assert_eq!(pairs.len(), 40);
}

#[test]
fn joining_hard_clipping() {
    let aln_span1 = PositiveSpan::new(0, 10, 20);
    let cigar1 = Cigar::from_str("10H5M1I5M5S5H").unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let aln_span2 = PositiveSpan::new(0, 20, 30);
    let cigar2 = Cigar::from_str("10H11S10M").unwrap();
    let mut aln2 = Alignment::new(None, aln_span2, cigar2, true);
    aln2.supplementary = true;

    let aln = join::join_alignments(&vec![aln1, aln2]);
    assert_eq!(aln.aln_span().start(), 10);
    assert_eq!(aln.aln_span().end(), 25);
    assert_eq!(aln.cigar().to_string(), "10H5M1I10M5H");
}

#[test]
fn joining_hard_clipping_only_primary() {
    let aln_span1 = PositiveSpan::new(0, 10, 20);
    let cigar1 = Cigar::from_str("5H5S5M1I5M5S5H").unwrap();
    let aln1 = Alignment::new(None, aln_span1, cigar1, true);

    let aln_span2 = PositiveSpan::new(0, 0, 10);
    let cigar2 = Cigar::from_str("10M21S").unwrap();
    let mut aln2 = Alignment::new(None, aln_span2, cigar2, true);
    aln2.supplementary = true;

    let aln_span3 = PositiveSpan::new(0, 20, 30);
    let cigar3 = Cigar::from_str("21S10M").unwrap();
    let mut aln3 = Alignment::new(None, aln_span3, cigar3, true);
    aln3.supplementary = true;

    let aln = join::join_alignments(&vec![aln1, aln2, aln3]);
    assert_eq!(aln.aln_span().start(), 5);
    assert_eq!(aln.aln_span().end(), 25);
    assert_eq!(aln.cigar().to_string(), "5H10M1I10M5H");
}