use std::cmp::{min, max};
use std::fmt::{Display, Debug, Formatter};
use std::convert::TryFrom;

use regex::Regex;
use bio::utils::Text;
use num_format::{Locale, ToFormattedString};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

use biology::{Genome, Sequence, IdConverter};

// ================= Range =================

#[derive(Clone)]
pub struct Range(u32, u32);

impl Range {
    pub fn new(start: u32, end: u32) -> Range {
        assert!(start <= end, "Start should be at most end ({} > {})", start, end);
        Range(start, end)
    }

    pub fn from_range(range: std::ops::Range<u32>) -> Range {
        Range::new(range.start, range.end)
    }

    pub fn len(&self) -> u32 {
        self.1 - self.0
    }

    pub fn start(&self) -> u32 {
        self.0
    }

    pub fn end(&self) -> u32 {
        self.1
    }

    pub fn to_std_range(&self) -> std::ops::Range<usize> {
        self.0 as usize..self.1 as usize
    }

    pub fn intersects(&self, other: &Range) -> bool {
        self.start() < other.end() && other.start() < self.end()
    }
}

impl PartialEq for Range {
    fn eq(&self, other: &Range) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}

impl Display for Range {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}-{}", self.0 + 1, self.1)
    }
}

// ================= Span =================

pub trait Span: Debug + Eq {
    fn chrom_id(&self) -> u32;
    fn start(&self) -> u32;
    fn end(&self) -> u32;
    fn strand(&self) -> bool;

    fn len(&self) -> u32 {
        self.end() - self.start()
    }

    fn to_positive(&self) -> PositiveSpan {
        PositiveSpan {
            chrom_id: self.chrom_id(),
            start: self.start(),
            end: self.end(),
        }
    }

    fn to_directional(&self, strand: bool) -> DirectionalSpan {
        DirectionalSpan {
            chrom_id: self.chrom_id(),
            start: self.start(),
            end: self.end(),
            strand: strand,
        }
    }

    fn start_comparator(&self) -> (u32, u32) {
        (self.chrom_id(), self.start())
    }
}

pub trait SpanTools: Span + Clone {
    fn format<'a, 'b: 'a>(&'a self, genome: &'b Genome) -> SpanFormatter<'a, Self>;

    fn from_string(string: &str, genome: &Genome) -> Result<Self, String>;

    fn write_binary<F: WriteBytesExt>(&self, f: &mut F) -> std::io::Result<()>;

    fn from_binary<F: ReadBytesExt>(f: &mut F, id_converter: &IdConverter) -> std::io::Result<Self>;

    fn intersects(&self, other: &impl Span) -> bool {
        if self.chrom_id() != other.chrom_id() {
            false
        } else {
            self.start() < other.end() && other.start() < self.end()
        }
    }

    fn intersection_size(&self, other: &impl Span) -> u32 {
        if self.intersects(other) {
            min(self.end(), other.end()) - max(self.start(), other.start())
        } else {
            0
        }
    }

    fn intersection_percentage(&self, other: &impl Span) -> f64 {
        100.0 * self.intersection_size(other) as f64 / min(self.len(), other.len()) as f64
    }

    fn intersects_on_same_strand(&self, other: &impl Span) -> bool {
        self.strand() == other.strand() && self.intersects(other)
    }

    fn contains(&self, other: &impl Span) -> bool {
        self.chrom_id() == other.chrom_id() && self.start() <= other.start()
            && other.end() <= self.end()
    }

    fn contains_on_same_strand(&self, other: &impl Span) -> bool {
        self.strand() == other.strand() && self.contains(other)
    }

    fn sorted_distance(&self, other: &impl Span) -> u32 {
        if self.chrom_id() != other.chrom_id() {
            std::u32::MAX
        } else {
            other.start().saturating_sub(self.end())
        }
    }

    fn distance(&self, other: &impl SpanTools) -> u32 {
        if self.intersects(other) {
            0
        } else {
            max(self.sorted_distance(other), other.sorted_distance(self))
        }
    }

    fn undirectional_eq(&self, other: &impl Span) -> bool {
        self.chrom_id() == other.chrom_id() && self.start() == other.start() && self.end() == other.end()
    }

    fn trim(&mut self, genome: &Genome);
}

pub struct SpanFormatter<'a, S: Span> {
    chrom_name: &'a str,
    span: &'a S,
    display_strand: bool,
}

impl<'a, S: Span> Display for SpanFormatter<'a, S> {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        if self.display_strand {
            write!(f, "{}:{}-{}:{}", self.chrom_name, self.span.start() + 1, self.span.end(),
                if self.span.strand() { '+' } else { '-' })
        } else {
            write!(f, "{}:{}-{}", self.chrom_name, self.span.start() + 1, self.span.end())
        }
    }
}

// ================= PositiveSpan =================

#[derive(Clone, Hash)]
pub struct PositiveSpan {
    chrom_id: u32,
    start: u32,
    end: u32,
}

impl PositiveSpan {
    pub fn new(chrom_id: u32, start: u32, end: u32) -> PositiveSpan {
        assert!(start <= end, "Start should be at most end ({} > {})", start, end);
        PositiveSpan { chrom_id, start, end }
    }

    pub fn join(&self, other: &PositiveSpan) -> Option<PositiveSpan> {
        if !self.intersects(other) {
            None
        } else {
            Some(PositiveSpan::new(self.chrom_id, min(self.start, other.start),
                max(self.end, other.end)))
        }
    }

    pub fn intersect(&self, other: &PositiveSpan) -> Option<PositiveSpan> {
        if !self.intersects(other) {
            None
        } else {
            Some(PositiveSpan::new(self.chrom_id, max(self.start, other.start),
                min(self.end, other.end)))
        }
    }

    pub fn add_padding(&self, padding: u32) -> PositiveSpan {
        PositiveSpan::new(self.chrom_id,
                  self.start.saturating_sub(padding),
                  self.end + padding)
    }

    pub fn extract_region(&self, start: u32, end: u32) -> PositiveSpan {
        assert!(end <= self.len(),
            "Cannot extract region: Out of bounds (end {} > length {})", end, self.len());
        PositiveSpan::new(self.chrom_id, self.start + start, self.start + end)
    }

    pub fn set_chrom_id(&mut self, new_chrom_id: u32) {
        self.chrom_id = new_chrom_id;
    }
}

impl Span for PositiveSpan {
    fn chrom_id(&self) -> u32 {
        self.chrom_id
    }

    fn start(&self) -> u32 {
        self.start
    }

    fn end(&self) -> u32 {
        self.end
    }

    fn strand(&self) -> bool {
        true
    }
}

impl SpanTools for PositiveSpan {
    fn format<'a, 'b: 'a>(&'a self, genome: &'b Genome) -> SpanFormatter<'a, Self> {
        SpanFormatter {
            chrom_name: genome.chrom_name(self.chrom_id),
            span: self,
            display_strand: false,
        }
    }

    fn from_string(string: &str, genome: &Genome) -> Result<PositiveSpan, String> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"(?x)
                  ^([^:]+):
                  (\d+)-(\d+)$").unwrap();
        }

        let caps = RE.captures(string).ok_or_else(|| format!("Could not parse \"{}\"", string))?;
        let chrom_name: &str = &caps[1];
        let chrom_id = genome.chrom_id(&chrom_name).ok_or_else(||
            format!("Chromosome name {} is not in the genome", chrom_name))?;
        let start: u32 = caps[2].parse().unwrap();
        let end: u32 = caps[3].parse().unwrap();

        Ok(PositiveSpan::new(chrom_id, start - 1, end))
    }

    fn trim(&mut self, genome: &Genome) {
        self.end = min(self.end, genome.chrom_len(self.chrom_id));
    }

    fn write_binary<F: WriteBytesExt>(&self, f: &mut F) -> std::io::Result<()> {
        let chrom_id = u16::try_from(self.chrom_id)
            .unwrap_or_else(|_| panic!("Chromosome id is longer than 16 bits: {}", self.chrom_id));
        let start = u32::try_from(self.start)
            .unwrap_or_else(|_| panic!("Chromosome position is longer than 32 bits: {}",
                self.start));
        let end = u32::try_from(self.end)
            .unwrap_or_else(|_| panic!("Chromosome position is longer than 32 bits: {}", self.end));
        f.write_u16::<BigEndian>(chrom_id)?;
        f.write_u32::<BigEndian>(start)?;
        f.write_u32::<BigEndian>(end)
    }

    fn from_binary<F: ReadBytesExt>(f: &mut F, id_converter: &IdConverter) -> std::io::Result<Self> {
        let chrom_id = f.read_u16::<BigEndian>()?;
        let chrom_id = id_converter.into_main(chrom_id as u32)
            .expect("Cannot load a region, its chromosome is not present in the reference");
        let start = f.read_u32::<BigEndian>()?;
        let end = f.read_u32::<BigEndian>()?;
        Ok(PositiveSpan::new(chrom_id, start, end))
    }
}

impl PartialEq for PositiveSpan {
    fn eq(&self, other: &PositiveSpan) -> bool {
        self.chrom_id == other.chrom_id && self.start == other.start && self.end == other.end
    }
}

impl Eq for PositiveSpan {}

impl Debug for PositiveSpan {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "[{}]:{}-{}", self.chrom_id + 1,
            (self.start + 1).to_formatted_string(&Locale::en),
            self.end.to_formatted_string(&Locale::en))
    }
}

impl AsRef<PositiveSpan> for PositiveSpan {
    fn as_ref(&self) -> &PositiveSpan {
        &self
    }
}

// ================= DirectionalSpan =================

#[derive(Clone, Hash)]
pub struct DirectionalSpan {
    chrom_id: u32,
    start: u32,
    end: u32,
    strand: bool,
}

impl DirectionalSpan {
    pub fn new(chrom_id: u32, start: u32, end: u32, strand: bool) -> DirectionalSpan {
        assert!(start <= end, "Start should be at most end ({} > {})", start, end);
        DirectionalSpan { chrom_id, start, end, strand }
    }

    pub fn switch_strand(&self) -> DirectionalSpan {
        DirectionalSpan {
            chrom_id: self.chrom_id,
            start: self.start,
            end: self.end,
            strand: !self.strand,
        }
    }

    pub fn extract_region(&self, start: u32, end: u32) -> DirectionalSpan {
        let (start, end) = if self.strand {
            (start, end)
        } else {
            (self.end - self.start - end, self.end - self.start - start)
        };

        assert!(end <= self.len(),
            "Cannot extract region: Out of bounds (end {} > length {})", end, self.len());
        DirectionalSpan::new(self.chrom_id, self.start + start, self.start + end, self.strand)
    }

    pub fn add_padding(&self, padding: u32) -> DirectionalSpan {
        DirectionalSpan::new(self.chrom_id,
            self.start.saturating_sub(padding),
            self.end + padding,
            self.strand)
    }

    pub fn combine(&self, other: &DirectionalSpan) -> DirectionalSpan {
        assert!(self.intersects(other) && self.strand == other.strand);
        DirectionalSpan {
            chrom_id: self.chrom_id,
            start: min(self.start, other.start),
            end: max(self.end, other.end),
            strand: self.strand,
        }
    }

    pub fn to_tuple(&self) -> (u32, u32, u32, bool) {
        (self.chrom_id, self.start, self.end, self.strand)
    }
}

impl Span for DirectionalSpan {
    fn chrom_id(&self) -> u32 {
        self.chrom_id
    }

    fn start(&self) -> u32 {
        self.start
    }

    fn end(&self) -> u32 {
        self.end
    }

    fn strand(&self) -> bool {
        self.strand
    }
}

impl SpanTools for DirectionalSpan {
    fn format<'a, 'b: 'a>(&'a self, genome: &'b Genome) -> SpanFormatter<'a, Self> {
        SpanFormatter {
            chrom_name: genome.chrom_name(self.chrom_id),
            span: self,
            display_strand: true,
        }
    }

    fn from_string(string: &str, genome: &Genome) -> Result<DirectionalSpan, String> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"(?x)
                  ^([^:]+):
                  (\d+)-(\d+):
                  ([+-])$").unwrap();
        }

        let caps = RE.captures(string).ok_or_else(|| format!("Could not parse \"{}\"", string))?;
        let chrom_name: &str = &caps[1];
        let chrom_id = genome.chrom_id(&chrom_name).ok_or_else(||
            format!("Chromosome name {} is not in the genome", chrom_name))?;
        let start: u32 = caps[2].parse().unwrap();
        let end: u32 = caps[3].parse().unwrap();

        Ok(DirectionalSpan {
            chrom_id: chrom_id,
            start: start - 1,
            end: end,
            strand: &caps[4] == "+",
        })
    }

    fn trim(&mut self, genome: &Genome) {
        self.end = min(self.end, genome.chrom_len(self.chrom_id));
    }

    fn write_binary<F: WriteBytesExt>(&self, f: &mut F) -> std::io::Result<()> {
        let mut chrom_id = i16::try_from(self.chrom_id)
            .unwrap_or_else(|_| panic!("Chromosome id is longer than 16 bits: {}", self.chrom_id));
        if !self.strand {
            chrom_id = -chrom_id - 1;
        }
        let start = u32::try_from(self.start)
            .unwrap_or_else(|_| panic!("Chromosome position is longer than 32 bits: {}",
                self.start));
        let end = u32::try_from(self.end)
            .unwrap_or_else(|_| panic!("Chromosome position is longer than 32 bits: {}", self.end));
        f.write_i16::<BigEndian>(chrom_id)?;
        f.write_u32::<BigEndian>(start)?;
        f.write_u32::<BigEndian>(end)
    }

    fn from_binary<F: ReadBytesExt>(f: &mut F, id_converter: &IdConverter) -> std::io::Result<Self> {
        let mut chrom_id = f.read_i16::<BigEndian>()?;
        let strand = chrom_id >= 0;
        if !strand {
            chrom_id = -chrom_id - 1;
        }
        let chrom_id = id_converter.into_main(chrom_id as u32)
            .expect("Cannot load a region, its chromosome is not present in the reference");
        let start = f.read_u32::<BigEndian>()?;
        let end = f.read_u32::<BigEndian>()?;
        Ok(DirectionalSpan::new(chrom_id, start, end, strand))
    }
}

impl PartialEq for DirectionalSpan {
    fn eq(&self, other: &DirectionalSpan) -> bool {
        self.chrom_id == other.chrom_id && self.start == other.start
            && self.end == other.end && self.strand == other.strand
    }
}

impl Eq for DirectionalSpan { }

impl Debug for DirectionalSpan {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let strand = if self.strand { "+" } else { "-" };
        write!(f, "[{}]:{}-{}:{}", self.chrom_id + 1,
            (self.start + 1).to_formatted_string(&Locale::en),
            self.end.to_formatted_string(&Locale::en), strand)
    }
}

impl AsRef<DirectionalSpan> for DirectionalSpan {
    fn as_ref(&self) -> &DirectionalSpan {
        &self
    }
}

// ================= SpanSeq =================

pub struct SpanSequenceFormatter<'a, T: SpanTools> {
    span_seq: &'a SpanSequence<T>,
    genome: &'a Genome,
}

impl<'a, T: SpanTools> Display for SpanSequenceFormatter<'a, T> {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{:25} {}", self.span_seq.span.format(self.genome),
            self.span_seq.sequence.to_str())
    }
}

#[derive(Clone)]
pub struct SpanSequence<T: SpanTools> {
    span: T,
    sequence: Text,
}

impl<T: SpanTools> SpanSequence<T> {
    pub fn new(span: T, genome: &mut Genome) -> SpanSequence<T> {
        let sequence = genome.fetch(&span);
        SpanSequence { span, sequence }
    }

    pub fn span(&self) -> &T {
        &self.span
    }

    pub fn sequence(&self) -> &Text {
        &self.sequence
    }

    pub fn chrom_id(&self) -> u32 {
        self.span.chrom_id()
    }

    pub fn start(&self) -> u32 {
        self.span.start()
    }

    pub fn end(&self) -> u32 {
        self.span.end()
    }

    pub fn strand(&self) -> bool {
        self.span.strand()
    }

    pub fn len(&self) -> u32 {
        self.span.len()
    }

    pub fn format<'a, 'b: 'a>(&'a self, genome: &'b Genome) -> SpanSequenceFormatter<'a, T> {
        SpanSequenceFormatter {
            span_seq: self,
            genome,
        }
    }
}

impl<T: SpanTools> PartialEq for SpanSequence<T> {
    fn eq(&self, other: &SpanSequence<T>) -> bool {
        self.span.eq(&other.span)
    }
}

impl<T: SpanTools> Eq for SpanSequence<T> {}

impl<T: SpanTools + Debug> Debug for SpanSequence<T> {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{:?} {}", self.span, self.sequence.to_str())
    }
}

pub fn merge(spans: &[PositiveSpan]) -> Vec<PositiveSpan> {
    if spans.is_empty() {
        return vec![];
    }

    let mut res = Vec::with_capacity(spans.len());
    let mut prev = spans[0].clone();
    for i in 1..spans.len() {
        let curr = &spans[i];
        assert!(prev.start_comparator() <= curr.start_comparator(), "Span list should be sorted");
        prev = match prev.join(&curr) {
            Some(v) => v,
            None => {
                res.push(prev);
                curr.clone()
            },
        };
    }
    res.push(prev);
    res
}

pub fn merge_with_padding(spans: &[PositiveSpan], padding: u32) -> Vec<PositiveSpan> {
    let mut res = Vec::new();
    let mut prev = spans[0].clone();
    for i in 1..spans.len() {
        let curr = &spans[i];
        assert!(prev.start_comparator() < curr.start_comparator(), "Spans should be sorted");
        if prev.sorted_distance(curr) > 2 * padding {
            res.push(prev.add_padding(padding));
            prev = curr.clone();
        } else {
            prev = PositiveSpan::new(prev.chrom_id, min(prev.start, curr.start),
                max(prev.end, curr.end));
        }
    }
    res.push(prev.add_padding(padding));
    res
}
