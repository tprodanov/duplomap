use std::cmp::{min, max};
use std::path;

use bio::io::fasta::Record;

use biology::spans::*;
use biology::span_set::*;
use biology::genome::*;

fn construct_path() -> path::PathBuf {
    let mut path_buf = std::env::current_exe().unwrap();
    for _ in 0..4 {
        path_buf.pop();
    }
    path_buf.push("test_data");
    path_buf
}

lazy_static! {
    static ref DATA_PATH: path::PathBuf = construct_path();
}

fn load_genome() -> Genome {
    Genome::from_file(DATA_PATH.join("test.fa").to_str().unwrap())
}

lazy_static! {
    static ref GENOME: Genome = load_genome();
}

// =================== Sequence ===================

#[test]
fn sequence_general() {
    let seq1 = b"AACGTGAGCA".to_vec();
    let compl = seq1.complexity();
    assert!(compl > 0.0 && compl <= 1.0, "Complexity {} not in (0, 1]", compl);

    let seq2 = b"TGCTCACGTT".to_vec();
    assert_ne!(seq1, seq2);
    assert_eq!(seq1.reverse_complement(), seq2);

    let mut seq3 = b"AaCgTGaGcA".to_vec();
    standartize(&mut seq3);
    assert_eq!(seq1, seq3);
}

#[test]
fn sequence_kmers() {
    let seq1 = b"AACGTGAGCA".to_vec();
    assert_eq!(seq1.kmers(3), vec![1, 6, 27, 46, 56, 34, 9, 36]);
    let kmers = seq1.kmers_without_n(3);
    let (kmers_pos, kmers_values): (Vec<_>, Vec<_>) = kmers.into_iter().unzip();
    assert_eq!((0..seq1.len() - 2).collect::<Vec<_>>(), kmers_pos);
    assert_eq!(seq1.kmers(3), kmers_values);

    let seq2 = b"AACGNGAGCA".to_vec();
    assert_eq!(seq2.kmers_without_n(3), vec![(0, 1), (1, 6), (5, 34), (6, 9), (7, 36)]);
}

#[test]
fn from_kmer() {
    let seq = b"AACGTGAGCA".to_vec();
    let kmer = seq.kmers(seq.len())[0];
    assert_eq!(kmer_to_sequence(kmer, seq.len()), seq);
}

#[test] #[should_panic]
fn sequence_non_nucleotide() {
    let mut seq = b"ACGTBAC".to_vec();
    standartize(&mut seq);
}

#[test] #[should_panic]
fn sequence_n_kmers() {
    let seq = b"AACGNGAGCA".to_vec();
    seq.kmers(3);
}

// =================== Spans ===================

#[test]
fn test_genome() {
    let genome = &GENOME;
    assert_eq!(genome.count_chromosomes(), 1);
    assert_eq!(genome.chrom_id("1").unwrap(), 0);
    assert_eq!(genome.chrom_name(0), "1");
    assert_eq!(genome.chrom_len(0), 300_000);
}

#[test]
fn test_spans() {
    let genome = &GENOME;
    let span1 = PositiveSpan::new(0, 100, 200);
    assert_eq!(span1.len(), 100);
    let span2 = PositiveSpan::from_string("1:101-200", &genome).unwrap();
    assert!(span1 == span2);
    let span3 = PositiveSpan::new(0, 101, 200);
    assert!(span1 != span3);
    assert_eq!(span3.to_string(&genome), "1:102-200");
}

#[test]
fn directional_spans() {
    let genome = &GENOME;
    let span = PositiveSpan::from_string("1:101-200", &genome).unwrap();
    let span_plus = span.to_directional(true);
    let span_minus = DirectionalSpan::new(0, 100, 200, false);
    assert_eq!(span_plus.len(), span.len());
    assert_eq!(span_minus.len(), span.len());
    assert!(span_plus.switch_strand() == span_minus);

    let span_plus_2 = DirectionalSpan::from_string("1:101-200:+", &genome).unwrap();
    assert!(span_plus_2 == span_plus);
    assert_eq!(span_minus.to_string(&genome), "1:101-200:-");
}

#[test]
fn span_seqs() {
    let mut genome = load_genome();
    let span = PositiveSpan::from_string("1:100001-100010", &genome).unwrap();
    let span_seq = SpanSequence::new(span.clone(), &mut genome);
    assert!(span_seq.span() == &span);
    assert_eq!(span_seq.sequence(), &b"ACTAAGCACA".to_vec());

    let span2 = DirectionalSpan::from_string("1:100002-100010:-", &genome).unwrap();
    let span_seq2 = SpanSequence::new(span2, &mut genome);
    assert_eq!(&(&span_seq.sequence()[1..10]).reverse_complement(), span_seq2.sequence());
}

#[test] #[should_panic]
fn spans_out_of_bounds() {
    let mut genome = load_genome();
    let span = PositiveSpan::new(0, 299_000, 300_001);
    let _span_seq = SpanSequence::new(span, &mut genome);
}

#[test]
fn simple_genome() {
    let seq1 = b"ACGTGACTGA".to_vec();
    let rec1 = Record::with_attrs(&"chr1", None, &seq1);
    let seq2 = &b"TTAATTAA".to_vec();
    let rec2 = Record::with_attrs(&"chr2", None, &seq2);

    let mut genome = Genome::from_sequences(vec![rec1, rec2].into_iter());
    assert_eq!(genome.count_chromosomes(), 2);
    assert_eq!(genome.chrom_name(0), "chr1");
    assert_eq!(genome.chrom_id(&"chr2").unwrap(), 1);
    assert_eq!(genome.chrom_len(0), seq1.len());
    assert_eq!(genome.fetch(&PositiveSpan::new(0, 0, seq1.len())), seq1);
}
