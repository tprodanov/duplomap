#!/bin/bash

set -e
set -o errexit
set -o pipefail
set -o nounset

RED='\033[0;31m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

function warn {
    >&2 printf "${YELLOW}%s${NC}\n" "$*"
}

function error {
    >&2 printf "${RED}%s${NC}\n" "$*"
}

dir="$(dirname "$0")"

if ! [ -d "$dir/minimap2" ]; then
    warn "Loading minimap2"
    git clone https://github.com/lh3/minimap2.git "$dir/minimap2"
fi

if ! [ -f "$dir/minimap2/libminimap2.a" ]; then
    make CFLAGS="-g -Wall -O2 -Wc++-compat -fPIC ${CFLAGS-}" \
        INCLUDES="${C_INCLUDE_PATH:+-I$C_INCLUDE_PATH}" \
        libminimap2.a --directory "$dir/minimap2"
fi

usage=$(cat <<-END
Usage: ./build [--debug] [--install <PATH>]
    -d, --debug     Build in debug mode instead of release mode
    -i <PATH>, --install <PATH>
                    Install binaries to <PATH>/bin
    -h, --help      Show this message
END
)

build_flag="--release"
install_flag=""
output="$dir"
ls_target="rust/target/release"
install_suffix=""

arguments=( "$@" )
i=0
while [ "$i" -lt "$#" ]; do
    arg="${arguments[$i]}"
    if [ "$arg" = "--debug" ] || [ "$arg" = "-d" ]; then
        build_flag=""
        install_flag="--debug"
        output="$dir/debug"
        ls_target="../rust/target/debug"
        install_suffix="-debug"

    elif [ "$arg" = "--install" ] || [ "$arg" = "-i" ]; then
        i=$((i + 1))
        if [ $i -eq "$#" ]; then
            error "Expected an argument after --install"
            >&2 echo "$usage"
            exit 1
        else
            root="${arguments[$i]}"
        fi

    elif [ "$arg" = "--help" ] || [ "$arg" = "-h" ]; then
        echo "$usage"
        exit 0
    else
        error "Unexpected argument $arg"
        >&2 echo "$usage"
        exit 1
    fi
    i=$((i + 1))
done

mkdir -p "$output"

if ! [ $(command -v cargo) ]; then
    error 'Rust compiler is not installed.'
    error 'To install it run'
    >&2 echo '    curl https://sh.rustup.rs -sSf | sh'
    exit 1
fi

if ! $(cargo build --manifest-path "$dir/rust/Cargo.toml" $build_flag); then
    error 'Build failed.'
    exit 1
fi

function create_link {
    if ! [ -f "$output/$2" ]; then
        ln -s "$ls_target/$1" "$output/$2"
    fi
}

create_link map duplomap
create_link prepare duplomap-prepare

function install {
    exec="$output/$1"
    link="$root/bin/$1$install_suffix"
    warn "Copying executable $exec -> $link"
    cp "$exec" "$link"
}

if ! [ -z ${root:+x} ]; then
    if ! [ -d "$root/bin" ]; then
        warn "Creating directory $root/bin"
        mkdir "$root/bin"
    fi
    install duplomap
    install duplomap-prepare
fi
